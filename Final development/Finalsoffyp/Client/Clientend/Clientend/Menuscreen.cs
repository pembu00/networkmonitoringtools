﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Clientend
{
    public partial class Menuscreen : Form
    {
        public Menuscreen()
        {
            InitializeComponent();
        }

        private void viewAssetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var myform = new NetworkAssetManager.Forms.MainUI();
            myform.Show();
        }

        private void fileUploadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var myform = new Form1();
            myform.Show();
        }
    }
}
