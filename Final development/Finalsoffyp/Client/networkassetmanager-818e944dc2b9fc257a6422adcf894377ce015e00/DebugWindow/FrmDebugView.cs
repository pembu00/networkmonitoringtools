using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DebugHelper;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace DebugWindow
{
    public partial class DebugWindowForm : Form
    {
        String szError;
        private static DebugHelper.CDebugHelper oHelper = new CDebugHelper();
        char[] sep = { '#' };
        int lineCount = 300; 
        public DebugWindowForm()
        {
            InitializeComponent();
            oHelper.Initialize(out szError);
            CDebugHelper.DbgHandler += new DebugDataEventHandler(OnDebugString);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            listView1.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
            oHelper.StartReading();
            rbAll.Checked = true; 
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            oHelper.StopReading();
            oHelper.DeInitialize();
        }
        public void OnDebugString(DebugHelper.DebugDataEventArgs args)
        {
            if (args.Data.StartsWith("NETASSETTRACK"))
            {
                AddToList(args); 
            }
        }
        private void AddToList(DebugDataEventArgs args)
        {
            string[] dbgArgs = args.Data.Split(sep);

            ListViewItem item = new ListViewItem(dbgArgs);
            this.listView1.SafeControlInvoke
            (
                listViewCtrl =>
                {
                    if (listViewCtrl.Items.Count > 300)
                    {
                        listViewCtrl.Items.RemoveAt(0);
                    }
                    listViewCtrl.Items.Add(item);
                    item.EnsureVisible();

                    if (lineCount == 300)
                    {
                        listViewCtrl.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
                    }
                    lineCount--; 

                    if ( lineCount == 0)
                    {
                        lineCount = 300; 
                    }
                    


                }
            );
            item = null; 


        }

        private void button1_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();
        }
    }
}