﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlServerCe;
using log4net;

namespace NetworkAssetManager.DataAccess
{
    public abstract class BaseDB:IDisposable
    {
        private SqlCeConnection _connection = null;
        protected bool isDisposed = false;
        public BaseDB()
        {
            _connection = new SqlCeConnection(NetworkAssetManager.Properties.Settings.Default.NetworkAssetManagerConnectionString);
            try
            {
                _connection.Open(); 
            }
            catch (Exception)
            {
//TODO_NORM: Add some error handling mechanism like create database if it is not found. 

            }
        }

        public SqlCeConnection Connection
        {
            get
            {
                return _connection; 
            }
        }
        #region IDisposable Members


        public void CloseConnection()
        {
            if ( _connection.State == System.Data.ConnectionState.Open)
            {
                _connection.Close();
                _connection.Dispose();
            }
        }

        void IDisposable.Dispose()
        {
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}
