﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetworkAssetManager.Entity;
using System.Management;
using log4net; 
namespace NetworkAssetManager.WMI
{
    class Share
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(Share));

        private static string GetShareType(UInt32 type)
        {
            string typeText = string.Empty; 
            switch (type)
            {
                case 0:
                    typeText = "Disk Drive"; 
                    break; 
                case 1:
                    typeText = "Print Queue"; 
                    break; 
                case 2:
                    typeText = "Device"; 
                    break; 
                case 3:
                    typeText = "IPC"; 
                    break; 
                case 2147483648:
                    typeText = "Disk Drive Admin"; 
                    break; 
                case 2147483649:
                    typeText = "Print Queue Admin"; 
                    break; 
                case 2147483650:
                    typeText = "Device Admin"; 
                    break; 
                case 2147483651:
                    typeText = "IPC Admin"; 
                    break; 
                default:
                    typeText = "Unknown";
                    break;
            }

            return typeText;
        }
        private static EntShare FillDetails(ManagementBaseObject obj)
        {
            EntShare objEntShare = new EntShare();

            try
            {
                objEntShare.Caption = WMIUtil.ToString(obj["Caption"]);
            }
            catch (Exception)
            {
                objEntShare.Caption = ""; 
            }

            try
            {
                objEntShare.Name = WMIUtil.ToString(obj["Name"]);
            }
            catch (Exception)
            {
                objEntShare.Name = "";
            }

            try
            {
                objEntShare.Path = WMIUtil.ToString(obj["Path"]);
            }
            catch (Exception)
            {
                objEntShare.Path = "";
            }

            try
            {
                objEntShare.Type = GetShareType((UInt32)obj["Type"]);
            }
            catch (Exception)
            {
                objEntShare.Type = "";
            }

            return objEntShare;
        }
        public static List<EntShare>GetShareDetails(ManagementScope scope)
        {
            _logger.Info("Collecting share details for machine " + scope.Path.Server);

            ObjectQuery query = null;
            ManagementObjectSearcher searcher = null;
            ManagementObjectCollection objects = null;
            List<EntShare> lstShare = new List<EntShare>();

            try
            {
                query = new ObjectQuery("Select * from Win32_Share");
                searcher = new ManagementObjectSearcher(scope, query);
                objects = searcher.Get();
                lstShare.Capacity = objects.Count;
                foreach (ManagementBaseObject obj in objects)
                {
                    lstShare.Add(FillDetails(obj));
                    obj.Dispose();
                }
            }
            catch (System.Exception e)
            {
                _logger.Error("Exception in share collection " + e.Message);
            }
            finally
            {
                searcher.Dispose();
            }
            return lstShare;
        }
    }
}
