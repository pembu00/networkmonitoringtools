﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetworkAssetManager.Entity;
using System.Management;
using log4net;

namespace NetworkAssetManager.WMI
{
    class CDRom
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(CDRom));

        private static EntCDRom FillDetails(ManagementBaseObject obj)
        {
            EntCDRom objEntCDRom = new EntCDRom();
            try
            {
                objEntCDRom.Manufacturer = WMIUtil.ToString(obj["Manufacturer"]); 
            }
            catch (Exception)
            {
                objEntCDRom.Manufacturer = "";
            }
            
            try
            {
                objEntCDRom.MediaType = WMIUtil.ToString(obj["MediaType"]);
            }
            catch (Exception)
            {
                objEntCDRom.MediaType = "";
            }

            try
            {
                objEntCDRom.Name = WMIUtil.ToString(obj["Name"]);
            }
            catch (Exception)
            {
                objEntCDRom.Name= ""; 
            }

            try
            {
                objEntCDRom.Drive = WMIUtil.ToString(obj["Drive"]);
            }
            catch (Exception)
            {
                objEntCDRom.Drive = "";
            }

            return objEntCDRom;
        }
        public static List<EntCDRom> GetCDRomDetails(ManagementScope scope)
        {

            _logger.Info("Collecting CDRom details for machine " + scope.Path.Server);

            ObjectQuery query = null;
            ManagementObjectSearcher searcher = null;
            ManagementObjectCollection objects = null;
            List<EntCDRom> lstCDRom = new List<EntCDRom>();
            try
            {
                query = new ObjectQuery("Select * from Win32_CDROMDrive");
                searcher = new ManagementObjectSearcher(scope, query);
                objects = searcher.Get();
                lstCDRom.Capacity = objects.Count;
                foreach (ManagementBaseObject obj in objects)
                {
                    lstCDRom.Add(FillDetails(obj));
                    obj.Dispose();
                }
            }
            catch (Exception e)
            {
                _logger.Error("Exception is Bios collection " + e.Message);
            }
            finally
            {
                searcher.Dispose();
            }
            return lstCDRom;
        }

    }
}
