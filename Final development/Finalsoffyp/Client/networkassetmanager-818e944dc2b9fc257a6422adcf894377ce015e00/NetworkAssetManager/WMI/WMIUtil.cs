﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using NetworkAssetManager.General;

namespace NetworkAssetManager.WMI
{
    class WMIUtil
    {
        public static string SplitCamelCase(string inputCamelCaseString)
        {
            string sTemp = Regex.Replace(inputCamelCaseString, "([A-Z][a-z])", " $1", RegexOptions.Compiled).Trim();
            return Regex.Replace(sTemp, "([A-Z][A-Z])", " $1", RegexOptions.Compiled).Trim();
        }

        public static DateTime ToShortDateTime(string dmtfDate)
        {
            DateTime initializer = DateTime.MinValue;
            int year = initializer.Year;
            int month = initializer.Month;
            int day = initializer.Day;

            string dmtf = dmtfDate;
            DateTime datetime = DateTime.MinValue;
            string tempString = string.Empty;
            if ((dmtf == null))
            {
                return DateTime.MinValue;
            }
            if ((dmtf.Length == 0))
            {
                return DateTime.MinValue;
            }
            if ((dmtf.Length != 8))
            {
                return DateTime.MinValue;
            }
            try
            {
                tempString = dmtf.Substring(0, 4);
                if (("****" != tempString))
                {
                    year = int.Parse(tempString);
                }
                tempString = dmtf.Substring(4, 2);
                if (("**" != tempString))
                {
                    month = int.Parse(tempString);
                }
                tempString = dmtf.Substring(6, 2);
                if (("**" != tempString))
                {
                    day = int.Parse(tempString);
                }
                if (year < 0 || month < 0 ||  day < 0 )
                {
                    return DateTime.MinValue;
                }
            }
            catch (Exception)
            {
                return DateTime.MinValue;
            }
            datetime = new DateTime(year, month, day, 0, 0, 0, 0);

            return datetime;
        }

        public static DateTime ToDateTime(string dmtfDate)
        {
            DateTime initializer = DateTime.MinValue;
            int year = initializer.Year;
            int month = initializer.Month;
            int day = initializer.Day;
            int hour = initializer.Hour;
            int minute = initializer.Minute;
            int second = initializer.Second;
            long ticks = 0;
            string dmtf = dmtfDate;
            DateTime datetime = DateTime.MinValue;
            string tempString = string.Empty;
            if ((dmtf == null))
            {
                return DateTime.MinValue;
            }
            if ((dmtf.Length == 0))
            {
                return DateTime.MinValue;
            }
            if ((dmtf.Length != 25))
            {
                //return DateTime.MinValue;
            }
            try
            {
                tempString = dmtf.Substring(0, 4);
                if (("****" != tempString))
                {
                    year = int.Parse(tempString);
                }
                tempString = dmtf.Substring(4, 2);
                if (("**" != tempString))
                {
                    month = int.Parse(tempString);
                }
                tempString = dmtf.Substring(6, 2);
                if (("**" != tempString))
                {
                    day = int.Parse(tempString);
                }
                tempString = dmtf.Substring(8, 2);
                if (("**" != tempString))
                {
                    hour = int.Parse(tempString);
                }
                tempString = dmtf.Substring(10, 2);
                if (("**" != tempString))
                {
                    minute = int.Parse(tempString);
                }
                tempString = dmtf.Substring(12, 2);
                if (("**" != tempString))
                {
                    second = int.Parse(tempString);
                }
                tempString = dmtf.Substring(15, 6);
                if (("******" != tempString))
                {
                    ticks = (long.Parse(tempString) * ((long)((TimeSpan.TicksPerMillisecond / 1000))));
                }
                if (((((((((year < 0)
                            || (month < 0))
                            || (day < 0))
                            || (hour < 0))
                            || (minute < 0))
                            || (minute < 0))
                            || (second < 0))
                            || (ticks < 0)))
                {
                    return DateTime.MinValue;
                }
            }
            catch (Exception)
            {
                return DateTime.MinValue;
            }
            datetime = new DateTime(year, month, day, hour, minute, second, 0);
            datetime = datetime.AddTicks(ticks);
            TimeSpan tickOffset = TimeZone.CurrentTimeZone.GetUtcOffset(datetime);
            int UTCOffset = 0;
            int OffsetToBeAdjusted = 0;
            long OffsetMins = ((long)((tickOffset.Ticks / TimeSpan.TicksPerMinute)));
            tempString = dmtf.Substring(22, 3);
            if ((tempString != "******"))
            {
                tempString = dmtf.Substring(21, 4);
                try
                {
                    UTCOffset = int.Parse(tempString);
                }
                catch (Exception)
                {
//HACK: (2009/08/06) Sometimes the UTC info in not proper so set UTC to 0 in such cases 
                    UTCOffset = 0; 
                    //return DateTime.MinValue;
                }
                OffsetToBeAdjusted = ((int)((OffsetMins - UTCOffset)));
                datetime = datetime.AddMinutes(((double)(OffsetToBeAdjusted)));
            }
            return datetime;
        }
        public static string ToString(object data)
        {
            //TODO_LOW: Need some code to remove the blank spaces in processor names 
            string retData = string.Empty; 
            if ( data != null)
            {
                retData = data.ToString();
                retData.Trim(); 
            }
            else
            {
                retData = ""; 
            }
            return retData; 

        }
        public static int ToInteger(object data)
        {
            int retData = 0;
            if (data != null)
            {
                retData = Convert.ToInt32(data);  
            }
            else
            {
                retData = 0;
            }
            return retData;
        }

        public static string ConvertSizetoString(UInt64 value, bool rounded, Units unit )
        {

            const double KB = 1024.0;
            const double MB = 1048576.0;
            const double GB = 1000000000.0;
            const double TB = 1000000000000.0;
            double dValue = 0;
            string retConv = string.Empty;
            string format = string.Empty; 
            
            if ( rounded == true )
            {
                format = "{0:0}"; 
            }
            else
            {
                format = "{0:0.##}"; 
            }

            if ( unit == Units.Auto)
            {
                if (value >= KB && value <= MB)
                {
                    dValue = (value / KB);
                    retConv = String.Format(format, dValue) + " KBs";
                }
                else if (value >= MB && value <= GB)
                {
                    dValue = (value / MB);
                    retConv = String.Format(format, dValue) + " MBs";

                }
                else if (value >= GB && value <= TB)
                {
                    dValue = (value / GB);
                    retConv = String.Format(format, dValue) + " GBs";
                }
                else if (value >= TB)
                {
                    dValue = (value / TB);
                    retConv = String.Format(format, dValue) + " KBs";
                }
                else
                {
                    retConv = value.ToString() + " bytes";
                }
            }
            else
            {
                switch (unit)
                {
                    case Units.B:
                        retConv = value.ToString() + " bytes";
                    break;
                    case Units.KB:
                        dValue = (value / KB);
                        retConv = String.Format(format, dValue) + " KBs";
                    break;
                    case Units.MB:
                        dValue = (value / MB);
                        retConv = String.Format(format, dValue) + " MBs";
                    break;
                    case Units.GB:
                        dValue = (value / GB);
                        retConv = String.Format(format, dValue) + " GBs";
                    break;
                }
            }
            return retConv;
        }

        public static string ToStringCamelCase(object data)
        {
            string retString = string.Empty; 
            if ( data == null)
            {
                return ""; 
            }
            else
            {
                retString = data.ToString();
                retString.Trim();
            }

            string sTemp = Regex.Replace(retString, "([A-Z][a-z])", " $1", RegexOptions.Compiled).Trim();
            return Regex.Replace(sTemp, "([A-Z][A-Z])", " $1", RegexOptions.Compiled).Trim();
        }

        public static string StringArrayToString(object objects )
        {
            string finalString = string.Empty; 
            string[] stringArray = (string[])objects;

            foreach (string stringElement in stringArray)
            {
                finalString += stringElement + ", "; 
            }

            finalString = finalString.TrimEnd(',', ' ');

            return finalString; 
        }


    }
}
