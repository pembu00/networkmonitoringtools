﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetworkAssetManager.Entity;
using System.Management;
using log4net; 
namespace NetworkAssetManager.WMI
{
    class Hotfix
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(Hotfix));

        private static EntHotfixes FillDetails(ManagementBaseObject obj)
        {
            EntHotfixes objEntHotfix = new EntHotfixes();
            try
            {
                objEntHotfix.Description = WMIUtil.ToString(obj["Description"]); 
            }
            catch (Exception)
            {
                objEntHotfix.Description = ""; 
            }

            try
            {
                objEntHotfix.FixComments = WMIUtil.ToString(obj["FixComments"]); 
            }
            catch (Exception)
            {
                objEntHotfix.FixComments = "";
            }

            try
            {
                objEntHotfix.HotfixID = WMIUtil.ToString(obj["HotFixID"]);
            }
            catch (Exception)
            {
                objEntHotfix.HotfixID = ""; 
            }

            try
            {
                objEntHotfix.InstallBy = WMIUtil.ToString(obj["InstalledBy"]);
            }
            catch (Exception)
            {
                objEntHotfix.InstallBy = ""; 
            }

            try
            {
                string txtSPEffect = WMIUtil.ToString(obj["ServicePackInEffect"]); 
                if ( txtSPEffect == "")
                {
                    txtSPEffect = " ";
                }
                objEntHotfix.SPEffect = txtSPEffect;
            }
            catch (Exception)
            {
                objEntHotfix.SPEffect = " "; 
            }


            return objEntHotfix; 
        }
        public static List<EntHotfixes> GetHotfixDetails(ManagementScope scope)
        {
            _logger.Info("Collecting hotfix for machine " + scope.Path.Server);

            ObjectQuery query = null;
            ManagementObjectSearcher searcher = null;
            ManagementObjectCollection objects = null;
            List<EntHotfixes> lstHotfix = new List<EntHotfixes>();

            try
            {
                query = new ObjectQuery("Select * from Win32_QuickFixEngineering");
                searcher = new ManagementObjectSearcher(scope, query);
                objects = searcher.Get();
                lstHotfix.Capacity = objects.Count;
                foreach (ManagementBaseObject obj in objects)
                {
                    lstHotfix.Add(FillDetails(obj));
                    obj.Dispose();
                }
            }
            catch (System.Exception e)
            {
                _logger.Error("Exception is hotfix collection " + e.Message); 
            }
            finally
            {
                searcher.Dispose();
            }
            return lstHotfix;
        }

    }
}
