﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetworkAssetManager.Entity;
using System.Management;
using log4net; 
namespace NetworkAssetManager.WMI
{
    class IPRoutes
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(IPRoutes));

        public enum ProtocolValues
        {
            Other=1,
            Local=2,
            Netmgmt=3,
            icmp=4,
            egp=5,
            ggp=6,
            hello=7,
            rip=8,
            is_is=9,
            es_is=10,
            CiscoIgrp=11,
            bbnSpfIgp=12,
            ospf=13,
            bgp=14
        }

        public enum RouteTypeValues
        {
            Other = 1,
            Invalid = 2,
            Direct = 3,
            Indirect = 4
        }

        private static EntIPRoutes FillDetails(ManagementBaseObject obj)
        {
            EntIPRoutes objEntIPRoutes = new EntIPRoutes();
            try
            {
                objEntIPRoutes.Age = WMIUtil.ToInteger(obj["Age"]);
            }
            catch (Exception)
            {
                objEntIPRoutes.Age = 0;
            }

            try
            {
                objEntIPRoutes.Description = WMIUtil.ToString(obj["Description"]);
            }
            catch (Exception)
            {
                objEntIPRoutes.Description =  "";
            }

            try
            {
                objEntIPRoutes.Destination = WMIUtil.ToString(obj["Destination"]);
            }
            catch (Exception)
            {
                objEntIPRoutes.Destination = "";
            }

            try
            {
                objEntIPRoutes.Mask = WMIUtil.ToString(obj["Mask"]);
            }
            catch (Exception)
            {
                objEntIPRoutes.Mask = "";
            }

            try
            {
                objEntIPRoutes.Metric1 = WMIUtil.ToInteger(obj["Metric1"]);
            }
            catch (Exception)
            {
                objEntIPRoutes.Metric1 = -1;
            }

            try
            {
                objEntIPRoutes.Metric2 = WMIUtil.ToInteger(obj["Metric2"]);
            }
            catch (Exception)
            {
                objEntIPRoutes.Metric2 = -1;
            }
            try
            {
                objEntIPRoutes.Metric3= WMIUtil.ToInteger(obj["Metric3"]);
            }
            catch (Exception)
            {
                objEntIPRoutes.Metric3 = -1;
            }
            try
            {
                objEntIPRoutes.Metric4= WMIUtil.ToInteger(obj["Metric4"]);
            }
            catch (Exception)
            {
                objEntIPRoutes.Metric4 = -1;
            }
            try
            {
                objEntIPRoutes.Metric5 = WMIUtil.ToInteger(obj["Metric5"]);
            }
            catch (Exception)
            {
                objEntIPRoutes.Metric5 = -1;
            }

            try
            {
                objEntIPRoutes.Name = WMIUtil.ToString(obj["Name"]);
            }
            catch (Exception)
            {
                objEntIPRoutes.Name = "";
            }

            try
            {
                objEntIPRoutes.NextHop = WMIUtil.ToString(obj["NextHop"]);
            }
            catch (Exception)
            {
                objEntIPRoutes.NextHop = "";
            }

            try
            {
                ProtocolValues protocol = (ProtocolValues)WMIUtil.ToInteger(obj["Protocol"]);
                objEntIPRoutes.Protocol = protocol.ToString();
            }
            catch (Exception)
            {
                objEntIPRoutes.Protocol= "Other";
            }

            try
            {
                RouteTypeValues routeType = (RouteTypeValues)WMIUtil.ToInteger(obj["Type"]);
                objEntIPRoutes.RouteType = routeType.ToString();
            }
            catch (Exception)
            {
                objEntIPRoutes.RouteType = "Other";
            }

            try
            {
                objEntIPRoutes.Status = WMIUtil.ToString(obj["Status"]);
            }
            catch (Exception)
            {
                objEntIPRoutes.Status = "";
            }

            return objEntIPRoutes;
        }
        public static List<EntIPRoutes>GetIPRoutesDetails(ManagementScope scope)
        {
            _logger.Info("Collecting IPRoutes for machine " + scope.Path.Server);

            ObjectQuery query = null;
            ManagementObjectSearcher searcher = null;
            ManagementObjectCollection objects = null;
            List<EntIPRoutes> lstIPRoutes = new List<EntIPRoutes>();

            try
            {
                query = new ObjectQuery("Select * from Win32_IP4RouteTable");
                searcher = new ManagementObjectSearcher(scope, query);
                objects = searcher.Get();
                lstIPRoutes.Capacity = objects.Count;
                foreach (ManagementBaseObject obj in objects)
                {
                    lstIPRoutes.Add(FillDetails(obj));
                    obj.Dispose();
                }
            }
            catch (System.Exception e)
            {
                _logger.Error("Exception is IPRoutes collection " + e.Message); 
            	
            }
            finally
            {
                searcher.Dispose();
            }
            return lstIPRoutes;
        }

    }
}
