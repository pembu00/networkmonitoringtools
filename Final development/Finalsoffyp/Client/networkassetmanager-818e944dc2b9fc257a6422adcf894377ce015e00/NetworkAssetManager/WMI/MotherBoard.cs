﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetworkAssetManager.Entity;
using System.Management;
using log4net; 
namespace NetworkAssetManager.WMI
{
    class MotherBoard
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(MotherBoard));

        private static EntMotherBoard FillDetails(ManagementBaseObject obj)
        {
            EntMotherBoard objEntMotherBoard = new EntMotherBoard();

            try
            {
                objEntMotherBoard.Manufacturer = WMIUtil.ToString(obj["Manufacturer"]);
            }
            catch (Exception)
            {
                objEntMotherBoard.Manufacturer = "";
            }

            try
            {
                objEntMotherBoard.Version = WMIUtil.ToString(obj["Version"]);
            }
            catch (Exception)
            {
                objEntMotherBoard.Version = "";
            }

            try
            {
                objEntMotherBoard.Product = WMIUtil.ToString(obj["Product"]);
            }
            catch (Exception)
            {
                objEntMotherBoard.Product = "";
            }

            try
            {
                objEntMotherBoard.SerialNo = WMIUtil.ToString(obj["SerialNumber"]);
            }
            catch (Exception)
            {
                objEntMotherBoard.SerialNo = "";
            }

            try
            {
                objEntMotherBoard.Model = WMIUtil.ToString(obj["Model"]);
            }
            catch (Exception)
            {
                objEntMotherBoard.Model = "";
            }

            return objEntMotherBoard;
        }
        public static List<EntMotherBoard> GetMotherBoardDetails(ManagementScope scope)
        {
            _logger.Info("Collecting monitor details for machine " + scope.Path.Server);

            ObjectQuery query = null;
            ManagementObjectSearcher searcher = null;
            ManagementObjectCollection objects = null;
            List<EntMotherBoard> lstMotherBoard = new List<EntMotherBoard>();

            try
            {
                query = new ObjectQuery("Select * from Win32_BaseBoard");
                searcher = new ManagementObjectSearcher(scope, query);
                objects = searcher.Get();
                lstMotherBoard.Capacity = objects.Count;
                foreach (ManagementBaseObject obj in objects)
                {
                    lstMotherBoard.Add(FillDetails(obj));
                    obj.Dispose();
                }
            }
            catch (System.Exception e)
            {
                _logger.Error("Exception is motherboard collection " + e.Message);
            }
            finally
            {
                searcher.Dispose();
            }
            return lstMotherBoard;
        }
    }
}
