﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetworkAssetManager.Entity;
using System.Management;
using log4net;

namespace NetworkAssetManager.WMI
{
    class Disk
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(Disk));
        private static EntDisk FillDetails(ManagementBaseObject obj)
        {
            EntDisk objEntDisk = new EntDisk();

            try
            {
                objEntDisk.BytesPerSector = WMIUtil.ToInteger(obj["BytesPerSector"]);
            }
            catch (Exception)
            {
                objEntDisk.BytesPerSector = 0;
            }
            try
            {
                objEntDisk.Caption = WMIUtil.ToString(obj["Caption"]);
            }
            catch (Exception)
            {
                objEntDisk.Caption = "";
            }
            try
            {
                objEntDisk.Cylinders = WMIUtil.ToString(obj["TotalCylinders"]);
            }
            catch (Exception)
            {
                objEntDisk.Cylinders = "";
            }

            try
            {
                objEntDisk.Heads = WMIUtil.ToInteger(obj["TotalHeads"]);
            }
            catch (Exception)
            {
                objEntDisk.Heads = 0;
            }

            try
            {
                objEntDisk.Interface = WMIUtil.ToString(obj["InterfaceType"]);
            }
            catch (Exception)
            {
                objEntDisk.Interface = "";
            }

            try
            {
                objEntDisk.Manufacturer = WMIUtil.ToString(obj["Manufacturer"]);
            }
            catch (Exception)
            {
                objEntDisk.Manufacturer = "";
            }

            try
            {
                objEntDisk.MediaType = WMIUtil.ToString(obj["MediaType"]);
            }
            catch (Exception)
            {
                objEntDisk.MediaType = "";
            }

            try
            {
                objEntDisk.Model = WMIUtil.ToString(obj["Model"]);
            }
            catch (Exception)
            {
                objEntDisk.Model = "";
            }

            try
            {
                objEntDisk.Sectors = WMIUtil.ToString(obj["TotalSectors"]);
            }
            catch (Exception)
            {
                objEntDisk.Sectors = "";
            }

            try
            {
                objEntDisk.Size = WMIUtil.ConvertSizetoString((UInt64)obj["Size"], true, NetworkAssetManager.General.Units.Auto);
            }
            catch (Exception)
            {
                objEntDisk.Size = "";
            }

            try
            {
                objEntDisk.Tracks = WMIUtil.ToString(obj["TotalTracks"]);
            }
            catch (Exception)
            {
                objEntDisk.Tracks = "";
            }
 
            return objEntDisk;
        }
        public static List<EntDisk>GetDiskDetails(ManagementScope scope)
        {
            _logger.Info("Collecting disk details for machine " + scope.Path.Server);

            ObjectQuery query = null;
            ManagementObjectSearcher searcher = null;
            ManagementObjectCollection objects = null;
            List<EntDisk> lstDisk = new List<EntDisk>();

            try
            {
                query = new ObjectQuery("Select * from Win32_DiskDrive");
                searcher = new ManagementObjectSearcher(scope, query);
                objects = searcher.Get();
                lstDisk.Capacity = objects.Count;
                foreach (ManagementBaseObject obj in objects)
                {
                    lstDisk.Add(FillDetails(obj));
                    obj.Dispose();
                }
            }
            catch (Exception e)
            {
                _logger.Error("Exception is disk collection " + e.Message);
            }
            finally
            {
                searcher.Dispose();
            }
            return lstDisk;
        }

    }
}
