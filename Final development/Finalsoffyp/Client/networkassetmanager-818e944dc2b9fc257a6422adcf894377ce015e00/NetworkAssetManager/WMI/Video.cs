﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetworkAssetManager.Entity;
using System.Management;
using log4net; 
namespace NetworkAssetManager.WMI
{
    class Video
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(Video));

        private static EntVideo FillDetails(ManagementBaseObject obj)
        {
            EntVideo objEntVideo = new EntVideo();

            try
            {
                objEntVideo.CurrentBitsperPixel = WMIUtil.ToInteger(obj["CurrentBitsPerPixel"]); 
            }
            catch (Exception)
            {
                objEntVideo.CurrentBitsperPixel = 0; 
            }

            try
            {
                string _value = string.Empty; 
                _value = WMIUtil.ToString(obj["CurrentRefreshRate"]);
                if ( _value == "")
                {
                    _value = "0";
                }
                _value = _value + " Hz"; 
                objEntVideo.CurrentRefreshRate = _value; 
            }
            catch (Exception)
            {
                objEntVideo.CurrentRefreshRate = "0 Hz"; 
            }
            try
            {
                objEntVideo.HorizontalResolution = WMIUtil.ToInteger(obj["CurrentHorizontalResolution"]); 
            }
            catch (Exception)
            {
                objEntVideo.HorizontalResolution = 0; 
            }

            try
            {
                objEntVideo.VerticalResolution = WMIUtil.ToInteger(obj["CurrentVerticalResolution"]); 
            }
            catch (Exception)
            {
                objEntVideo.VerticalResolution = 0; 
            }

            try
            {
                objEntVideo.MemorySize = WMIUtil.ConvertSizetoString((UInt32)obj["AdapterRAM"], true, NetworkAssetManager.General.Units.Auto);
            }
            catch (Exception)
            {
                objEntVideo.MemorySize = "0 Mb"; 
            }

            try
            {
                objEntVideo.Name = WMIUtil.ToString(obj["Name"]);
            }
            catch (Exception)
            {
                objEntVideo.Name = "";
            }
                         
            return objEntVideo;
        }
        public static List<EntVideo>GetVideoDetails(ManagementScope scope)
        {
            _logger.Info("Collecting video details for machine " + scope.Path.Server);

            ObjectQuery query = null;
            ManagementObjectSearcher searcher = null;
            ManagementObjectCollection objects = null;
            List<EntVideo> lstVideo = new List<EntVideo>();

            try
            {
                query = new ObjectQuery("Select * from Win32_VideoController");
                searcher = new ManagementObjectSearcher(scope, query);
                objects = searcher.Get();
                lstVideo.Capacity = objects.Count;
                foreach (ManagementBaseObject obj in objects)
                {
                    lstVideo.Add(FillDetails(obj));
                    obj.Dispose();
                }
            }
            catch (System.Exception e)
            {
                _logger.Error("Exception in video collection " + e.Message);
            }
            finally
            {
                searcher.Dispose();
            }
            return lstVideo;
        }
    }
}
