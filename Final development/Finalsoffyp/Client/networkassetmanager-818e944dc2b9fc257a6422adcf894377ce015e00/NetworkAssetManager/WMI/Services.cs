﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetworkAssetManager.Entity;
using System.Management;
using log4net; 
namespace NetworkAssetManager.WMI
{
    class Services
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(Services));

        private static EntServices FillDetails(ManagementBaseObject obj)
        {
            EntServices objEntServices = new EntServices();
            try
            {
                objEntServices.DisplayName = WMIUtil.ToString(obj["DisplayName"]);
            }
            catch (Exception)
            {
                objEntServices.DisplayName = "";
            }

            try
            {
                objEntServices.Account = WMIUtil.ToString(obj["StartName"]);
            }
            catch (Exception)
            {
                objEntServices.Account = "";
            }

            try
            {
                objEntServices.Description = WMIUtil.ToString(obj["Description"]);
            }
            catch (Exception)
            {
                objEntServices.Description = "";
            }

            try
            {
                objEntServices.Path = WMIUtil.ToString(obj["PathName"]);
            }
            catch (Exception)
            {
                objEntServices.Path= "";
            }

            try
            {
                objEntServices.StartMode = WMIUtil.ToString(obj["StartMode"]);
            }
            catch (Exception)
            {
                objEntServices.StartMode = "";
            }

            try
            {
                objEntServices.State = WMIUtil.ToString(obj["State"]);
            }
            catch (Exception)
            {
                objEntServices.State = "";
            }

            return objEntServices;
        }
        public static List<EntServices> GetServicesDetails(ManagementScope scope)
        {
            _logger.Info("Collecting processor details for machine " + scope.Path.Server);

            ObjectQuery query = null;
            ManagementObjectSearcher searcher = null;
            ManagementObjectCollection objects = null;
            List<EntServices> lstServices = new List<EntServices>();

            try
            {
                query = new ObjectQuery("Select * from Win32_Service");
                searcher = new ManagementObjectSearcher(scope, query);
                objects = searcher.Get();
                lstServices.Capacity = objects.Count;
                foreach (ManagementBaseObject obj in objects)
                {
                    lstServices.Add(FillDetails(obj));
                    obj.Dispose();
                }
            }
            catch (System.Exception e)
            {
                _logger.Error("Exception in processor collection " + e.Message);
            }
            finally
            {
                searcher.Dispose();
            }
            return lstServices;
        }

    }
}
