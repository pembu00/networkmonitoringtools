﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetworkAssetManager.Entity;
using System.Management;
using log4net; 

namespace NetworkAssetManager.WMI
{
    class Processor
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(Processor));

        private static string GetArchitecture(object data)
        {
            string retData = string.Empty; 
            switch (WMIUtil.ToInteger(data))
            {
                case 0:
                    retData = "x86"; 
            	break;
                case 1:
                    retData = "MIPS"; 
            	break;
                case 2:
                    retData = "Alpha"; 
            	break;
                case 3:
                    retData = "PowerPC"; 
            	break;
                case 6:
                    retData = "Intel Itanium Processor Family (IPF)"; 
            	break;
                case 9:
                    retData = "x64"; 
            	break;
                default:
                    retData = "Unknown Architecture";
                break;
            }
            return retData; 

        }
        private static EntProcessor FillDetails(ManagementBaseObject obj)
        {
            EntProcessor objEntProcessor    = new EntProcessor();

            try
            {
                objEntProcessor.DeviceID = WMIUtil.ToString(obj["DeviceID"]);
            }
            catch (Exception)
            {
                objEntProcessor.DeviceID = ""; 
            }

            try
            {
                objEntProcessor.Name = WMIUtil.ToString(obj["Name"]);
            }
            catch (Exception)
            {
                objEntProcessor.Name = ""; 
            }

            try
            {
                objEntProcessor.Manufacturer = WMIUtil.ToString(obj["Manufacturer"]);
            }
            catch (Exception)
            {
                objEntProcessor.Manufacturer = "";
            }

            try
            {
                objEntProcessor.ClockSpeed = WMIUtil.ToString(obj["MaxClockSpeed"]);
            }
            catch (Exception)
            {
                objEntProcessor.ClockSpeed = "";
            }

            try
            {
                objEntProcessor.Architecture = GetArchitecture(obj["Architecture"]);
            }
            catch (Exception)
            {
                objEntProcessor.Architecture = "";
            }

            try
            {
                objEntProcessor.L2CacheSize = WMIUtil.ToInteger(obj["L2CacheSize"]);
            }
            catch (Exception)
            {
                objEntProcessor.L2CacheSize = 0;
            }

            try
            {
                objEntProcessor.L2CacheSpeed = WMIUtil.ToInteger(obj["L2CacheSpeed"]);
            }
            catch (Exception)
            {
                objEntProcessor.L2CacheSpeed = 0;
            }

            try
            {
                objEntProcessor.SocketType = WMIUtil.ToString(obj["SocketDesignation"]);
            }
            catch (Exception)
            {
                objEntProcessor.SocketType = "";
            }

            try
            {
                objEntProcessor.Version = WMIUtil.ToString(obj["Version"]);
            }
            catch (Exception)
            {
                objEntProcessor.Version = "";
            }

            return objEntProcessor;
        }
        public static List<EntProcessor> GetProcessorDetails(ManagementScope scope)
        {
            _logger.Info("Collecting processor details for machine " + scope.Path.Server);

            ObjectQuery query = null;
            ManagementObjectSearcher searcher = null;
            ManagementObjectCollection objects = null;
            List<EntProcessor> lstProcessor = new List<EntProcessor>();

            try
            {
                query = new ObjectQuery("Select * from Win32_Processor");
                searcher = new ManagementObjectSearcher(scope, query);
                objects = searcher.Get();
                lstProcessor.Capacity = objects.Count;
                foreach (ManagementBaseObject obj in objects)
                {
                    lstProcessor.Add(FillDetails(obj));
                    obj.Dispose();
                }
            }
            catch (System.Exception e)
            {
                _logger.Error("Exception in processor collection " + e.Message);
            }
            finally
            {
                searcher.Dispose();
            }
            return lstProcessor;
        }
    }
}
