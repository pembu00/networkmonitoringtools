﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


using NetworkAssetManager.Entity;
using NetworkAssetManager.General;
namespace NetworkAssetManager.Forms
{
    public partial class FrmCredentials : Form
    {
        private List<EntCredential> _lstData = null;
        private bool _IsChanged = false;

        int selItem = 0; 

        public bool IsChanged 
        {
            get
            {
                return _IsChanged;
            }
        }
        public List<EntCredential>UpdatedList
        {
            get
            {
                return _lstData; 
            }
        }
        public FrmCredentials()
        {
            InitializeComponent();
            _lstData = new List<EntCredential>();
        }

        private void LoadCredentials() 
        {
            List<EntCredential>_lstCredentials = Controller.Instance.GetCredentials();
            lstCredential.Items.Clear();
            ListViewItem item = null; 
            foreach (EntCredential cred in _lstCredentials)
            {
                item = new ListViewItem();
                item.Text = cred.CredentialName;
                item.Tag = cred;
                lstCredential.Items.Add(item);
                item = null; 
            }
        }
        private void FrmCredentials_Load(object sender, EventArgs e)
        {
            if ( _lstData.Count> 0)
            {
                _lstData.Clear();
            }
            LoadCredentials(); 

            if ( lstCredential.Items.Count > 0 ) 
            {
                lstCredential.Items[0].Selected = true; 
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            ClearFields(); 
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            bool bUpdate = false;
            EntCredential found  = null;
            if ( txtCredName.Text == "" || txtCredUsername.Text == "")
            {
                MessageBox.Show("Credential name and Username cannot be blank"); 
                return; 
            }


            foreach (ListViewItem item in lstCredential.Items)
            {
                found = (EntCredential)item.Tag; 
                if ( found.CredentialName == txtCredName.Text)
                {
                    bUpdate = true;
                    break; 
                }
            }

            if (bUpdate == true && found != null)
            {
                found.Username = txtCredUsername.Text;
                found.Password = txtCredPassword.Text;
                found.Operation = DBOperations.Update; 
                _lstData.Add(found);
                selItem = lstCredential.SelectedIndices[0];

            }
            else
            {
                EntCredential newCred = new EntCredential();
                newCred.CredentialName = txtCredName.Text;
                newCred.Username = txtCredUsername.Text;
                newCred.Password = txtCredPassword.Text;
                newCred.Operation = DBOperations.Insert;
                _lstData.Add(newCred);

                ListViewItem temp = new ListViewItem();
                temp.Text = txtCredName.Text;
                temp.Tag = newCred;
                lstCredential.Items.Add(temp);
                temp = null;
                selItem = lstCredential.Items.Count-1;  
            }
            lstCredential.Items[selItem].Selected = true;
            //ClearFields();
        }

        private void ClearFields()
        {
            txtCredName.Text = string.Empty;
            txtCredUsername.Text = string.Empty;
            txtCredPassword.Text = string.Empty;
            txtCredName.Focus(); 
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            EntCredential found = null; 
            foreach (ListViewItem item in lstCredential.Items)
            {
                if ( item.Text == txtCredName.Text)
                {
                    if (MessageBox.Show("Are you sure you want to delete this credential", "Remove credential", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        found = (EntCredential)item.Tag; 
                        found.Operation = DBOperations.Delete;
                        _lstData.Add(found);

                        if ( item.Index != 0)
                        {
                            lstCredential.Items[item.Index - 1].Selected = true;
                        }
                        else if (item.Index == 0 && lstCredential.Items.Count > 1 )
                        {
                            lstCredential.Items[item.Index + 1].Selected = true;
                        }
                        lstCredential.Items.Remove(item);

                        if ( lstCredential.Items.Count<= 0 )
                        {
                            ClearFields();
                        }

                        break;

                    }
                }
            }
        }

        private void lstCredential_SelectedIndexChanged(object sender, EventArgs e)
        {
/*
            if (lstCredential.SelectedItems.Count > 0)
            {
                EntCredential found = (EntCredential)lstCredential.SelectedItems[0].Tag;

                if (found != null)
                {
                    txtCredName.Text = found.CredentialName;
                    txtCredUsername.Text = found.Username;
                    txtCredPassword.Text = found.Password;
                }
            }
*/
        }

        private void lstCredential_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            int a = e.ItemIndex;
            bool r = e.IsSelected;
            EntCredential found = (EntCredential)e.Item.Tag;

            if (found != null)
            {
                txtCredName.Text = found.CredentialName;
                txtCredUsername.Text = found.Username;
                txtCredPassword.Text = found.Password;
            }
        }
    }
}
