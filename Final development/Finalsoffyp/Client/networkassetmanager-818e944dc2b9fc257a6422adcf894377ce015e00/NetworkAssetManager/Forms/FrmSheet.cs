﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NetworkAssetManager.DataAccess;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml.XPath;
using System.Xml.Xsl;
using System.Xml;

namespace NetworkAssetManager.Forms
{
    public partial class FrmSheet : Form
    {

        DataSet _dsResult = null; 


        public DataSet ResultDataSet
        {
            set
            {
                _dsResult = value;
            }
        }

        public FrmSheet()
        {
            InitializeComponent();
        }

        private void FrmSheet_Load(object sender, EventArgs e)
        {
            object empty = System.Reflection.Missing.Value;
            axWebBrowser1.Navigate("about:blank", false);

            XslCompiledTransform xslt = new XslCompiledTransform();
            xslt.Load(Application.StartupPath + "\\xslt\\NamHtml.xsl");

            StringReader xmlStringReader = new StringReader(_dsResult.GetXml());
            XPathDocument myXPathDoc = new XPathDocument(xmlStringReader);

            StringWriter sw = new StringWriter();
            XmlWriter xmlWriter = new XmlTextWriter(sw);

            // using makes sure that we flush the writer at the end
            xslt.Transform(myXPathDoc, null, xmlWriter);
            xmlWriter.Flush();
            xmlWriter.Close();

            string xml = sw.ToString();

            HtmlDocument htmlDoc = axWebBrowser1.Document;
            htmlDoc.Write(xml);
        }

        // readStream is the stream you need to read
        // writeStream is the stream you want to write to
        private void ReadWriteStream(Stream readStream, Stream writeStream) 
        {
            int Length = 256;
            Byte [] buffer = new Byte[Length];
            int bytesRead = readStream.Read(buffer,0,Length);
            // write the required bytes
            while( bytesRead > 0 ) 
            {
                writeStream.Write(buffer,0,bytesRead);
                bytesRead = readStream.Read(buffer,0,Length);
            }
            readStream.Close();
            writeStream.Close();
        } 


        private void HTMLFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveDlg = new SaveFileDialog();
            saveDlg.Filter = "HTML file (*.html)|*.html"; 
            if ( saveDlg.ShowDialog() == DialogResult.OK)
            {
                string fileName = saveDlg.FileName;
                FileStream writeStream = new FileStream(fileName, FileMode.Create, FileAccess.Write);
                ReadWriteStream(axWebBrowser1.DocumentStream, writeStream);
                MessageBox.Show("File saved to " + fileName, "HTML report saved"); 
            }
        }

        private void printToolStripMenuItem_Click(object sender, EventArgs e)
        {
            axWebBrowser1.ShowPrintDialog();
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close(); 
        }

        private void xMLFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveDlg = new SaveFileDialog();
            saveDlg.Filter = "XML file (*.xml)|*.xml";
            if (saveDlg.ShowDialog() == DialogResult.OK)
            {
                string fileName = saveDlg.FileName;
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(_dsResult.GetXml());
                xmlDoc.Save(saveDlg.FileName);
                MessageBox.Show("File saved to " + fileName, "XML report saved"); 
            }
        }
    }
}

