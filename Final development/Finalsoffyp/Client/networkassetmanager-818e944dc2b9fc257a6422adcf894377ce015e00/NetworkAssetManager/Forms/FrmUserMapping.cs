﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NetworkAssetManager.Entity;
using NetworkAssetManager;
using System.Collections; 
namespace NetworkAssetManager.Forms
{
    public partial class FrmUserMapping : Form
    {
        private ListViewItem lvItem;
        Hashtable hsCredentialsCredID = null;
        Hashtable hsCredentialsName = null; 
        public FrmUserMapping()
        {
            InitializeComponent();
        }

        private void LoadCredentials()
        {
            hsCredentialsCredID = new Hashtable();
            hsCredentialsName = new Hashtable();

            if ( cbUserName.Items.Count> 0 )
            {
                cbUserName.Items.Clear();
            }

            List<EntCredential>lsCredential = Controller.Instance.GetCredentials();

            foreach (EntCredential cred in lsCredential)
            {
                cbUserName.Items.Add(cred.CredentialName);
                hsCredentialsCredID.Add(cred.CredentialID, cred);
                hsCredentialsName.Add(cred.CredentialName, cred); 
            }
        }

        private void LoadMachines()
        {
            ListViewItem listviewitem = null;
            lstMachineMap.View = View.Details;
            lstMachineMap.FullRowSelect = true;
            if ( lstMachineMap.Items.Count > 0 ) 
            {
                lstMachineMap.Items.Clear(); 
            }

            List<EntDiscover> lsMachines = Controller.Instance.GetMachineList();

            foreach (EntDiscover mach in lsMachines)
            {
                EntCredential tempCred = (EntCredential)hsCredentialsCredID[mach.CredentialID];
                listviewitem = new ListViewItem();
                listviewitem.Text = mach.MachineName;
                listviewitem.Tag = mach; 
                if ( tempCred!= null)
                {
                    listviewitem.SubItems.Add(tempCred.CredentialName);
                }
                else
                {
                    listviewitem.SubItems.Add("");
                }
                lstMachineMap.Items.Add(listviewitem);
                listviewitem = null; 

            }

            
            foreach (ColumnHeader ch in this.lstMachineMap.Columns)
            {
                ch.Width = (lstMachineMap.Width-4)/2;
            }

        }

        private void FrmUserMapping_Load(object sender, EventArgs e)
        {
            LoadCredentials(); 
            LoadMachines(); 
        }

        private void cbUserName_SelectedValueChanged(object sender, EventArgs e)
        {
            //lvItem.SubItems[1].Text = cbUserName.Text;
            //cbUserName.Visible = false;

        }

        private void lstMachineMap_KeyPress(object sender, KeyPressEventArgs e)
        {
            switch (e.KeyChar)
            {
                case (char)(int)Keys.Escape:
                    {
                        // Reset the original text value, and then hide the ComboBox.
                        cbUserName.Text = lvItem.SubItems[1].Text;
                        cbUserName.Visible = false;
                        break;
                    }

                case (char)(int)Keys.Enter:
                    {
                        // Hide the ComboBox.
                        cbUserName.Visible = false;
                        break;
                    }
            }

        }

        private void lstMachineMap_MouseUp(object sender, MouseEventArgs e)
        {
            lvItem = lstMachineMap.GetItemAt(e.X, e.Y);

            

            // Make sure that an item is clicked.
            if (lvItem != null)
            {
                // Get the bounds of the item that is clicked.
                Rectangle ClickedItem = lvItem.Bounds;

                // Verify that the column is completely scrolled off to the left.
                if ((ClickedItem.Left + lstMachineMap.Columns[0].Width) < 0)
                {
                    // If the cell is out of view to the left, do nothing.
                    return;
                }

                // Verify that the column is partially scrolled off to the left.
                else if (ClickedItem.Left < 0)
                {
                    // Determine if column extends beyond right side of ListView.
                    if ((ClickedItem.Left + lstMachineMap.Columns[0].Width) > lstMachineMap.Width)
                    {
                        // Set width of column to match width of ListView.
                        ClickedItem.Width = lstMachineMap.Width;
                        ClickedItem.X = 0;
                    }
                    else
                    {
                        // Right side of cell is in view.
                        ClickedItem.Width = lstMachineMap.Columns[0].Width + ClickedItem.Left;
                        ClickedItem.X = 2;
                    }
                }
                else if (lstMachineMap.Columns[0].Width > lstMachineMap.Width)
                {
                    ClickedItem.Width = lstMachineMap.Width;
                }
                else
                {
                    ClickedItem.Width = lstMachineMap.Columns[0].Width;
                    ClickedItem.X = 2;
                }

                // Adjust the top to account for the location of the ListView.
                ClickedItem.Y += lstMachineMap.Top;
                ClickedItem.X += lstMachineMap.Left+lstMachineMap.Columns[0].Width;

                // Assign calculated bounds to the ComboBox.
                cbUserName.Bounds = ClickedItem;

                // Set default text for ComboBox to match the item that is clicked.
                cbUserName.Text = lvItem.SubItems[1].Text;

                // Display the ComboBox, and make sure that it is on top with focus.
                cbUserName.Visible = true;
                cbUserName.BringToFront();
                cbUserName.Focus();
            }


        }

        private void cbUserName_Leave(object sender, EventArgs e)
        {
            lvItem.SubItems[1].Text = this.cbUserName.Text;

            // Hide the ComboBox.
            this.cbUserName.Visible = false;

        }

        private void btnCommit_Click(object sender, EventArgs e)
        {
            List<EntDiscover> lstDiscover = new List<EntDiscover>();
            foreach (ListViewItem item in lstMachineMap.Items)
            {
                string credName = item.SubItems[1].Text;
                EntCredential mappedCred = (EntCredential)hsCredentialsName[credName];
                EntDiscover machine = (EntDiscover)item.Tag;
                if (machine!=null && mappedCred!=null)
                {
                    if (machine.CredentialID != mappedCred.CredentialID)
                    {
                        machine.CredentialID = mappedCred.CredentialID;
                        lstDiscover.Add(machine);
                    }
                }
            }

            if ( lstDiscover.Count > 0 )
            {
                Controller.Instance.UpdateDiscoveryMachines(lstDiscover);
                MessageBox.Show("Credential Mapping complete");
            }
        }
    }
}
