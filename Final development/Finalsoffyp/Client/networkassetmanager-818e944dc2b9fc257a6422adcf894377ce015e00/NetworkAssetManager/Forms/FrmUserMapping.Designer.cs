﻿using NetworkAssetManager.General;
namespace NetworkAssetManager.Forms
{
    partial class FrmUserMapping
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCommit = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.cbUserName = new System.Windows.Forms.ComboBox();
            this.lstMachineMap = new NetworkAssetManager.General.ListViewCombo();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
            this.SuspendLayout();
            // 
            // btnCommit
            // 
            this.btnCommit.Location = new System.Drawing.Point(63, 292);
            this.btnCommit.Name = "btnCommit";
            this.btnCommit.Size = new System.Drawing.Size(75, 23);
            this.btnCommit.TabIndex = 1;
            this.btnCommit.Text = "Commit";
            this.btnCommit.UseVisualStyleBackColor = true;
            this.btnCommit.Click += new System.EventHandler(this.btnCommit_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(247, 292);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Close";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // cbUserName
            // 
            this.cbUserName.FormattingEnabled = true;
            this.cbUserName.Location = new System.Drawing.Point(159, 294);
            this.cbUserName.Name = "cbUserName";
            this.cbUserName.Size = new System.Drawing.Size(73, 21);
            this.cbUserName.TabIndex = 3;
            this.cbUserName.Visible = false;
            this.cbUserName.Leave += new System.EventHandler(this.cbUserName_Leave);
            this.cbUserName.SelectedValueChanged += new System.EventHandler(this.cbUserName_SelectedValueChanged);
            // 
            // lstMachineMap
            // 
            this.lstMachineMap.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.lstMachineMap.Location = new System.Drawing.Point(12, 12);
            this.lstMachineMap.Name = "lstMachineMap";
            this.lstMachineMap.Size = new System.Drawing.Size(362, 268);
            this.lstMachineMap.TabIndex = 0;
            this.lstMachineMap.UseCompatibleStateImageBehavior = false;
            this.lstMachineMap.MouseUp += new System.Windows.Forms.MouseEventHandler(this.lstMachineMap_MouseUp);
            this.lstMachineMap.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.lstMachineMap_KeyPress);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Machine Name";
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Credential";
            // 
            // FrmUserMapping
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(386, 323);
            this.Controls.Add(this.cbUserName);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnCommit);
            this.Controls.Add(this.lstMachineMap);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "FrmUserMapping";
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Map credential";
            this.Load += new System.EventHandler(this.FrmUserMapping_Load);
            this.ResumeLayout(false);

        }

        #endregion


        private ListViewCombo lstMachineMap;
        private System.Windows.Forms.Button btnCommit;
        private System.Windows.Forms.ComboBox cbUserName;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.Button btnCancel;
    }
}