﻿using System;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections; 

using NetworkAssetManager.General;

namespace NetworkAssetManager.Forms
{
    public partial class FrmAddByDomain : Form
    {
        public FrmAddByDomain()
        {
            InitializeComponent();
        }
        public string DomainName 
        {
            get
            {
                return cbDomainName.Text; 
            }
        }

        private void FrmAddByDomain_Load(object sender, EventArgs e)
        {
            Controller.Instance.DomainDiscoveryStart += new Controller.DelegateDomainDiscoveryStart(Instance_DomainDiscoveryStart);
            Controller.Instance.DomainDiscoveryComplete += new Controller.DelegateDomainDiscoveryComplete(Instance_DomainDiscoveryComplete);
            Controller.Instance.DomainFound += new Controller.DelegateDomainFound(Instance_DomainFound);
            Controller.Instance.GetDomains();
        }

        void Instance_DomainDiscoveryComplete(object sender, EventArgs args)
        {
            SetSpinningProgressBarState(false);
            this.cbDomainName.SafeControlInvoke
            (
                cbDomainName =>
                {
                    cbDomainName.Items.Add("ALL MACHINES");
                    if (cbDomainName.Items.Count > 0)
                    {
                        cbDomainName.SelectedIndex = 0;
                    }
                }
            );
        }

        void Instance_DomainDiscoveryStart(object sender, DiscoveryDetailsArgs args)
        {
            SetSpinningProgressBarState(true);
            this.cbDomainName.SafeControlInvoke
            (
                cbDomainName =>
                {
                    cbDomainName.Items.Clear();
                }
            );
        }

        void Instance_DomainFound(object sender, DomainDetailsArgs args)
        {
            this.cbDomainName.SafeControlInvoke
            (
                cbDomainName =>
                {
                    cbDomainName.Items.Add(args.DomainName);
                }
            );
        }

        private void SetSpinningProgressBarState(bool state)
        {
            this.progBar.SafeControlInvoke
            (
                progBar =>
                {
                    if ( state == true)
                    {
                        progBar.Style = ProgressBarStyle.Marquee;
                        progBar.MarqueeAnimationSpeed = 70;
                    }
                    else
                    {
                        progBar.Style = ProgressBarStyle.Blocks;
                    }
                }
            );
        }

        private void FrmAddByDomain_FormClosing(object sender, FormClosingEventArgs e)
        {
            Controller.Instance.DomainDiscoveryStart -= new Controller.DelegateDomainDiscoveryStart(Instance_DomainDiscoveryStart); 
            Controller.Instance.DomainFound -= new Controller.DelegateDomainFound(Instance_DomainFound);
            Controller.Instance.DomainDiscoveryComplete -= new Controller.DelegateDomainDiscoveryComplete(Instance_DomainDiscoveryComplete);

        }
    }
}
