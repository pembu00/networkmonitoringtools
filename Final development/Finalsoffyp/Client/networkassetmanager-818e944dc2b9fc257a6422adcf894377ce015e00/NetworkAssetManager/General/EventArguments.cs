﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetworkAssetManager.General
{
    public class MachineDetailsArgs : EventArgs
    {
        public string MachineName;
        public string IPAddress;
        public string DomainName;
        public int CredentialID;
        public string StatusMessage;
        public DateTime LastChecked;
        public bool Discovered;
    }
    public class DomainDetailsArgs : EventArgs
    {
        public string DomainName;
    }

    public class DiscoveryDetailsArgs : EventArgs
    {
        public string Message;
    }
}
