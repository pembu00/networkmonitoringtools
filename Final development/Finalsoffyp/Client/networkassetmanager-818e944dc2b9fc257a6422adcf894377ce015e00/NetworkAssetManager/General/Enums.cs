﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetworkAssetManager.General
{
    public enum DBCode
    {
        Error,
        Ok
    }

    public enum ImageCode
    {
        MachineOk,
        MachineNotOk,
        Processor,
        OS,
        Bios,
        Motherboard,
        Disk,
        Memory,
        LogicalDrive,
        CDRom,
        Video, 
        Multimedia, 
        Monitor,
        Share,
        StartUp,
        Hotfix,
        Processes,
        Softwares,
        Services,
        IPRoutes,
        EnvironmentVar,
        Computer,
        Printer,
        UserGroup
    }

    public enum EntityType
    {
        Processor,
        OS,
        Bios,
        MotherBoard,
        Disk,
        Memory, 
        LogicalDrive,
        CDRom, 
        Video,
        Multimedia, 
        Monitor,
        Share,
        StartUp,
        Hotfix,
        Processes,
        Softwares,
        Services,
        IPRoutes,
        EnvironmentVar,
        Computer,
        Printer,
        UserGroup
    }

    public enum Units
    {
        B,
        KB,
        MB,
        GB,
        Auto
    }

    public enum DBOperations
    {
        Insert, 
        Delete, 
        Update
    }
      



}
