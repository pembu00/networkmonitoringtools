﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetworkAssetManager.General;

namespace NetworkAssetManager.Entity
{
    public class EntSoftwares : IEntity
    {
        private int _ScanID = 0; 
        private string _Name = string.Empty;
        private string _Version = string.Empty;
        DateTime _InstallDate = DateTime.MinValue;
        private string _Location = string.Empty;
        private string _ClassName = "Softwares";

        public string NodeName
        {
            get
            {
                return _Name + " " + _Version;
            }
        }

        public int ScanID
        {
            set
            {
                _ScanID = value;
            }
            get
            {
                return _ScanID;
            }
        }

        public string Name
        {
            set
            {
                _Name = value;
            }
            get
            {
                return _Name;
            }
        }

        public string Version
        {
            set
            {
                _Version = value;
            }
            get
            {
                return _Version;
            }
        }

        public DateTime InstallDate
        {
            set
            {
                _InstallDate = value;
            }
            get
            {
                return _InstallDate;
            }
        }
        public string Location
        {
            set
            {
                _Location = value;
            }
            get
            {
                return _Location;
            }
        }

        public string ClassName
        {
            get
            {
                return _ClassName;
            }
        }

        public ImageCode Icon
        {
            get
            {
                return ImageCode.Bios;
            }
        }
    }
}
