﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetworkAssetManager.General;

namespace NetworkAssetManager.Entity
{
    public class EntVideo : IEntity
    {
        private int _ScanID = 0;
        private string _Name = string.Empty;
        private int _HorizontalResolution = 0;
        private int _VerticalResolution = 0;
        private int _CurrentBitsperPixel = 0;
        private string _CurrentRefreshRate = string.Empty;
        private string _MemorySize = string.Empty;
        private string _ClassName = "Video";

        public string NodeName
        {
            get
            {
                return _Name + " (" + _HorizontalResolution.ToString()+"X"+_VerticalResolution.ToString()+")";
            }
        }

        public int ScanID
        {
            get
            {
                return _ScanID;
            }

            set
            {
                _ScanID = value;
            }
        }
        public string Name
        {
            get
            {
                return _Name;
            }

            set
            {
                _Name = value;
            }
        }
        public int HorizontalResolution
        {
            get
            {
                return _HorizontalResolution;
            }

            set
            {
                _HorizontalResolution = value;
            }
        }
        public int VerticalResolution
        {
            get
            {
                return _VerticalResolution;
            }

            set
            {
                _VerticalResolution = value;
            }
        }
        public int CurrentBitsperPixel
        {
            get
            {
                return _CurrentBitsperPixel;
            }

            set
            {
                _CurrentBitsperPixel = value;
            }
        }
        public string CurrentRefreshRate
        {
            get
            {
                return _CurrentRefreshRate;
            }

            set
            {
                _CurrentRefreshRate = value;
            }
        }
        public string MemorySize
        {
            get
            {
                return _MemorySize;
            }

            set
            {
                _MemorySize = value;
            }
        }

        public string ClassName
        {
            get
            {
                return _ClassName;
            }
        }

        public ImageCode Icon
        {
            get
            {
                return ImageCode.Video;
            }
        }
    }
}