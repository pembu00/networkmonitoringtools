﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetworkAssetManager.General;

namespace NetworkAssetManager.Entity
{
    public class EntBios : IEntity
    {
        private int _ScanID = 0; 
        private string _Manufacturer = string.Empty;
        private string _Version = string.Empty;
        DateTime _ReleaseDate = DateTime.MinValue;
        private string _ClassName = "Bios";

        public string NodeName
        {
            get
            {
                return _Manufacturer + " " + Version;
            }
        }

        public int ScanID
        {
            set
            {
                _ScanID = value;
            }
            get
            {
                return _ScanID;
            }
        }

        public string Manufacturer
        {
            set
            {
                _Manufacturer = value;
            }
            get
            {
                return _Manufacturer;
            }
        }

        public string Version
        {
            set
            {
                _Version = value;
            }
            get
            {
                return _Version;
            }
        }

        public DateTime ReleaseDate
        {
            set
            {
                _ReleaseDate = value;
            }
            get
            {
                return _ReleaseDate;
            }
        }
        public string ClassName
        {
            get
            {
                return _ClassName;
            }
        }

        public ImageCode Icon
        {
            get
            {
                return ImageCode.Bios;
            }
        }
    }
}
