﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetworkAssetManager.Entity
{
    public class EntDiscover
    {
        #region Internal private variables
        private int _MachineID = 0;
        private string _IPAddress = string.Empty;
        private DateTime _LastChecked ;
        private bool   _Discovered = false;
        private string _StatusMessage = string.Empty;
        private string _DomainName = string.Empty;
        private string _MachineName = string.Empty;
        private int _CredentialID = 0; 

        #endregion

        #region Properties for the Discover object
        public int MachineID
        {
            get
            {
                return _MachineID;
            }
            set
            {
                _MachineID = value;
            }

        }
        public int CredentialID
        {
            get
            {
                return _CredentialID;
            }
            set
            {
                _CredentialID = value;
            }

        }

        public string IPAddr
        {
            get
            {
                return _IPAddress;
            }
            set
            {
                _IPAddress = value;

            }
        }
        public DateTime LastChecked
        {
            get
            {
                return _LastChecked;
            }
            set 
            {
                _LastChecked = value; 
            }
        }
        public bool Discovered
        {
            get
            {
                return _Discovered;
            }
            set
            {
                _Discovered = value;

            }
        }
        public string StatusMessage
        {
            get
            {
                return _StatusMessage;
            }
            set
            {
                _StatusMessage = value;

            }
        }
        public string DomainName
        {
            get
            {
                return _DomainName;
            }
            set
            {
                _DomainName = value;

            }
        }
        public string MachineName
        {
            get
            {
                return _MachineName;
            }
            set
            {
                _MachineName= value;

            }
        }

        #endregion 

    }
}
