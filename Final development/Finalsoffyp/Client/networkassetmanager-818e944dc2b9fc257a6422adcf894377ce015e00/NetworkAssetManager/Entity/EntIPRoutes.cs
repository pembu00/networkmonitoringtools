﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetworkAssetManager.General;

namespace NetworkAssetManager.Entity
{
    public class EntIPRoutes : IEntity
    {
		private int _ScanID = 0;
        private int _Age = 0;
        private string _Name = string.Empty;
        private string _Description = string.Empty;
        private string _Destination = string.Empty;
        private string _Mask = string.Empty;
        private int _Metric1 = 0;
        private int _Metric2 = 0;
        private int _Metric3 = 0;
        private int _Metric4 = 0;
        private int _Metric5 = 0;
        private string _NextHop = string.Empty;
        private string _Protocol = string.Empty;
        private string _Status = string.Empty;
        private string _RouteType = string.Empty;
        private string _ClassName = "IPRoutes";

        public string NodeName
        {
            get
            {
                return _Name;
            }
        }

		public int ScanID
		{
			get 
			{ 
				return _ScanID; 
			}
			set 
			{ 
				_ScanID = value; 
			}
		}


		public int Age
		{
			get 
			{ 
				return _Age; 
			}
			set 
			{ 
				_Age = value; 
			}
		}


		public string Name
		{
			get 
			{ 
				return _Name; 
			}
			set 
			{ 
				_Name = value; 
			}
		}


		public string Description
		{
			get 
			{ 
				return _Description; 
			}
			set 
			{ 
				_Description = value; 
			}
		}


		public string Destination
		{
			get 
			{ 
				return _Destination; 
			}
			set 
			{ 
				_Destination = value; 
			}
		}


		public string Mask
		{
			get 
			{ 
				return _Mask; 
			}
			set 
			{ 
				_Mask = value; 
			}
		}


		public int Metric1
		{
			get 
			{ 
				return _Metric1; 
			}
			set 
			{ 
				_Metric1 = value; 
			}
		}


		public int Metric2
		{
			get 
			{ 
				return _Metric2; 
			}
			set 
			{ 
				_Metric2 = value; 
			}
		}


		public int Metric3
		{
			get 
			{ 
				return _Metric3; 
			}
			set 
			{ 
				_Metric3 = value; 
			}
		}


		public int Metric4
		{
			get 
			{ 
				return _Metric4; 
			}
			set 
			{ 
				_Metric4 = value; 
			}
		}


		public int Metric5
		{
			get 
			{ 
				return _Metric5; 
			}
			set 
			{ 
				_Metric5 = value; 
			}
		}


		public string NextHop
		{
			get 
			{ 
				return _NextHop; 
			}
			set 
			{ 
				_NextHop = value; 
			}
		}


		public string Protocol
		{
			get 
			{ 
				return _Protocol; 
			}
			set 
			{ 
				_Protocol = value; 
			}
		}


		public string Status
		{
			get 
			{ 
				return _Status; 
			}
			set 
			{ 
				_Status = value; 
			}
		}


		public string RouteType
		{
			get 
			{ 
				return _RouteType; 
			}
			set 
			{ 
				_RouteType = value; 
			}
		}
        public string ClassName
        {
            get
            {
                return _ClassName;
            }
        }

        public ImageCode Icon
        {
            get
            {
                return ImageCode.IPRoutes;
            }
        }
    }
}
