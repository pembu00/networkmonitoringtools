﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetworkAssetManager.General;

namespace NetworkAssetManager.Entity
{
    public class EntComputer : IEntity
    {
        private int _ScanID = 0;
        private string _BootupState = string.Empty;
        private string _Caption = string.Empty;
        private string _CurrentTimeZone = string.Empty;
        private bool _DaylightInEffect = false;
        private string _Description = string.Empty;
        private string _DNSHostName = string.Empty;
        private string _Domain = string.Empty;
        private string _DomainRole = string.Empty;
        private bool _EnableDaylightSavingsTime;
        private string _Manufacturer = string.Empty;
        private string _Model = string.Empty;
        private string _Name = string.Empty;
        private bool _NetworkServerModeEnabled = false;
        private bool _PartOfDomain = false;
        private string _PCSystemType = string.Empty;
        private string _PowerState = string.Empty;
        private string _PowerSupplyState = string.Empty;
        private string _PrimaryOwnerContact = string.Empty;
        private string _PrimaryOwnerName = string.Empty;
        private string _Roles = string.Empty;
        private string _Status = string.Empty;
        private string _SupportContactDescription = string.Empty;
        private int _SystemStartupDelay = 0;
        private string _SystemStartupOptions = string.Empty;
        private string _SystemType = string.Empty;
        private string _ThermalState = string.Empty;
        private string _UserName = string.Empty;
        private string _WakeUpType = string.Empty;
        private string _Workgroup = string.Empty;
        private string _ClassName = "Computer";

        public string NodeName
        {
            get
            {
                return _Name;
            }
        }

        public int ScanID
        {
            get
            {
                return _ScanID;
            }
            set
            {
                _ScanID = value;
            }
        }

        public string BootupState
        {
            get
            {
                return _BootupState;
            }
            set
            {
                _BootupState = value;
            }
        }


        public string Caption
        {
            get
            {
                return _Caption;
            }
            set
            {
                _Caption = value;
            }
        }


        public string CurrentTimeZone
        {
            get
            {
                return _CurrentTimeZone;
            }
            set
            {
                _CurrentTimeZone = value;
            }
        }


        public bool DaylightInEffect
        {
            get
            {
                return _DaylightInEffect;
            }
            set
            {
                _DaylightInEffect = value;
            }
        }


        public string Description
        {
            get
            {
                return _Description;
            }
            set
            {
                _Description = value;
            }
        }


        public string DNSHostName
        {
            get
            {
                return _DNSHostName;
            }
            set
            {
                _DNSHostName = value;
            }
        }


        public string Domain
        {
            get
            {
                return _Domain;
            }
            set
            {
                _Domain = value;
            }
        }


        public string DomainRole
        {
            get
            {
                return _DomainRole;
            }
            set
            {
                _DomainRole = value;
            }
        }


        public bool EnableDaylightSavingsTime
        {
            get
            {
                return _EnableDaylightSavingsTime;
            }
            set
            {
                _EnableDaylightSavingsTime = value;
            }
        }


        public string Manufacturer
        {
            get
            {
                return _Manufacturer;
            }
            set
            {
                _Manufacturer = value;
            }
        }


        public string Model
        {
            get
            {
                return _Model;
            }
            set
            {
                _Model = value;
            }
        }


        public string Name
        {
            get
            {
                return _Name;
            }
            set
            {
                _Name = value;
            }
        }


        public bool NetworkServerModeEnabled
        {
            get
            {
                return _NetworkServerModeEnabled;
            }
            set
            {
                _NetworkServerModeEnabled = value;
            }
        }


        public bool PartOfDomain
        {
            get
            {
                return _PartOfDomain;
            }
            set
            {
                _PartOfDomain = value;
            }
        }


        public string PCSystemType
        {
            get
            {
                return _PCSystemType;
            }
            set
            {
                _PCSystemType = value;
            }
        }


        public string PowerState
        {
            get
            {
                return _PowerState;
            }
            set
            {
                _PowerState = value;
            }
        }


        public string PowerSupplyState
        {
            get
            {
                return _PowerSupplyState;
            }
            set
            {
                _PowerSupplyState = value;
            }
        }


        public string PrimaryOwnerContact
        {
            get
            {
                return _PrimaryOwnerContact;
            }
            set
            {
                _PrimaryOwnerContact = value;
            }
        }


        public string PrimaryOwnerName
        {
            get
            {
                return _PrimaryOwnerName;
            }
            set
            {
                _PrimaryOwnerName = value;
            }
        }


        public string Roles
        {
            get
            {
                return _Roles;
            }
            set
            {
                _Roles = value;
            }
        }


        public string Status
        {
            get
            {
                return _Status;
            }
            set
            {
                _Status = value;
            }
        }


        public string SupportContactDescription
        {
            get
            {
                return _SupportContactDescription;
            }
            set
            {
                _SupportContactDescription = value;
            }
        }


        public int SystemStartupDelay
        {
            get
            {
                return _SystemStartupDelay;
            }
            set
            {
                _SystemStartupDelay = value;
            }
        }


        public string SystemStartupOptions
        {
            get
            {
                return _SystemStartupOptions;
            }
            set
            {
                _SystemStartupOptions = value;
            }
        }


        public string SystemType
        {
            get
            {
                return _SystemType;
            }
            set
            {
                _SystemType = value;
            }
        }


        public string ThermalState
        {
            get
            {
                return _ThermalState;
            }
            set
            {
                _ThermalState = value;
            }
        }


        public string UserName
        {
            get
            {
                return _UserName;
            }
            set
            {
                _UserName = value;
            }
        }


        public string WakeUpType
        {
            get
            {
                return _WakeUpType;
            }
            set
            {
                _WakeUpType = value;
            }
        }


        public string Workgroup
        {
            get
            {
                return _Workgroup;
            }
            set
            {
                _Workgroup = value;
            }
        }

        public string ClassName
        {
            get
            {
                return _ClassName;
            }
        }

        public ImageCode Icon
        {
            get
            {
                return ImageCode.Bios;
            }
        }


    }
}