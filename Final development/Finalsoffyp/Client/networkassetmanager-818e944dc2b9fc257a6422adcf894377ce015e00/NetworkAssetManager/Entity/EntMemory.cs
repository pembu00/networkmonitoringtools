﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetworkAssetManager.General;

namespace NetworkAssetManager.Entity
{
    public class EntMemory : IEntity
    {
        private string _Capacity = string.Empty;
        private string _DeviceLocator = string.Empty;
        private string _BankLabel = string.Empty;
        private string _FormFactor = string.Empty;
        private string _MemoryType = string.Empty;
        private string _Manufacturer = string.Empty;
        private string _Speed = string.Empty;
        private int _ScanID = 0;
        private string _ClassName = "Memory";

        public string NodeName
        {
            get
            {
                return _BankLabel+ " " + _Capacity;
            }
        }

        public string Capacity
        {
            get
            {
                return _Capacity;
            }

            set
            {
                _Capacity = value;
            }
        }
        public string DeviceLocator
        {
            get
            {
                return _DeviceLocator;
            }

            set
            {
                _DeviceLocator = value;
            }
        }
        public string BankLabel
        {
            get
            {
                return _BankLabel;
            }

            set
            {
                _BankLabel = value;
            }
        }
        public string FormFactor
        {
            get
            {
                return _FormFactor;
            }

            set
            {
                _FormFactor = value;
            }
        }
        public string MemoryType
        {
            get
            {
                return _MemoryType;
            }

            set
            {
                _MemoryType = value;
            }
        }
        public string Manufacturer
        {
            get
            {
                return _Manufacturer;
            }

            set
            {
                _Manufacturer = value;
            }
        }
        public string Speed
        {
            get
            {
                return _Speed;
            }

            set
            {
                _Speed = value;
            }
        }
        public int ScanID
        {
            get
            {
                return _ScanID;
            }

            set
            {
                _ScanID = value;
            }
        }

        public string ClassName
        {
            get
            {
                return _ClassName;
            }
        }

        public ImageCode Icon
        {
            get
            {
                return ImageCode.Memory;
            }
        }
    }
}

