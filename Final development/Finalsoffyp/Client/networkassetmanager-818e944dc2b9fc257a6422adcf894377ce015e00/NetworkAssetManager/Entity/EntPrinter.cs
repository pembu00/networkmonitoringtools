﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetworkAssetManager.General;

namespace NetworkAssetManager.Entity
{

    public class EntPrinter : IEntity
    {
        private string _Attributes = string.Empty;
        private string _Availability = string.Empty;
        private string _AveragePagesPerMinute = string.Empty;
        private string _Caption = string.Empty;
        private int _HorizontalResolution = 0;
        private int _VerticalResolution = 0;
        private bool _Local = false;
        private bool _Network = false;
        private bool _Shared = false;
        private string _Location = string.Empty;
        private string _PortName = string.Empty;
        private string _PrinterState = string.Empty;
        private string _PrinterStatus = string.Empty;
        private string _ServerName = string.Empty;
        private string _ShareName = string.Empty;
        private int _ScanID;
        private string _ClassName = "Printer";


        public string NodeName
        {
            get
            {
                return _Caption;
            }
        }

        public string Attributes
        {
            get
            {
                return _Attributes;
            }
            set
            {
                _Attributes = value;
            }
        }


        public string Availability
        {
            get
            {
                return _Availability;
            }
            set
            {
                _Availability = value;
            }
        }


        public string AveragePagesPerMinute
        {
            get
            {
                return _AveragePagesPerMinute;
            }
            set
            {
                _AveragePagesPerMinute = value;
            }
        }

        public string Caption
        {
            get
            {
                return _Caption;
            }
            set
            {
                _Caption = value;
            }
        }

        public int HorizontalResolution
        {
            get
            {
                return _HorizontalResolution;
            }
            set
            {
                _HorizontalResolution = value;
            }
        }


        public int VerticalResolution
        {
            get
            {
                return _VerticalResolution;
            }
            set
            {
                _VerticalResolution = value;
            }
        }


        public bool Local
        {
            get
            {
                return _Local;
            }
            set
            {
                _Local = value;
            }
        }


        public bool Network
        {
            get
            {
                return _Network;
            }
            set
            {
                _Network = value;
            }
        }


        public bool Shared
        {
            get
            {
                return _Shared;
            }
            set
            {
                _Shared = value;
            }
        }


        public string Location
        {
            get
            {
                return _Location;
            }
            set
            {
                _Location = value;
            }
        }

        public string PortName
        {
            get
            {
                return _PortName;
            }
            set
            {
                _PortName = value;
            }
        }


        public string PrinterState
        {
            get
            {
                return _PrinterState;
            }
            set
            {
                _PrinterState = value;
            }
        }


        public string PrinterStatus
        {
            get
            {
                return _PrinterStatus;
            }
            set
            {
                _PrinterStatus = value;
            }
        }


        public string ServerName
        {
            get
            {
                return _ServerName;
            }
            set
            {
                _ServerName = value;
            }
        }


        public string ShareName
        {
            get
            {
                return _ShareName;
            }
            set
            {
                _ShareName = value;
            }
        }


        public int ScanID
        {
            get
            {
                return _ScanID;
            }
            set
            {
                _ScanID = value;
            }
        }

        public string ClassName
        {
            get
            {
                return _ClassName;
            }
        }

        public ImageCode Icon
        {
            get
            {
                return ImageCode.CDRom;
            }
        }

    }
}