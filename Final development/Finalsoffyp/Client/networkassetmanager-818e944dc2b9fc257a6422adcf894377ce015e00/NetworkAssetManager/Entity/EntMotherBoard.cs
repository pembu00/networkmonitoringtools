﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetworkAssetManager.General;

namespace NetworkAssetManager.Entity
{
    public class EntMotherBoard : IEntity
    {
        private int _ScanID = 0; 
        private string _Manufacturer = string.Empty;
        private string _Version = string.Empty;
        private string _Product = string.Empty;
        private string _SerialNo = string.Empty;
        private string _Model = string.Empty;
        private string _ClassName = "MotherBoard";

        public string NodeName
        {
            get
            {
                return _Manufacturer+ " " + _Model;
            }
        }

        public int ScanID
        {
            set
            {
                _ScanID = value;
            }
            get
            {
                return _ScanID;
            }
        }

        public string Manufacturer
        {
            set
            {
                _Manufacturer = value;
            }
            get
            {
                return _Manufacturer;
            }
        }

        public string Version
        {
            set
            {
                _Version = value;
            }
            get
            {
                return _Version;
            }
        }

        public string Product
        {
            set
            {
                _Product = value;
            }
            get
            {
                return _Product;
            }
        }

        public string SerialNo
        {
            set
            {
                _SerialNo= value;
            }
            get
            {
                return _SerialNo;
            }
        }
        
        public string Model
        {
            set
            {
                _Model = value;
            }
            get
            {
                return _Model ;
            }
        }
        public string ClassName
        {
            get
            {
                return _ClassName;
            }
        }

        public ImageCode Icon
        {
            get
            {
                return ImageCode.Motherboard;
            }
        }



    }
}
