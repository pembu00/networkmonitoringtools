﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetworkAssetManager.General;

namespace NetworkAssetManager.Entity
{
    public class EntMultimedia : IEntity
    {
        private int _ScanID = 0;
        private string _Name = string.Empty;
        private string _Manufacturer = string.Empty;
        private string _ClassName = "Multimedia";

        public string NodeName
        {
            get
            {
                return _Name + " " + _Manufacturer;
            }
        }

        public int ScanID
        {
            get
            {
                return _ScanID;
            }

            set
            {
                _ScanID = value;
            }
        }
        public string Name
        {
            get
            {
                return _Name;
            }

            set
            {
                _Name = value;
            }
        }
        public string Manufacturer
        {
            get
            {
                return _Manufacturer;
            }

            set
            {
                _Manufacturer = value;
            }
        }

        public string ClassName
        {
            get
            {
                return _ClassName;
            }
        }

        public ImageCode Icon
        {
            get
            {
                return ImageCode.Multimedia;
            }
        }
    }
}