﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetworkAssetManager.General;

namespace NetworkAssetManager.Entity
{
    public class EntServices : IEntity
    {
        private int _ScanID = 0;
        private string _DisplayName = string.Empty;
        private string _Description = string.Empty;
        private string _Path = string.Empty;
        private string _Account = string.Empty;
        private string _State = string.Empty;
        private string _StartMode = string.Empty;
        private string _ClassName = "Services";


        public string NodeName
        {
            get
            {
                return _DisplayName;
            }
        }

        public int ScanID
        {
            get
            {
                return _ScanID;
            }
            set
            {
                _ScanID = value;
            }
        }

        public string DisplayName
        {
            get
            {
                return _DisplayName;
            }
            set
            {
                _DisplayName = value;
            }
        }


        public string Description
        {
            get
            {
                return _Description;
            }
            set
            {
                _Description = value;
            }
        }

        public string State
        {
            get
            {
                return _State;
            }
            set
            {
                _State = value;
            }
        }

        public string StartMode
        {
            get
            {
                return _StartMode;
            }
            set
            {
                _StartMode = value;
            }
        }

        public string Account
        {
            get
            {
                return _Account;
            }
            set
            {
                _Account = value;
            }
        }

        public string Path
        {
            get
            {
                return _Path;
            }
            set
            {
                _Path = value;
            }
        }

        public string ClassName
        {
            get
            {
                return _ClassName;
            }
        }

        public ImageCode Icon
        {
            get
            {
                return ImageCode.CDRom;
            }
        }


    }
}