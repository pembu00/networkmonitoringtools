﻿



using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using WifiDistributor.Properties;

namespace WifiDistributor
{
    public partial class Hotspot : Form
    {
        WlanManager wlanManager = new WlanManager();
        IcsManager icsManager = new IcsManager();

        bool isStarted;

        public Hotspot()
        {
            InitializeComponent();
            

        }

        private void MainForm_Load(object sender, EventArgs e)
        {
             
            wlanManager.HostedNetworkAvailable += wlanManager_HostedNetworkAvailable;
            wlanManager.StationJoin += wlanManager_StationJoin;

            RefreshConnection();

            ssidTextBox.Text = Settings.Default.SSID;
            passwordTextBox.Text = Settings.Default.Password;
            foreach (IcsConnection connection in connectionComboBox.Items)
            {
                if (connection.Name == Settings.Default.Connection)
                {
                    connectionComboBox.SelectedItem = connection;
                    break;
                }
            }
        }

        void wlanManager_StationJoin(object sender, EventArgs e)
        {
            
        }

        void wlanManager_HostedNetworkAvailable(object sender, EventArgs e)
        {
            
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Settings.Default.SSID = ssidTextBox.Text;
            Settings.Default.Password = passwordTextBox.Text;
            Settings.Default.Connection = connectionComboBox.Text;
            Settings.Default.Save();
        }

        private void startButton_Click(object sender, EventArgs e)
        {
            startButton.Enabled = false;
            ssidTextBox.Enabled = false;
            passwordTextBox.Enabled = false;
            connectionComboBox.Enabled = false;
            refreshConnectionButton.Enabled = false;
            startButton.Text = "Working...";

            if (isStarted)
            {
                if (Stop())
                {
                    isStarted = false;
                    notifyIcon.ShowBalloonTip(5000, "Success", "Wi-Fi Distributor Stopped", ToolTipIcon.Info);
                }
                else
                {
                    MessageBox.Show("Wi-Fi Distributor could not be stopped.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                if (!ValidateFields())
                    return;

                if (Start(ssidTextBox.Text, passwordTextBox.Text, (IcsConnection)connectionComboBox.SelectedItem, 16))
                {
                    isStarted = true;
                    WindowState = FormWindowState.Minimized;
                    notifyIcon.ShowBalloonTip(5000, "Success", "Wi-Fi Distributor Started", ToolTipIcon.Info);
                }
                else
                {
                    MessageBox.Show("Wi-Fi Distributor could not be started. Supported hardware may not have been found.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            notifyIcon.Icon = new Icon(isStarted ? "WiFiDistributorStarted.ico" : "WiFiDistributorStopped.ico");
            startButton.Text = isStarted ? "Stop Wifi Distributor" : "Start Wi-Fi Distributor";
            
            ssidTextBox.Enabled = !isStarted;
            passwordTextBox.Enabled = !isStarted;
            connectionComboBox.Enabled = !isStarted;
            refreshConnectionButton.Enabled = !isStarted;
            startButton.Enabled = true;
        }

        private bool Start(string ssid, string password, IcsConnection connection, int maxClients)
        {
            try
            {
                Stop();

                wlanManager.SetConnectionSettings(ssid, 32);
                wlanManager.SetSecondaryKey(password);

                wlanManager.StartHostedNetwork();

                var privateConnectionGuid = wlanManager.HostedNetworkInterfaceGuid;

                icsManager.EnableIcs(connection.Guid, privateConnectionGuid);
                
                return true;
            }
            catch
            {
                return false;
            }
        }

        private bool Stop()
        {
            try
            {
                if (this.icsManager.SharingInstalled)
                {
                    this.icsManager.DisableIcsOnAll();
                }

                this.wlanManager.StopHostedNetwork();

                return true;
            }
            catch
            {
                return false;
            }
        }

        private bool ValidateFields()
        {
            if (ssidTextBox.Text.Length <= 0)
            {
                errorProvider.SetError(ssidTextBox, "Network Name (SSID) is required");
                return false;
            }

            if (ssidTextBox.Text.Length > 32)
            {
                errorProvider.SetError(ssidTextBox, "Network Name (SSID) can not be longer than 32 characters");
                return false;
            }

            //if (cboPasswordEncryptionMode.SelectedIndex == 0)
            //{
            //    errorProvider.SetError(lblErrorPasswordMessage, "Please input password length up to 8 character");
            //}
            //if (cboPasswordEncryptionMode.SelectedIndex == 1)
            //{
            //    errorProvider.SetError(lblErrorPasswordMessage, "Please input 0-9 or A-F characters only");

            //}
            //if (cboPasswordEncryptionMode.SelectedIndex == 2)
            //{
            //    passwordTextBox.Visible = false;
            //    passwordLabel.Visible = false;
            //}

            if (passwordTextBox.Text.Length < 8)
            {
                errorProvider.SetError(ssidTextBox, "Password must be at least 8 characters");
                return false;
            }

            if (passwordTextBox.Text.Length > 64)
            {
                errorProvider.SetError(ssidTextBox, "Password can not be longer than 64 characters");
                return false;
            }

            return true;
        }

        private void refreshConnectionButton_Click(object sender, EventArgs e)
        {
            RefreshConnection();
        }

        private void RefreshConnection()
        {
            connectionComboBox.Items.Clear();

            foreach (var connection in icsManager.Connections)
            {
                if (connection.IsConnected && connection.IsSupported)
                {
                    connectionComboBox.Items.Add(connection);
                }
            }

            if (connectionComboBox.Items.Count > 0)
                connectionComboBox.SelectedIndex = 0;
        }

        private void MainForm_SizeChanged(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Minimized)
            {
                Hide();
            }
        }

        private void notifyIcon_MouseClick(object sender, MouseEventArgs e)
        {
            Show();
            WindowState = FormWindowState.Normal;
        }
    }
}
