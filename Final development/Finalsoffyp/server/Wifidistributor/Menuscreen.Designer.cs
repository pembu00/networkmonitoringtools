﻿namespace WifiDistributor
{
    partial class Menuscreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.hotspotToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.networkMonitoringModeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fileSharingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.hotspotToolStripMenuItem,
            this.networkMonitoringModeToolStripMenuItem,
            this.fileSharingToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(479, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // hotspotToolStripMenuItem
            // 
            this.hotspotToolStripMenuItem.Name = "hotspotToolStripMenuItem";
            this.hotspotToolStripMenuItem.Size = new System.Drawing.Size(62, 20);
            this.hotspotToolStripMenuItem.Text = "Hotspot";
            this.hotspotToolStripMenuItem.Click += new System.EventHandler(this.hotspotToolStripMenuItem_Click);
            // 
            // networkMonitoringModeToolStripMenuItem
            // 
            this.networkMonitoringModeToolStripMenuItem.Name = "networkMonitoringModeToolStripMenuItem";
            this.networkMonitoringModeToolStripMenuItem.Size = new System.Drawing.Size(161, 20);
            this.networkMonitoringModeToolStripMenuItem.Text = "Network Monitoring Mode";
            this.networkMonitoringModeToolStripMenuItem.Click += new System.EventHandler(this.networkMonitoringModeToolStripMenuItem_Click);
            // 
            // fileSharingToolStripMenuItem
            // 
            this.fileSharingToolStripMenuItem.Name = "fileSharingToolStripMenuItem";
            this.fileSharingToolStripMenuItem.Size = new System.Drawing.Size(79, 20);
            this.fileSharingToolStripMenuItem.Text = "File sharing";
            this.fileSharingToolStripMenuItem.Click += new System.EventHandler(this.fileSharingToolStripMenuItem_Click);
            // 
            // Menuscreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(479, 344);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Menuscreen";
            this.Text = "Menuscreen";
            this.Load += new System.EventHandler(this.Menuscreen_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem hotspotToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem networkMonitoringModeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fileSharingToolStripMenuItem;
    }
}