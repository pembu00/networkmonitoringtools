﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WifiDistributor
{
    public partial class Menuscreen : Form
    {
        public Menuscreen()
        {
            InitializeComponent();
        }

        private void hotspotToolStripMenuItem_Click(object sender, EventArgs e)
        {

            var myForm = new Hotspot();
            myForm.Show();

        }

        private void Menuscreen_Load(object sender, EventArgs e)
        {

        }

        private void networkMonitoringModeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var myForm = new NetworkAssetManager.Forms.MainUI();
            myForm.Show();
        }

        private void fileSharingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var myForm = new Filesharing();
            myForm.Show();
        }

   
    }
}
