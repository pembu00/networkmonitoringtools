﻿namespace WifiDistributor
{
    partial class Hotspot
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Hotspot));
            this.ssidTextBox = new System.Windows.Forms.TextBox();
            this.passwordTextBox = new System.Windows.Forms.TextBox();
            this.ssidLabel = new System.Windows.Forms.Label();
            this.passwordLabel = new System.Windows.Forms.Label();
            this.connectionLabel = new System.Windows.Forms.Label();
            this.startButton = new System.Windows.Forms.Button();
            this.connectionComboBox = new System.Windows.Forms.ComboBox();
            this.refreshConnectionButton = new System.Windows.Forms.Button();
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.cboPasswordEncryptionMode = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.lblErrorPasswordMessage = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // ssidTextBox
            // 
            this.ssidTextBox.Location = new System.Drawing.Point(201, 38);
            this.ssidTextBox.Name = "ssidTextBox";
            this.ssidTextBox.Size = new System.Drawing.Size(197, 20);
            this.ssidTextBox.TabIndex = 0;
            // 
            // passwordTextBox
            // 
            this.passwordTextBox.Location = new System.Drawing.Point(201, 115);
            this.passwordTextBox.Name = "passwordTextBox";
            this.passwordTextBox.Size = new System.Drawing.Size(197, 20);
            this.passwordTextBox.TabIndex = 1;
            // 
            // ssidLabel
            // 
            this.ssidLabel.AutoSize = true;
            this.ssidLabel.Location = new System.Drawing.Point(57, 41);
            this.ssidLabel.Name = "ssidLabel";
            this.ssidLabel.Size = new System.Drawing.Size(118, 13);
            this.ssidLabel.TabIndex = 2;
            this.ssidLabel.Text = "Network Name (SSID): ";
            // 
            // passwordLabel
            // 
            this.passwordLabel.AutoSize = true;
            this.passwordLabel.Location = new System.Drawing.Point(116, 122);
            this.passwordLabel.Name = "passwordLabel";
            this.passwordLabel.Size = new System.Drawing.Size(59, 13);
            this.passwordLabel.TabIndex = 3;
            this.passwordLabel.Text = "Password: ";
            // 
            // connectionLabel
            // 
            this.connectionLabel.AutoSize = true;
            this.connectionLabel.Location = new System.Drawing.Point(77, 158);
            this.connectionLabel.Name = "connectionLabel";
            this.connectionLabel.Size = new System.Drawing.Size(104, 13);
            this.connectionLabel.TabIndex = 4;
            this.connectionLabel.Text = "Shared Connection: ";
            // 
            // startButton
            // 
            this.startButton.Location = new System.Drawing.Point(201, 203);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(56, 23);
            this.startButton.TabIndex = 5;
            this.startButton.Text = "Start";
            this.startButton.UseVisualStyleBackColor = true;
            this.startButton.Click += new System.EventHandler(this.startButton_Click);
            // 
            // connectionComboBox
            // 
            this.connectionComboBox.FormattingEnabled = true;
            this.connectionComboBox.Location = new System.Drawing.Point(201, 155);
            this.connectionComboBox.Name = "connectionComboBox";
            this.connectionComboBox.Size = new System.Drawing.Size(133, 21);
            this.connectionComboBox.TabIndex = 6;
            // 
            // refreshConnectionButton
            // 
            this.refreshConnectionButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("refreshConnectionButton.BackgroundImage")));
            this.refreshConnectionButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.refreshConnectionButton.Location = new System.Drawing.Point(366, 148);
            this.refreshConnectionButton.Margin = new System.Windows.Forms.Padding(0);
            this.refreshConnectionButton.Name = "refreshConnectionButton";
            this.refreshConnectionButton.Size = new System.Drawing.Size(32, 32);
            this.refreshConnectionButton.TabIndex = 7;
            this.refreshConnectionButton.UseVisualStyleBackColor = true;
            this.refreshConnectionButton.Click += new System.EventHandler(this.refreshConnectionButton_Click);
            // 
            // notifyIcon
            // 
            this.notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon.Icon")));
            this.notifyIcon.Text = "WiFi Distributor";
            this.notifyIcon.Visible = true;
            this.notifyIcon.MouseClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon_MouseClick);
            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(40, 82);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(135, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Password Encryption mode";
            // 
            // cboPasswordEncryptionMode
            // 
            this.cboPasswordEncryptionMode.FormattingEnabled = true;
            this.cboPasswordEncryptionMode.Items.AddRange(new object[] {
            "Wifi Adhoc Encrypted (WEP)",
            "Wifi Acess Encrypted(WPA)",
            "Wifi Adhoc Open"});
            this.cboPasswordEncryptionMode.Location = new System.Drawing.Point(201, 74);
            this.cboPasswordEncryptionMode.Name = "cboPasswordEncryptionMode";
            this.cboPasswordEncryptionMode.Size = new System.Drawing.Size(133, 21);
            this.cboPasswordEncryptionMode.TabIndex = 6;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(275, 203);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(59, 23);
            this.button1.TabIndex = 5;
            this.button1.Text = "Exit";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.startButton_Click);
            // 
            // lblErrorPasswordMessage
            // 
            this.lblErrorPasswordMessage.AutoSize = true;
            this.lblErrorPasswordMessage.ForeColor = System.Drawing.Color.Firebrick;
            this.lblErrorPasswordMessage.Location = new System.Drawing.Point(201, 98);
            this.lblErrorPasswordMessage.Name = "lblErrorPasswordMessage";
            this.lblErrorPasswordMessage.Size = new System.Drawing.Size(0, 13);
            this.lblErrorPasswordMessage.TabIndex = 8;
            // 
            // Hotspot
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(471, 259);
            this.Controls.Add(this.lblErrorPasswordMessage);
            this.Controls.Add(this.refreshConnectionButton);
            this.Controls.Add(this.cboPasswordEncryptionMode);
            this.Controls.Add(this.connectionComboBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.startButton);
            this.Controls.Add(this.connectionLabel);
            this.Controls.Add(this.passwordLabel);
            this.Controls.Add(this.ssidLabel);
            this.Controls.Add(this.passwordTextBox);
            this.Controls.Add(this.ssidTextBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Hotspot";
            this.Text = "Wi-Fi Distributor";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.SizeChanged += new System.EventHandler(this.MainForm_SizeChanged);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox ssidTextBox;
        private System.Windows.Forms.TextBox passwordTextBox;
        private System.Windows.Forms.Label ssidLabel;
        private System.Windows.Forms.Label passwordLabel;
        private System.Windows.Forms.Label connectionLabel;
        private System.Windows.Forms.Button startButton;
        private System.Windows.Forms.ComboBox connectionComboBox;
        private System.Windows.Forms.Button refreshConnectionButton;
        private System.Windows.Forms.NotifyIcon notifyIcon;
        private System.Windows.Forms.ErrorProvider errorProvider;
        private System.Windows.Forms.ComboBox cboPasswordEncryptionMode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label lblErrorPasswordMessage;
    }
}

