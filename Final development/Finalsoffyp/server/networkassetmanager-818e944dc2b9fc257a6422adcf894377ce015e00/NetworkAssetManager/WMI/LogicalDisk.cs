﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetworkAssetManager.Entity;
using System.Management;
using log4net; 
namespace NetworkAssetManager.WMI
{
    class LogicalDisk
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(LogicalDisk));

        private static EntLogicalDrive FillDetails(ManagementBaseObject obj)
        {
            EntLogicalDrive objEntLogicalDrive = new EntLogicalDrive();
            try
            {
                objEntLogicalDrive.Description = WMIUtil.ToString(obj["Description"]);
            }
            catch (Exception)
            {
                objEntLogicalDrive.Description = ""; 
            }

            try
            {
                objEntLogicalDrive.FileSystem = WMIUtil.ToString(obj["FileSystem"]);
            }
            catch (Exception)
            {
                objEntLogicalDrive.FileSystem = ""; 
            }

            try
            {
                objEntLogicalDrive.FreeSpace = WMIUtil.ConvertSizetoString((UInt64)obj["FreeSpace"], true, NetworkAssetManager.General.Units.Auto);
            }
            catch (Exception)
            {
                objEntLogicalDrive.FreeSpace = "0 bytes"; 
            }

            try
            {
                objEntLogicalDrive.Name = WMIUtil.ToString(obj["Name"]);
            }
            catch (Exception)
            {
                objEntLogicalDrive.Name = ""; 
            }

            try
            {
                objEntLogicalDrive.SerialNumber = WMIUtil.ToString(obj["VolumeSerialNumber"]);
            }
            catch (Exception )
            {
                objEntLogicalDrive.SerialNumber = ""; 
            }

            try
            {
                objEntLogicalDrive.Size = WMIUtil.ConvertSizetoString((UInt64)obj["Size"], true, NetworkAssetManager.General.Units.Auto);
            }
            catch (Exception )
            {
                objEntLogicalDrive.Size = "0 bytes"; 
            }

            try
            {
                objEntLogicalDrive.VolumeLabel = WMIUtil.ToString(obj["VolumeName"]);
            }
            catch (Exception)
            {
                objEntLogicalDrive.VolumeLabel = ""; 
            }

            return objEntLogicalDrive;
        }
        public static List<EntLogicalDrive>GetLogicalDriveDetails(ManagementScope scope)
        {
            _logger.Info("Collecting logical drive details for machine " + scope.Path.Server);

            ObjectQuery query = null;
            ManagementObjectSearcher searcher = null;
            ManagementObjectCollection objects = null;
            List<EntLogicalDrive> lstLogicalDrive = new List<EntLogicalDrive>();

            try
            {
                query = new ObjectQuery("Select * from Win32_LogicalDisk");
                searcher = new ManagementObjectSearcher(scope, query);
                objects = searcher.Get();
                lstLogicalDrive.Capacity = objects.Count;
                foreach (ManagementBaseObject obj in objects)
                {
                    lstLogicalDrive.Add(FillDetails(obj));
                    obj.Dispose();
                }
            }
            catch (System.Exception e)
            {
                _logger.Error("Exception is logical drive collection " + e.Message);
            }
            finally
            {
                searcher.Dispose();
            }
            return lstLogicalDrive;
        }

    }
}
