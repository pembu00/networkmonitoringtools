﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetworkAssetManager.Entity;
using System.Management;
using log4net; 
namespace NetworkAssetManager.WMI
{
    class Processes
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(Processes));

        private static EntProcesses FillDetails(ManagementBaseObject obj)
        {
            EntProcesses objEntProcesses = new EntProcesses();
            try
            {
                objEntProcesses.Name = WMIUtil.ToString(obj["Name"]);
            }
            catch (Exception)
            {
                objEntProcesses.Name = ""; 
            }
            try
            {
                objEntProcesses.ParentProcessID = WMIUtil.ToInteger(obj["ParentProcessId"]); 
            }
            catch (Exception)
            {
                objEntProcesses.ParentProcessID = 0; 
            }
            try
            {
                objEntProcesses.ProcessID= WMIUtil.ToInteger(obj["ProcessId"]);
            }
            catch (Exception)
            {
                objEntProcesses.ProcessID = 0;
            }
            try
            {
                objEntProcesses.ThreadCount= WMIUtil.ToInteger(obj["ThreadCount"]);
            }
            catch (Exception)
            {
                objEntProcesses.ThreadCount = 0;
            }
            try
            {
                objEntProcesses.Priority= WMIUtil.ToInteger(obj["Priority"]);
            }
            catch (Exception)
            {
                objEntProcesses.Priority = 0;
            }
            try
            {
                objEntProcesses.CommandLine = WMIUtil.ToString(obj["CommandLine"]);
            }
            catch (Exception)
            {
                objEntProcesses.CommandLine = "";
            }
            try
            {
                objEntProcesses.ExecutablePath = WMIUtil.ToString(obj["ExecutablePath"]);
            }
            catch (Exception)
            {
                objEntProcesses.ExecutablePath = "";
            }
            try
            {
                objEntProcesses.VirtualSize = WMIUtil.ConvertSizetoString((UInt64)obj["VirtualSize"], false, NetworkAssetManager.General.Units.KB);
            }
            catch (Exception)
            {
                objEntProcesses.VirtualSize = ""; 
            }
            try
            {
                objEntProcesses.PrivateBytes = WMIUtil.ConvertSizetoString((UInt64)obj["PrivatePageCount"], false, NetworkAssetManager.General.Units.KB);
            }
            catch (Exception)
            {
                objEntProcesses.PrivateBytes = "";
            }
            try
            {
                objEntProcesses.CreationDate = WMIUtil.ToDateTime((string)obj["CreationDate"]);
            }
            catch (Exception)
            {
                objEntProcesses.CreationDate = DateTime.MinValue; 
            }

            return objEntProcesses;
        }
        public static List<EntProcesses>GetProcessesDetails(ManagementScope scope)
        {
            _logger.Info("Collecting processes details for machine " + scope.Path.Server);

            ObjectQuery query = null;
            ManagementObjectSearcher searcher = null;
            ManagementObjectCollection objects = null;
            List<EntProcesses> lstProcesses = new List<EntProcesses>();

            try
            {
                query = new ObjectQuery("Select * from Win32_Process");
                searcher = new ManagementObjectSearcher(scope, query);
                objects = searcher.Get();
                lstProcesses.Capacity = objects.Count;
                foreach (ManagementBaseObject obj in objects)
                {
                    lstProcesses.Add(FillDetails(obj));
                    obj.Dispose();
                }
            }
            catch (System.Exception e)
            {
                _logger.Error("Exception in processes collection " + e.Message);
            }
            finally
            {
                searcher.Dispose();
            }
            return lstProcesses;
        }

    }
}
