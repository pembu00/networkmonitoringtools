﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Management;
using System.Net.NetworkInformation;
using NetworkAssetManager.Entity;

namespace NetworkAssetManager.WMI
{
    class WMIHandler
    {
        private string _Hostname = string.Empty;
        private string _Username = string.Empty;
        private string _Password = string.Empty;

        private bool _bWmiInitialized = false ; 

        private ManagementScope _WmiScope = null;
        private ManagementScope _RegistryScope = null; 
 
        public string Hostname 
        {
            set
            {
                _Hostname = value; 
            }
            get
            {
                return _Hostname; 
            }
        }

        public string Username 
        {
            set
            {
                _Username = value; 
            }

            get
            {
                return _Username; 
            }
        }

        public string Password
        {
            set
            {
                _Password = value; 
            }
            get
            {
                return _Password; 
            }
        }

        public bool IsWmiInitialized
        {
            get
            {
                return _bWmiInitialized; 
            }
        }


        public WMIHandler()
        {

        }
        public WMIHandler(string Hostname, string Username, string Password)
        {
            _Hostname = Hostname;
            _Username = Username;
            _Password = Password;
            InitializeWmiScope(); 
        }
        private bool checkPing()
        {
            bool bStatus = false;
            Ping pingSender = new Ping();
            PingOptions options = new PingOptions();

            // Use the default Ttl value which is 128,
            // but change the fragmentation behavior.
            options.DontFragment = true;

            // Create a buffer of 32 bytes of data to be transmitted.
            string data = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
            byte[] buffer = Encoding.ASCII.GetBytes(data);
            int timeout = 10;
            PingReply reply = null;
            try
            {
                reply = pingSender.Send(_Hostname, timeout, buffer, options);
            }
            catch (System.Exception)
            {
                bStatus = false;
            }

            if (reply != null && reply.Status == IPStatus.Success)
                bStatus = true;

            return bStatus;
        }

        private void InitializeWmiScope ()
        {
            bool retCode = false;
            ConnectionOptions options = new ConnectionOptions();
            options.Username = _Username;
            options.Password = _Password;
            if ( checkPing() ==true )
            {
                retry:
                try
                {
                    _WmiScope =new ManagementScope("\\\\" + _Hostname+ "\\root\\cimv2", options);
                    _WmiScope.Connect();

                    _RegistryScope = new ManagementScope("\\\\" + _Hostname + "\\root\\default", options);
                    _RegistryScope.Connect();

                    retCode = true; 
                }
                catch(ManagementException e)
                {
                    if ( e.ErrorCode == ManagementStatus.LocalCredentials)
                    {
                        // this is a hack
                        // this is local machine so retry by null Username and Password
                        options.Password = null;
                        options.Username = null;
                        goto retry; 
                    }
                    else
                    {
                        retCode = false;
                    }
                }
                catch
                {
                    retCode = false;
                }
            }
            else
            {
                retCode = false; 
            }

            _bWmiInitialized = retCode; 
        }

        public List<EntOS> GetOSDetails()
        {
            return OS.GetOSDetails(_WmiScope); 

        }

        public List<EntBios> GetBiosDetails()
        {
            return Bios.GetBiosDetails(_WmiScope); 
        }

        public List<EntMotherBoard> GetMotherBoardDetails()
        {
            return MotherBoard.GetMotherBoardDetails(_WmiScope);
        }

        public List<EntProcessor> GetProcessorDetails()
        {
            return Processor.GetProcessorDetails(_WmiScope);
        }

        public List<EntDisk> GetDiskDetails()
        {
            return Disk.GetDiskDetails(_WmiScope);
        }

        public List<EntMemory> GetMemoryDetails()
        {
            return Memory.GetMemoryDetails(_WmiScope);
        }

        public List<EntLogicalDrive> GetLogicalDriveDetails()
        {
            return LogicalDisk.GetLogicalDriveDetails(_WmiScope);
        }

        public List<EntCDRom> GetCDRomDetails()
        {
            return CDRom.GetCDRomDetails(_WmiScope);
        }

        public List<EntVideo> GetVideoDetails()
        {
            return Video.GetVideoDetails(_WmiScope);
        }

        public List<EntMultimedia> GetMultimediaDetails()
        {
            return Multimedia.GetMultimediaDetails(_WmiScope);
        }

        public List<EntMonitor> GetMonitorDetails()
        {
            return Monitor.GetMonitorDetails(_WmiScope);
        }

        public List<EntShare> GetShareDetails()
        {
            return Share.GetShareDetails(_WmiScope);
        }

        public List<EntStartUp> GetStartUpDetails()
        {
            return StartUp.GetStartUpDetails(_WmiScope);
        }
        public List<EntHotfixes> GetHotfixDetails()
        {
            return Hotfix.GetHotfixDetails(_WmiScope);
        }

        public List<EntProcesses> GetProcessesDetails()
        {
            return Processes.GetProcessesDetails(_WmiScope);
        }

        public List<EntSoftwares> GetInstalledSoftwareDetails()
        {
            return RegistryHelper.GetInstalledSoftwareDetails(_RegistryScope); 
        }

        public List<EntServices> GetServicesDetails()
        {
            return Services.GetServicesDetails(_WmiScope);
        }

        public List<EntIPRoutes> GetIPRoutes()
        {
            return IPRoutes.GetIPRoutesDetails(_WmiScope);
        }

        public List<EntEnvironmentVars> GetEnvironmentVariables()
        {
            return EnvironmentVar.GetEnvironmentVarDetails(_WmiScope);
        }

        public List<EntComputer> GetComputerDetails()
        {
            return Computer.GetComputerDetails(_WmiScope);
        }

        public List<EntPrinter> GetPrinterDetails()
        {
            return Printer.GetPrinterDetails(_WmiScope);
        }

        public List<EntUserGroups> GetUserGroupDetails()
        {
            return UserGroups.GetUserGroupDetails(_WmiScope);
        }

    }
}
