﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetworkAssetManager.Entity;
using System.Management;
using log4net; 
namespace NetworkAssetManager.WMI
{
    class Monitor
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(Monitor));

        public enum AvailabilityValues
        {
            Other = 1,
            Unknown = 2,
            RunningFullPower = 3,
            Warning = 4,
            InTest = 5,
            NotApplicable = 6,
            PowerOff = 7,
            OffLine = 8,
            OffDuty = 9,
            Degraded = 10,
            NotInstalled = 11,
            InstallError = 12,
            PowerSaveUnknown = 13,
            PowerSaveLowPowerMode = 14,
            PowerSaveStandby = 15,
            PowerCycle = 16,
            PowerSaveWarning = 17,
            Paused = 18,
            NotReady = 19,
            NotConfigured = 20,
            Quiesced = 21,
            NULL = 0
        }
        private static EntMonitor FillDetails(ManagementBaseObject obj)
        {
            EntMonitor objEntMonitor = new EntMonitor();
            try
            {
                objEntMonitor.Name = WMIUtil.ToString(obj["Name"]);
            }
            catch (Exception)
            {
                objEntMonitor.Name = "";
            }
            
            try
            {
                objEntMonitor.ScreenHeight = WMIUtil.ToInteger(obj["ScreenHeight"]);
            }
            catch (Exception)
            {
                objEntMonitor.ScreenHeight = 0;
            }

            try
            {
                objEntMonitor.ScreenWidth = WMIUtil.ToInteger(obj["ScreenWidth"]);
            }
            catch (Exception)
            {
                objEntMonitor.ScreenWidth = 0; 
            }

            try
            {
                AvailabilityValues availability = (AvailabilityValues)WMIUtil.ToInteger(obj["Availability"]);
                objEntMonitor.Availability = WMIUtil.ToStringCamelCase(availability);
            }
            catch (Exception)
            {
                objEntMonitor.Availability = "Unknown"; 
            }

            return objEntMonitor;
        }
        public static List<EntMonitor>GetMonitorDetails(ManagementScope scope)
        {
            _logger.Info("Collecting monitor details for machine " + scope.Path.Server);

            ObjectQuery query = null;
            ManagementObjectSearcher searcher = null;
            ManagementObjectCollection objects = null;
            List<EntMonitor> lstMonitor = new List<EntMonitor>();

            try
            {
                query = new ObjectQuery("Select * from Win32_DesktopMonitor");
                searcher = new ManagementObjectSearcher(scope, query);
                objects = searcher.Get();
                lstMonitor.Capacity = objects.Count;
                foreach (ManagementBaseObject obj in objects)
                {
                    lstMonitor.Add(FillDetails(obj));
                    obj.Dispose();
                }
            }
            catch (System.Exception e)
            {
                _logger.Error("Exception is monitor collection " + e.Message);
            }
            finally
            {
                searcher.Dispose();
            }
            return lstMonitor;
        }
    }
}
