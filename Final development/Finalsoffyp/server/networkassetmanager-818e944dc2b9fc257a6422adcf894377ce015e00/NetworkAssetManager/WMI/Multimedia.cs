﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetworkAssetManager.Entity;
using System.Management;
using log4net; 
namespace NetworkAssetManager.WMI
{
    class Multimedia
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(Multimedia));

        private static EntMultimedia FillDetails(ManagementBaseObject obj)
        {
            EntMultimedia objEntMultimedia = new EntMultimedia();
            try
            {
                objEntMultimedia.Manufacturer = WMIUtil.ToString(obj["Manufacturer"]);
            }
            catch (Exception)
            {
                objEntMultimedia.Manufacturer = "";
            }
            
            try
            {
                objEntMultimedia.Name = WMIUtil.ToString(obj["Name"]); 
            }
            catch (Exception)
            {
                objEntMultimedia.Name = "";            
            }

            return objEntMultimedia;
        }
        public static List<EntMultimedia> GetMultimediaDetails(ManagementScope scope)
        {
            _logger.Info("Collecting monitor details for machine " + scope.Path.Server);

            ObjectQuery query = null;
            ManagementObjectSearcher searcher = null;
            ManagementObjectCollection objects = null;
            List<EntMultimedia> lstMultimedia = new List<EntMultimedia>();

            try
            {
                query = new ObjectQuery("Select * from Win32_SoundDevice");
                searcher = new ManagementObjectSearcher(scope, query);
                objects = searcher.Get();
                lstMultimedia.Capacity = objects.Count;
                foreach (ManagementBaseObject obj in objects)
                {
                    lstMultimedia.Add(FillDetails(obj));
                    obj.Dispose();
                }
            }
            catch (System.Exception e)
            {
                _logger.Error("Exception is multimedia collection " + e.Message);
            }
            finally
            {
                searcher.Dispose();
            }
            return lstMultimedia;
        }

    }
}
