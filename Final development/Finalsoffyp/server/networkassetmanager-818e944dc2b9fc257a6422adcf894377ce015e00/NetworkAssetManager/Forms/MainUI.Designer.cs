﻿namespace NetworkAssetManager.Forms
{
    partial class MainUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainUI));
            ExtendedListView.Forms.ToggleColumnHeader toggleColumnHeader1 = new ExtendedListView.Forms.ToggleColumnHeader();
            ExtendedListView.Forms.ToggleColumnHeader toggleColumnHeader2 = new ExtendedListView.Forms.ToggleColumnHeader();
            ExtendedListView.Forms.ToggleColumnHeader toggleColumnHeader3 = new ExtendedListView.Forms.ToggleColumnHeader();
            ExtendedListView.Forms.ToggleColumnHeader toggleColumnHeader4 = new ExtendedListView.Forms.ToggleColumnHeader();
            ExtendedListView.Forms.ToggleColumnHeader toggleColumnHeader5 = new ExtendedListView.Forms.ToggleColumnHeader();
            ExtendedListView.Forms.ToggleColumnHeader toggleColumnHeader6 = new ExtendedListView.Forms.ToggleColumnHeader();
            ExtendedListView.Forms.ToggleColumnHeader toggleColumnHeader7 = new ExtendedListView.Forms.ToggleColumnHeader();
            ExtendedListView.Forms.ToggleColumnHeader toggleColumnHeader8 = new ExtendedListView.Forms.ToggleColumnHeader();
            ExtendedListView.Forms.ToggleColumnHeader toggleColumnHeader9 = new ExtendedListView.Forms.ToggleColumnHeader();
            ExtendedListView.Forms.ToggleColumnHeader toggleColumnHeader10 = new ExtendedListView.Forms.ToggleColumnHeader();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configurationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addMachinesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.addComputersByHostnameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addComputersByDomainToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addComputersByIpRangeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addComputersFromFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.deleteComputerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.credentialsConfigToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.credentialsMapToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.scanSelectedComputerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.scanAllComputerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.deleteCurScanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.viewSystemInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.dNSLookupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iPScanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.debugViewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.systemReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.contentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.indexToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.tsProgBar = new System.Windows.Forms.ToolStripProgressBar();
            this.tsStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.lstMonitoredMachine = new System.Windows.Forms.ListView();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.scanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteMachineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lstIcons = new System.Windows.Forms.ImageList(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.cbScans = new System.Windows.Forms.ComboBox();
            this.tbCtrl = new System.Windows.Forms.TabControl();
            this.tabSystem = new System.Windows.Forms.TabPage();
            this.cbListExpand = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblCollectionStatus = new System.Windows.Forms.Label();
            this.lstDisplayInfo = new ExtendedListView.Forms.TreeListView();
            this.tabSoftwares = new System.Windows.Forms.TabPage();
            this.lstSoftwares = new System.Windows.Forms.ListView();
            this.tabProcesses = new System.Windows.Forms.TabPage();
            this.lstProcesses = new System.Windows.Forms.ListView();
            this.tabServices = new System.Windows.Forms.TabPage();
            this.lstServices = new System.Windows.Forms.ListView();
            this.tabIPRoutes = new System.Windows.Forms.TabPage();
            this.lstIPRoutes = new System.Windows.Forms.ListView();
            this.tabEnvVars = new System.Windows.Forms.TabPage();
            this.lstEnvVars = new System.Windows.Forms.ListView();
            this.tabUserGroups = new System.Windows.Forms.TabPage();
            this.lstUserGroups = new System.Windows.Forms.ListView();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.treeListView1 = new ExtendedListView.Forms.TreeListView();
            this.label4 = new System.Windows.Forms.Label();
            this.treeListView2 = new ExtendedListView.Forms.TreeListView();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.treeListView3 = new ExtendedListView.Forms.TreeListView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.listView1 = new System.Windows.Forms.ListView();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.listView2 = new System.Windows.Forms.ListView();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.listView3 = new System.Windows.Forms.ListView();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.treeListView4 = new ExtendedListView.Forms.TreeListView();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.listView4 = new System.Windows.Forms.ListView();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.listView5 = new System.Windows.Forms.ListView();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.listView6 = new System.Windows.Forms.ListView();
            this.menuStrip.SuspendLayout();
            this.statusStrip.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.tbCtrl.SuspendLayout();
            this.tabSystem.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tabSoftwares.SuspendLayout();
            this.tabProcesses.SuspendLayout();
            this.tabServices.SuspendLayout();
            this.tabIPRoutes.SuspendLayout();
            this.tabEnvVars.SuspendLayout();
            this.tabUserGroups.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.tabPage7.SuspendLayout();
            this.tabPage8.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.configurationsToolStripMenuItem,
            this.toolStripMenuItem2,
            this.toolsMenu,
            this.reportsToolStripMenuItem,
            this.helpMenu});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(767, 24);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "MenuStrip";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(35, 20);
            this.toolStripMenuItem1.Text = "File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            // 
            // configurationsToolStripMenuItem
            // 
            this.configurationsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addMachinesToolStripMenuItem,
            this.toolStripSeparator1,
            this.addComputersByHostnameToolStripMenuItem,
            this.addComputersByDomainToolStripMenuItem,
            this.addComputersByIpRangeToolStripMenuItem,
            this.addComputersFromFileToolStripMenuItem,
            this.toolStripSeparator2,
            this.deleteComputerToolStripMenuItem,
            this.toolStripSeparator5,
            this.credentialsConfigToolStripMenuItem1,
            this.credentialsMapToolStripMenuItem});
            this.configurationsToolStripMenuItem.Name = "configurationsToolStripMenuItem";
            this.configurationsToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.C)));
            this.configurationsToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.configurationsToolStripMenuItem.Text = "Machine";
            // 
            // addMachinesToolStripMenuItem
            // 
            this.addMachinesToolStripMenuItem.Name = "addMachinesToolStripMenuItem";
            this.addMachinesToolStripMenuItem.Size = new System.Drawing.Size(217, 22);
            this.addMachinesToolStripMenuItem.Text = "Find Machine";
            this.addMachinesToolStripMenuItem.Click += new System.EventHandler(this.addMachinesToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(214, 6);
            // 
            // addComputersByHostnameToolStripMenuItem
            // 
            this.addComputersByHostnameToolStripMenuItem.Name = "addComputersByHostnameToolStripMenuItem";
            this.addComputersByHostnameToolStripMenuItem.Size = new System.Drawing.Size(217, 22);
            this.addComputersByHostnameToolStripMenuItem.Text = "Add computer by hostname";
            this.addComputersByHostnameToolStripMenuItem.Click += new System.EventHandler(this.addComputersByHostnameToolStripMenuItem_Click);
            // 
            // addComputersByDomainToolStripMenuItem
            // 
            this.addComputersByDomainToolStripMenuItem.Name = "addComputersByDomainToolStripMenuItem";
            this.addComputersByDomainToolStripMenuItem.Size = new System.Drawing.Size(217, 22);
            this.addComputersByDomainToolStripMenuItem.Text = "Add computers by domain";
            this.addComputersByDomainToolStripMenuItem.Click += new System.EventHandler(this.addComputersByDomainToolStripMenuItem_Click);
            // 
            // addComputersByIpRangeToolStripMenuItem
            // 
            this.addComputersByIpRangeToolStripMenuItem.Enabled = false;
            this.addComputersByIpRangeToolStripMenuItem.Name = "addComputersByIpRangeToolStripMenuItem";
            this.addComputersByIpRangeToolStripMenuItem.Size = new System.Drawing.Size(217, 22);
            this.addComputersByIpRangeToolStripMenuItem.Text = "Add computers by IP range";
            // 
            // addComputersFromFileToolStripMenuItem
            // 
            this.addComputersFromFileToolStripMenuItem.Enabled = false;
            this.addComputersFromFileToolStripMenuItem.Name = "addComputersFromFileToolStripMenuItem";
            this.addComputersFromFileToolStripMenuItem.Size = new System.Drawing.Size(217, 22);
            this.addComputersFromFileToolStripMenuItem.Text = "Add computers from file";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(214, 6);
            // 
            // deleteComputerToolStripMenuItem
            // 
            this.deleteComputerToolStripMenuItem.Name = "deleteComputerToolStripMenuItem";
            this.deleteComputerToolStripMenuItem.Size = new System.Drawing.Size(217, 22);
            this.deleteComputerToolStripMenuItem.Text = "Delete computer";
            this.deleteComputerToolStripMenuItem.Click += new System.EventHandler(this.deleteMachineToolStripMenuItem_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(214, 6);
            // 
            // credentialsConfigToolStripMenuItem1
            // 
            this.credentialsConfigToolStripMenuItem1.Name = "credentialsConfigToolStripMenuItem1";
            this.credentialsConfigToolStripMenuItem1.Size = new System.Drawing.Size(217, 22);
            this.credentialsConfigToolStripMenuItem1.Text = "Credential configuration";
            this.credentialsConfigToolStripMenuItem1.Click += new System.EventHandler(this.credentialsConfigToolStripMenuItem1_Click);
            // 
            // credentialsMapToolStripMenuItem
            // 
            this.credentialsMapToolStripMenuItem.Name = "credentialsMapToolStripMenuItem";
            this.credentialsMapToolStripMenuItem.Size = new System.Drawing.Size(217, 22);
            this.credentialsMapToolStripMenuItem.Text = "Credential mapping";
            this.credentialsMapToolStripMenuItem.Click += new System.EventHandler(this.credentialsMapToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.scanSelectedComputerToolStripMenuItem,
            this.scanAllComputerToolStripMenuItem,
            this.toolStripSeparator4,
            this.deleteCurScanToolStripMenuItem,
            this.toolStripMenuItem4,
            this.toolStripSeparator3,
            this.viewSystemInformationToolStripMenuItem});
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(42, 20);
            this.toolStripMenuItem2.Text = "Scan";
            // 
            // scanSelectedComputerToolStripMenuItem
            // 
            this.scanSelectedComputerToolStripMenuItem.Name = "scanSelectedComputerToolStripMenuItem";
            this.scanSelectedComputerToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.scanSelectedComputerToolStripMenuItem.Text = "Scan selected computer";
            this.scanSelectedComputerToolStripMenuItem.Click += new System.EventHandler(this.scanToolStripMenuItem_Click);
            // 
            // scanAllComputerToolStripMenuItem
            // 
            this.scanAllComputerToolStripMenuItem.Name = "scanAllComputerToolStripMenuItem";
            this.scanAllComputerToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.scanAllComputerToolStripMenuItem.Text = "Scan all computers";
            this.scanAllComputerToolStripMenuItem.Click += new System.EventHandler(this.scanAllComputerToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(201, 6);
            // 
            // deleteCurScanToolStripMenuItem
            // 
            this.deleteCurScanToolStripMenuItem.Enabled = false;
            this.deleteCurScanToolStripMenuItem.Name = "deleteCurScanToolStripMenuItem";
            this.deleteCurScanToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.deleteCurScanToolStripMenuItem.Text = "Delete current scan";
            this.deleteCurScanToolStripMenuItem.Click += new System.EventHandler(this.deleteCurScanToolStripMenuItem_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Enabled = false;
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(204, 22);
            this.toolStripMenuItem4.Text = "Delete old scans";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(201, 6);
            // 
            // viewSystemInformationToolStripMenuItem
            // 
            this.viewSystemInformationToolStripMenuItem.Enabled = false;
            this.viewSystemInformationToolStripMenuItem.Name = "viewSystemInformationToolStripMenuItem";
            this.viewSystemInformationToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.viewSystemInformationToolStripMenuItem.Text = "View system  information";
            this.viewSystemInformationToolStripMenuItem.Click += new System.EventHandler(this.viewSystemInformationToolStripMenuItem_Click);
            // 
            // toolsMenu
            // 
            this.toolsMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dNSLookupToolStripMenuItem,
            this.iPScanToolStripMenuItem,
            this.optionsToolStripMenuItem,
            this.debugViewToolStripMenuItem});
            this.toolsMenu.Name = "toolsMenu";
            this.toolsMenu.Size = new System.Drawing.Size(44, 20);
            this.toolsMenu.Text = "&Tools";
            // 
            // dNSLookupToolStripMenuItem
            // 
            this.dNSLookupToolStripMenuItem.Enabled = false;
            this.dNSLookupToolStripMenuItem.Name = "dNSLookupToolStripMenuItem";
            this.dNSLookupToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.dNSLookupToolStripMenuItem.Text = "DNS Lookup";
            this.dNSLookupToolStripMenuItem.Click += new System.EventHandler(this.dNSLookupToolStripMenuItem_Click);
            // 
            // iPScanToolStripMenuItem
            // 
            this.iPScanToolStripMenuItem.Enabled = false;
            this.iPScanToolStripMenuItem.Name = "iPScanToolStripMenuItem";
            this.iPScanToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.iPScanToolStripMenuItem.Text = "IP Scan";
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.Enabled = false;
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.optionsToolStripMenuItem.Text = "&Options";
            // 
            // debugViewToolStripMenuItem
            // 
            this.debugViewToolStripMenuItem.Name = "debugViewToolStripMenuItem";
            this.debugViewToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.debugViewToolStripMenuItem.Text = "Debug View";
            this.debugViewToolStripMenuItem.Click += new System.EventHandler(this.debugViewToolStripMenuItem_Click);
            // 
            // reportsToolStripMenuItem
            // 
            this.reportsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.systemReportToolStripMenuItem});
            this.reportsToolStripMenuItem.Name = "reportsToolStripMenuItem";
            this.reportsToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.reportsToolStripMenuItem.Text = "Reports";
            // 
            // systemReportToolStripMenuItem
            // 
            this.systemReportToolStripMenuItem.Name = "systemReportToolStripMenuItem";
            this.systemReportToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.systemReportToolStripMenuItem.Text = "System report";
            this.systemReportToolStripMenuItem.Click += new System.EventHandler(this.systemReportToolStripMenuItem_Click);
            // 
            // helpMenu
            // 
            this.helpMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.contentsToolStripMenuItem,
            this.indexToolStripMenuItem,
            this.searchToolStripMenuItem,
            this.toolStripSeparator8,
            this.aboutToolStripMenuItem});
            this.helpMenu.Name = "helpMenu";
            this.helpMenu.Size = new System.Drawing.Size(40, 20);
            this.helpMenu.Text = "&Help";
            // 
            // contentsToolStripMenuItem
            // 
            this.contentsToolStripMenuItem.Name = "contentsToolStripMenuItem";
            this.contentsToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F1)));
            this.contentsToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.contentsToolStripMenuItem.Text = "&Contents";
            // 
            // indexToolStripMenuItem
            // 
            this.indexToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("indexToolStripMenuItem.Image")));
            this.indexToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black;
            this.indexToolStripMenuItem.Name = "indexToolStripMenuItem";
            this.indexToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.indexToolStripMenuItem.Text = "&Index";
            // 
            // searchToolStripMenuItem
            // 
            this.searchToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("searchToolStripMenuItem.Image")));
            this.searchToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black;
            this.searchToolStripMenuItem.Name = "searchToolStripMenuItem";
            this.searchToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.searchToolStripMenuItem.Text = "&Search";
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(170, 6);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.aboutToolStripMenuItem.Text = "&About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsProgBar,
            this.tsStatus});
            this.statusStrip.Location = new System.Drawing.Point(0, 527);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(767, 22);
            this.statusStrip.TabIndex = 2;
            this.statusStrip.Text = "StatusStrip";
            // 
            // tsProgBar
            // 
            this.tsProgBar.Name = "tsProgBar";
            this.tsProgBar.Size = new System.Drawing.Size(100, 16);
            // 
            // tsStatus
            // 
            this.tsStatus.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.tsStatus.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenInner;
            this.tsStatus.Name = "tsStatus";
            this.tsStatus.Size = new System.Drawing.Size(650, 17);
            this.tsStatus.Spring = true;
            this.tsStatus.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 24);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.lstMonitoredMachine);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.label1);
            this.splitContainer1.Panel2.Controls.Add(this.cbScans);
            this.splitContainer1.Panel2.Controls.Add(this.tbCtrl);
            this.splitContainer1.Size = new System.Drawing.Size(767, 503);
            this.splitContainer1.SplitterDistance = 259;
            this.splitContainer1.TabIndex = 4;
            // 
            // lstMonitoredMachine
            // 
            this.lstMonitoredMachine.CheckBoxes = true;
            this.lstMonitoredMachine.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.lstMonitoredMachine.ContextMenuStrip = this.contextMenuStrip1;
            this.lstMonitoredMachine.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstMonitoredMachine.FullRowSelect = true;
            this.lstMonitoredMachine.GridLines = true;
            this.lstMonitoredMachine.HideSelection = false;
            this.lstMonitoredMachine.Location = new System.Drawing.Point(0, 0);
            this.lstMonitoredMachine.MultiSelect = false;
            this.lstMonitoredMachine.Name = "lstMonitoredMachine";
            this.lstMonitoredMachine.Size = new System.Drawing.Size(259, 503);
            this.lstMonitoredMachine.SmallImageList = this.lstIcons;
            this.lstMonitoredMachine.TabIndex = 0;
            this.lstMonitoredMachine.UseCompatibleStateImageBehavior = false;
            this.lstMonitoredMachine.View = System.Windows.Forms.View.Details;
            this.lstMonitoredMachine.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.lstMonitoredMachine_ItemSelectionChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Machine";
            this.columnHeader1.Width = 81;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "IP address";
            this.columnHeader2.Width = 98;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Status";
            this.columnHeader3.Width = 140;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.scanToolStripMenuItem,
            this.deleteMachineToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(159, 48);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // scanToolStripMenuItem
            // 
            this.scanToolStripMenuItem.Name = "scanToolStripMenuItem";
            this.scanToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.scanToolStripMenuItem.Text = "Scan";
            this.scanToolStripMenuItem.Click += new System.EventHandler(this.scanToolStripMenuItem_Click);
            // 
            // deleteMachineToolStripMenuItem
            // 
            this.deleteMachineToolStripMenuItem.Name = "deleteMachineToolStripMenuItem";
            this.deleteMachineToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.deleteMachineToolStripMenuItem.Text = "Delete machine";
            this.deleteMachineToolStripMenuItem.Click += new System.EventHandler(this.deleteMachineToolStripMenuItem_Click);
            // 
            // lstIcons
            // 
            this.lstIcons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("lstIcons.ImageStream")));
            this.lstIcons.TransparentColor = System.Drawing.Color.Transparent;
            this.lstIcons.Images.SetKeyName(0, "Ok.png");
            this.lstIcons.Images.SetKeyName(1, "Error.png");
            this.lstIcons.Images.SetKeyName(2, "Ok.png");
            this.lstIcons.Images.SetKeyName(3, "Ok.png");
            this.lstIcons.Images.SetKeyName(4, "Ok.png");
            this.lstIcons.Images.SetKeyName(5, "Ok.png");
            this.lstIcons.Images.SetKeyName(6, "Ok.png");
            this.lstIcons.Images.SetKeyName(7, "Ok.png");
            this.lstIcons.Images.SetKeyName(8, "Ok.png");
            this.lstIcons.Images.SetKeyName(9, "Ok.png");
            this.lstIcons.Images.SetKeyName(10, "Ok.png");
            this.lstIcons.Images.SetKeyName(11, "Ok.png");
            this.lstIcons.Images.SetKeyName(12, "Ok.png");
            this.lstIcons.Images.SetKeyName(13, "Ok.png");
            this.lstIcons.Images.SetKeyName(14, "Ok.png");
            this.lstIcons.Images.SetKeyName(15, "Ok.png");
            this.lstIcons.Images.SetKeyName(16, "Ok.png");
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Scans";
            // 
            // cbScans
            // 
            this.cbScans.FormattingEnabled = true;
            this.cbScans.Location = new System.Drawing.Point(46, 6);
            this.cbScans.Name = "cbScans";
            this.cbScans.Size = new System.Drawing.Size(180, 21);
            this.cbScans.TabIndex = 4;
            this.cbScans.SelectedIndexChanged += new System.EventHandler(this.cbScans_SelectedIndexChanged);
            // 
            // tbCtrl
            // 
            this.tbCtrl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tbCtrl.Controls.Add(this.tabSystem);
            this.tbCtrl.Controls.Add(this.tabSoftwares);
            this.tbCtrl.Controls.Add(this.tabProcesses);
            this.tbCtrl.Controls.Add(this.tabServices);
            this.tbCtrl.Controls.Add(this.tabIPRoutes);
            this.tbCtrl.Controls.Add(this.tabEnvVars);
            this.tbCtrl.Controls.Add(this.tabUserGroups);
            this.tbCtrl.Location = new System.Drawing.Point(0, 33);
            this.tbCtrl.Name = "tbCtrl";
            this.tbCtrl.SelectedIndex = 0;
            this.tbCtrl.Size = new System.Drawing.Size(504, 470);
            this.tbCtrl.TabIndex = 0;
            // 
            // tabSystem
            // 
            this.tabSystem.Controls.Add(this.cbListExpand);
            this.tabSystem.Controls.Add(this.panel1);
            this.tabSystem.Location = new System.Drawing.Point(4, 22);
            this.tabSystem.Name = "tabSystem";
            this.tabSystem.Padding = new System.Windows.Forms.Padding(3);
            this.tabSystem.Size = new System.Drawing.Size(496, 444);
            this.tabSystem.TabIndex = 0;
            this.tabSystem.Text = "System";
            this.tabSystem.UseVisualStyleBackColor = true;
            // 
            // cbListExpand
            // 
            this.cbListExpand.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbListExpand.AutoSize = true;
            this.cbListExpand.Location = new System.Drawing.Point(348, 11);
            this.cbListExpand.Name = "cbListExpand";
            this.cbListExpand.Size = new System.Drawing.Size(141, 17);
            this.cbListExpand.TabIndex = 4;
            this.cbListExpand.Text = "Expand list automatically";
            this.cbListExpand.UseVisualStyleBackColor = true;
            this.cbListExpand.CheckedChanged += new System.EventHandler(this.cbListExpand_CheckedChanged);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.lblCollectionStatus);
            this.panel1.Controls.Add(this.lstDisplayInfo);
            this.panel1.Location = new System.Drawing.Point(3, 34);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(490, 407);
            this.panel1.TabIndex = 1;
            // 
            // lblCollectionStatus
            // 
            this.lblCollectionStatus.AutoSize = true;
            this.lblCollectionStatus.BackColor = System.Drawing.SystemColors.Window;
            this.lblCollectionStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCollectionStatus.Location = new System.Drawing.Point(6, 31);
            this.lblCollectionStatus.Name = "lblCollectionStatus";
            this.lblCollectionStatus.Size = new System.Drawing.Size(0, 20);
            this.lblCollectionStatus.TabIndex = 3;
            // 
            // lstDisplayInfo
            // 
            this.lstDisplayInfo.BackColor = System.Drawing.SystemColors.Window;
            this.lstDisplayInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            toggleColumnHeader1.Hovered = false;
            toggleColumnHeader1.Image = null;
            toggleColumnHeader1.Index = 0;
            toggleColumnHeader1.Pressed = false;
            toggleColumnHeader1.ScaleStyle = ExtendedListView.Forms.ColumnScaleStyle.Slide;
            toggleColumnHeader1.Selected = false;
            toggleColumnHeader1.Text = null;
            toggleColumnHeader1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            toggleColumnHeader1.Visible = true;
            toggleColumnHeader1.Width = 400;
            toggleColumnHeader2.Hovered = false;
            toggleColumnHeader2.Image = null;
            toggleColumnHeader2.Index = 0;
            toggleColumnHeader2.Pressed = false;
            toggleColumnHeader2.ScaleStyle = ExtendedListView.Forms.ColumnScaleStyle.Slide;
            toggleColumnHeader2.Selected = false;
            toggleColumnHeader2.Text = null;
            toggleColumnHeader2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            toggleColumnHeader2.Visible = true;
            toggleColumnHeader2.Width = 400;
            this.lstDisplayInfo.Columns.AddRange(new ExtendedListView.Forms.ToggleColumnHeader[] {
            toggleColumnHeader1,
            toggleColumnHeader2});
            this.lstDisplayInfo.ColumnSortColor = System.Drawing.Color.Gainsboro;
            this.lstDisplayInfo.ColumnTrackColor = System.Drawing.Color.WhiteSmoke;
            this.lstDisplayInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstDisplayInfo.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstDisplayInfo.GridLineColor = System.Drawing.Color.WhiteSmoke;
            this.lstDisplayInfo.GridLines = true;
            this.lstDisplayInfo.HeaderMenu = null;
            this.lstDisplayInfo.ItemHeight = 17;
            this.lstDisplayInfo.ItemMenu = null;
            this.lstDisplayInfo.LabelEdit = false;
            this.lstDisplayInfo.Location = new System.Drawing.Point(0, 0);
            this.lstDisplayInfo.Name = "lstDisplayInfo";
            this.lstDisplayInfo.RowSelectColor = System.Drawing.SystemColors.Highlight;
            this.lstDisplayInfo.RowTrackColor = System.Drawing.Color.WhiteSmoke;
            this.lstDisplayInfo.ShowLines = true;
            this.lstDisplayInfo.ShowRootLines = true;
            this.lstDisplayInfo.Size = new System.Drawing.Size(486, 403);
            this.lstDisplayInfo.SmallImageList = this.lstIcons;
            this.lstDisplayInfo.StateImageList = null;
            this.lstDisplayInfo.TabIndex = 2;
            this.lstDisplayInfo.Text = "treeListView1";
            // 
            // tabSoftwares
            // 
            this.tabSoftwares.Controls.Add(this.lstSoftwares);
            this.tabSoftwares.Location = new System.Drawing.Point(4, 22);
            this.tabSoftwares.Name = "tabSoftwares";
            this.tabSoftwares.Padding = new System.Windows.Forms.Padding(3);
            this.tabSoftwares.Size = new System.Drawing.Size(496, 444);
            this.tabSoftwares.TabIndex = 3;
            this.tabSoftwares.Text = "Softwares";
            this.tabSoftwares.UseVisualStyleBackColor = true;
            // 
            // lstSoftwares
            // 
            this.lstSoftwares.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstSoftwares.FullRowSelect = true;
            this.lstSoftwares.GridLines = true;
            this.lstSoftwares.Location = new System.Drawing.Point(3, 3);
            this.lstSoftwares.Name = "lstSoftwares";
            this.lstSoftwares.Size = new System.Drawing.Size(490, 438);
            this.lstSoftwares.SmallImageList = this.lstIcons;
            this.lstSoftwares.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.lstSoftwares.TabIndex = 7;
            this.lstSoftwares.UseCompatibleStateImageBehavior = false;
            this.lstSoftwares.View = System.Windows.Forms.View.Details;
            // 
            // tabProcesses
            // 
            this.tabProcesses.Controls.Add(this.lstProcesses);
            this.tabProcesses.Location = new System.Drawing.Point(4, 22);
            this.tabProcesses.Name = "tabProcesses";
            this.tabProcesses.Padding = new System.Windows.Forms.Padding(3);
            this.tabProcesses.Size = new System.Drawing.Size(496, 444);
            this.tabProcesses.TabIndex = 1;
            this.tabProcesses.Text = "Processes";
            this.tabProcesses.UseVisualStyleBackColor = true;
            // 
            // lstProcesses
            // 
            this.lstProcesses.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstProcesses.FullRowSelect = true;
            this.lstProcesses.GridLines = true;
            this.lstProcesses.Location = new System.Drawing.Point(3, 3);
            this.lstProcesses.Name = "lstProcesses";
            this.lstProcesses.Size = new System.Drawing.Size(490, 438);
            this.lstProcesses.SmallImageList = this.lstIcons;
            this.lstProcesses.TabIndex = 5;
            this.lstProcesses.UseCompatibleStateImageBehavior = false;
            this.lstProcesses.View = System.Windows.Forms.View.Details;
            // 
            // tabServices
            // 
            this.tabServices.Controls.Add(this.lstServices);
            this.tabServices.Location = new System.Drawing.Point(4, 22);
            this.tabServices.Name = "tabServices";
            this.tabServices.Padding = new System.Windows.Forms.Padding(3);
            this.tabServices.Size = new System.Drawing.Size(496, 444);
            this.tabServices.TabIndex = 4;
            this.tabServices.Text = "Services";
            this.tabServices.UseVisualStyleBackColor = true;
            // 
            // lstServices
            // 
            this.lstServices.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstServices.FullRowSelect = true;
            this.lstServices.GridLines = true;
            this.lstServices.Location = new System.Drawing.Point(3, 3);
            this.lstServices.Name = "lstServices";
            this.lstServices.Size = new System.Drawing.Size(490, 438);
            this.lstServices.SmallImageList = this.lstIcons;
            this.lstServices.TabIndex = 6;
            this.lstServices.UseCompatibleStateImageBehavior = false;
            this.lstServices.View = System.Windows.Forms.View.Details;
            // 
            // tabIPRoutes
            // 
            this.tabIPRoutes.Controls.Add(this.lstIPRoutes);
            this.tabIPRoutes.Location = new System.Drawing.Point(4, 22);
            this.tabIPRoutes.Name = "tabIPRoutes";
            this.tabIPRoutes.Padding = new System.Windows.Forms.Padding(3);
            this.tabIPRoutes.Size = new System.Drawing.Size(496, 444);
            this.tabIPRoutes.TabIndex = 5;
            this.tabIPRoutes.Text = "IP Routes";
            this.tabIPRoutes.UseVisualStyleBackColor = true;
            // 
            // lstIPRoutes
            // 
            this.lstIPRoutes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstIPRoutes.FullRowSelect = true;
            this.lstIPRoutes.GridLines = true;
            this.lstIPRoutes.Location = new System.Drawing.Point(3, 3);
            this.lstIPRoutes.Name = "lstIPRoutes";
            this.lstIPRoutes.Size = new System.Drawing.Size(490, 438);
            this.lstIPRoutes.SmallImageList = this.lstIcons;
            this.lstIPRoutes.TabIndex = 7;
            this.lstIPRoutes.UseCompatibleStateImageBehavior = false;
            this.lstIPRoutes.View = System.Windows.Forms.View.Details;
            // 
            // tabEnvVars
            // 
            this.tabEnvVars.Controls.Add(this.lstEnvVars);
            this.tabEnvVars.Location = new System.Drawing.Point(4, 22);
            this.tabEnvVars.Name = "tabEnvVars";
            this.tabEnvVars.Padding = new System.Windows.Forms.Padding(3);
            this.tabEnvVars.Size = new System.Drawing.Size(496, 444);
            this.tabEnvVars.TabIndex = 6;
            this.tabEnvVars.Text = "Environment Variables";
            this.tabEnvVars.UseVisualStyleBackColor = true;
            // 
            // lstEnvVars
            // 
            this.lstEnvVars.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstEnvVars.FullRowSelect = true;
            this.lstEnvVars.GridLines = true;
            this.lstEnvVars.LabelWrap = false;
            this.lstEnvVars.Location = new System.Drawing.Point(3, 3);
            this.lstEnvVars.Name = "lstEnvVars";
            this.lstEnvVars.Size = new System.Drawing.Size(490, 438);
            this.lstEnvVars.SmallImageList = this.lstIcons;
            this.lstEnvVars.TabIndex = 8;
            this.lstEnvVars.UseCompatibleStateImageBehavior = false;
            this.lstEnvVars.View = System.Windows.Forms.View.Details;
            // 
            // tabUserGroups
            // 
            this.tabUserGroups.Controls.Add(this.lstUserGroups);
            this.tabUserGroups.Location = new System.Drawing.Point(4, 22);
            this.tabUserGroups.Name = "tabUserGroups";
            this.tabUserGroups.Padding = new System.Windows.Forms.Padding(3);
            this.tabUserGroups.Size = new System.Drawing.Size(496, 444);
            this.tabUserGroups.TabIndex = 7;
            this.tabUserGroups.Text = "UserGroups";
            this.tabUserGroups.UseVisualStyleBackColor = true;
            // 
            // lstUserGroups
            // 
            this.lstUserGroups.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstUserGroups.FullRowSelect = true;
            this.lstUserGroups.GridLines = true;
            this.lstUserGroups.LabelWrap = false;
            this.lstUserGroups.Location = new System.Drawing.Point(3, 3);
            this.lstUserGroups.Name = "lstUserGroups";
            this.lstUserGroups.Size = new System.Drawing.Size(490, 438);
            this.lstUserGroups.SmallImageList = this.lstIcons;
            this.lstUserGroups.TabIndex = 9;
            this.lstUserGroups.UseCompatibleStateImageBehavior = false;
            this.lstUserGroups.View = System.Windows.Forms.View.Details;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(256, 11);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(141, 17);
            this.checkBox1.TabIndex = 4;
            this.checkBox1.Text = "Expand list automatically";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Scans";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(49, 9);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(180, 21);
            this.comboBox1.TabIndex = 2;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.treeListView1);
            this.panel2.Location = new System.Drawing.Point(3, 34);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(490, 440);
            this.panel2.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.SystemColors.Window;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(6, 31);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 20);
            this.label3.TabIndex = 3;
            // 
            // treeListView1
            // 
            this.treeListView1.BackColor = System.Drawing.SystemColors.Window;
            this.treeListView1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            toggleColumnHeader3.Hovered = false;
            toggleColumnHeader3.Image = null;
            toggleColumnHeader3.Index = 0;
            toggleColumnHeader3.Pressed = false;
            toggleColumnHeader3.ScaleStyle = ExtendedListView.Forms.ColumnScaleStyle.Slide;
            toggleColumnHeader3.Selected = false;
            toggleColumnHeader3.Text = null;
            toggleColumnHeader3.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            toggleColumnHeader3.Visible = true;
            toggleColumnHeader3.Width = 400;
            toggleColumnHeader4.Hovered = false;
            toggleColumnHeader4.Image = null;
            toggleColumnHeader4.Index = 0;
            toggleColumnHeader4.Pressed = false;
            toggleColumnHeader4.ScaleStyle = ExtendedListView.Forms.ColumnScaleStyle.Slide;
            toggleColumnHeader4.Selected = false;
            toggleColumnHeader4.Text = null;
            toggleColumnHeader4.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            toggleColumnHeader4.Visible = true;
            toggleColumnHeader4.Width = 400;
            this.treeListView1.Columns.AddRange(new ExtendedListView.Forms.ToggleColumnHeader[] {
            toggleColumnHeader3,
            toggleColumnHeader4});
            this.treeListView1.ColumnSortColor = System.Drawing.Color.Gainsboro;
            this.treeListView1.ColumnTrackColor = System.Drawing.Color.WhiteSmoke;
            this.treeListView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeListView1.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.treeListView1.GridLineColor = System.Drawing.Color.WhiteSmoke;
            this.treeListView1.GridLines = true;
            this.treeListView1.HeaderMenu = null;
            this.treeListView1.ItemHeight = 17;
            this.treeListView1.ItemMenu = null;
            this.treeListView1.LabelEdit = false;
            this.treeListView1.Location = new System.Drawing.Point(0, 0);
            this.treeListView1.Name = "treeListView1";
            this.treeListView1.RowSelectColor = System.Drawing.SystemColors.Highlight;
            this.treeListView1.RowTrackColor = System.Drawing.Color.WhiteSmoke;
            this.treeListView1.ShowLines = true;
            this.treeListView1.ShowRootLines = true;
            this.treeListView1.Size = new System.Drawing.Size(486, 436);
            this.treeListView1.SmallImageList = this.lstIcons;
            this.treeListView1.StateImageList = null;
            this.treeListView1.TabIndex = 2;
            this.treeListView1.Text = "treeListView1";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.SystemColors.Window;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 31);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(0, 20);
            this.label4.TabIndex = 3;
            // 
            // treeListView2
            // 
            this.treeListView2.BackColor = System.Drawing.SystemColors.Window;
            this.treeListView2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            toggleColumnHeader5.Hovered = false;
            toggleColumnHeader5.Image = null;
            toggleColumnHeader5.Index = 0;
            toggleColumnHeader5.Pressed = false;
            toggleColumnHeader5.ScaleStyle = ExtendedListView.Forms.ColumnScaleStyle.Slide;
            toggleColumnHeader5.Selected = false;
            toggleColumnHeader5.Text = null;
            toggleColumnHeader5.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            toggleColumnHeader5.Visible = true;
            toggleColumnHeader5.Width = 400;
            toggleColumnHeader6.Hovered = false;
            toggleColumnHeader6.Image = null;
            toggleColumnHeader6.Index = 0;
            toggleColumnHeader6.Pressed = false;
            toggleColumnHeader6.ScaleStyle = ExtendedListView.Forms.ColumnScaleStyle.Slide;
            toggleColumnHeader6.Selected = false;
            toggleColumnHeader6.Text = null;
            toggleColumnHeader6.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            toggleColumnHeader6.Visible = true;
            toggleColumnHeader6.Width = 400;
            this.treeListView2.Columns.AddRange(new ExtendedListView.Forms.ToggleColumnHeader[] {
            toggleColumnHeader5,
            toggleColumnHeader6});
            this.treeListView2.ColumnSortColor = System.Drawing.Color.Gainsboro;
            this.treeListView2.ColumnTrackColor = System.Drawing.Color.WhiteSmoke;
            this.treeListView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeListView2.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.treeListView2.GridLineColor = System.Drawing.Color.WhiteSmoke;
            this.treeListView2.GridLines = true;
            this.treeListView2.HeaderMenu = null;
            this.treeListView2.ItemHeight = 17;
            this.treeListView2.ItemMenu = null;
            this.treeListView2.LabelEdit = false;
            this.treeListView2.Location = new System.Drawing.Point(0, 0);
            this.treeListView2.Name = "treeListView2";
            this.treeListView2.RowSelectColor = System.Drawing.SystemColors.Highlight;
            this.treeListView2.RowTrackColor = System.Drawing.Color.WhiteSmoke;
            this.treeListView2.ShowLines = true;
            this.treeListView2.ShowRootLines = true;
            this.treeListView2.Size = new System.Drawing.Size(486, 436);
            this.treeListView2.SmallImageList = this.lstIcons;
            this.treeListView2.StateImageList = null;
            this.treeListView2.TabIndex = 2;
            this.treeListView2.Text = "treeListView1";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.checkBox2);
            this.tabPage1.Controls.Add(this.panel3);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(496, 444);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "System";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // checkBox2
            // 
            this.checkBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(348, 11);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(141, 17);
            this.checkBox2.TabIndex = 4;
            this.checkBox2.Text = "Expand list automatically";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.treeListView3);
            this.panel3.Location = new System.Drawing.Point(3, 34);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(490, 407);
            this.panel3.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.SystemColors.Window;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(6, 31);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(0, 20);
            this.label5.TabIndex = 3;
            // 
            // treeListView3
            // 
            this.treeListView3.BackColor = System.Drawing.SystemColors.Window;
            this.treeListView3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            toggleColumnHeader7.Hovered = false;
            toggleColumnHeader7.Image = null;
            toggleColumnHeader7.Index = 0;
            toggleColumnHeader7.Pressed = false;
            toggleColumnHeader7.ScaleStyle = ExtendedListView.Forms.ColumnScaleStyle.Slide;
            toggleColumnHeader7.Selected = false;
            toggleColumnHeader7.Text = null;
            toggleColumnHeader7.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            toggleColumnHeader7.Visible = true;
            toggleColumnHeader7.Width = 400;
            toggleColumnHeader8.Hovered = false;
            toggleColumnHeader8.Image = null;
            toggleColumnHeader8.Index = 0;
            toggleColumnHeader8.Pressed = false;
            toggleColumnHeader8.ScaleStyle = ExtendedListView.Forms.ColumnScaleStyle.Slide;
            toggleColumnHeader8.Selected = false;
            toggleColumnHeader8.Text = null;
            toggleColumnHeader8.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            toggleColumnHeader8.Visible = true;
            toggleColumnHeader8.Width = 400;
            this.treeListView3.Columns.AddRange(new ExtendedListView.Forms.ToggleColumnHeader[] {
            toggleColumnHeader7,
            toggleColumnHeader8});
            this.treeListView3.ColumnSortColor = System.Drawing.Color.Gainsboro;
            this.treeListView3.ColumnTrackColor = System.Drawing.Color.WhiteSmoke;
            this.treeListView3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeListView3.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.treeListView3.GridLineColor = System.Drawing.Color.WhiteSmoke;
            this.treeListView3.GridLines = true;
            this.treeListView3.HeaderMenu = null;
            this.treeListView3.ItemHeight = 17;
            this.treeListView3.ItemMenu = null;
            this.treeListView3.LabelEdit = false;
            this.treeListView3.Location = new System.Drawing.Point(0, 0);
            this.treeListView3.Name = "treeListView3";
            this.treeListView3.RowSelectColor = System.Drawing.SystemColors.Highlight;
            this.treeListView3.RowTrackColor = System.Drawing.Color.WhiteSmoke;
            this.treeListView3.ShowLines = true;
            this.treeListView3.ShowRootLines = true;
            this.treeListView3.Size = new System.Drawing.Size(486, 403);
            this.treeListView3.SmallImageList = this.lstIcons;
            this.treeListView3.StateImageList = null;
            this.treeListView3.TabIndex = 2;
            this.treeListView3.Text = "treeListView1";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.listView1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(496, 444);
            this.tabPage2.TabIndex = 3;
            this.tabPage2.Text = "Softwares";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // listView1
            // 
            this.listView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView1.FullRowSelect = true;
            this.listView1.GridLines = true;
            this.listView1.Location = new System.Drawing.Point(3, 3);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(490, 438);
            this.listView1.SmallImageList = this.lstIcons;
            this.listView1.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.listView1.TabIndex = 7;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.listView2);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(496, 444);
            this.tabPage3.TabIndex = 1;
            this.tabPage3.Text = "Processes";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // listView2
            // 
            this.listView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView2.FullRowSelect = true;
            this.listView2.GridLines = true;
            this.listView2.Location = new System.Drawing.Point(3, 3);
            this.listView2.Name = "listView2";
            this.listView2.Size = new System.Drawing.Size(490, 438);
            this.listView2.SmallImageList = this.lstIcons;
            this.listView2.TabIndex = 5;
            this.listView2.UseCompatibleStateImageBehavior = false;
            this.listView2.View = System.Windows.Forms.View.Details;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.listView3);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(496, 444);
            this.tabPage4.TabIndex = 2;
            this.tabPage4.Text = "Services";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // listView3
            // 
            this.listView3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView3.FullRowSelect = true;
            this.listView3.GridLines = true;
            this.listView3.Location = new System.Drawing.Point(0, 0);
            this.listView3.Name = "listView3";
            this.listView3.Size = new System.Drawing.Size(496, 444);
            this.listView3.SmallImageList = this.lstIcons;
            this.listView3.TabIndex = 6;
            this.listView3.UseCompatibleStateImageBehavior = false;
            this.listView3.View = System.Windows.Forms.View.Details;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.checkBox3);
            this.tabPage5.Controls.Add(this.panel4);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(496, 444);
            this.tabPage5.TabIndex = 0;
            this.tabPage5.Text = "System";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // checkBox3
            // 
            this.checkBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(348, 11);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(141, 17);
            this.checkBox3.TabIndex = 4;
            this.checkBox3.Text = "Expand list automatically";
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // panel4
            // 
            this.panel4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel4.Controls.Add(this.label6);
            this.panel4.Controls.Add(this.treeListView4);
            this.panel4.Location = new System.Drawing.Point(3, 34);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(490, 407);
            this.panel4.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.SystemColors.Window;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(6, 31);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(0, 20);
            this.label6.TabIndex = 3;
            // 
            // treeListView4
            // 
            this.treeListView4.BackColor = System.Drawing.SystemColors.Window;
            this.treeListView4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            toggleColumnHeader9.Hovered = false;
            toggleColumnHeader9.Image = null;
            toggleColumnHeader9.Index = 0;
            toggleColumnHeader9.Pressed = false;
            toggleColumnHeader9.ScaleStyle = ExtendedListView.Forms.ColumnScaleStyle.Slide;
            toggleColumnHeader9.Selected = false;
            toggleColumnHeader9.Text = null;
            toggleColumnHeader9.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            toggleColumnHeader9.Visible = true;
            toggleColumnHeader9.Width = 400;
            toggleColumnHeader10.Hovered = false;
            toggleColumnHeader10.Image = null;
            toggleColumnHeader10.Index = 0;
            toggleColumnHeader10.Pressed = false;
            toggleColumnHeader10.ScaleStyle = ExtendedListView.Forms.ColumnScaleStyle.Slide;
            toggleColumnHeader10.Selected = false;
            toggleColumnHeader10.Text = null;
            toggleColumnHeader10.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            toggleColumnHeader10.Visible = true;
            toggleColumnHeader10.Width = 400;
            this.treeListView4.Columns.AddRange(new ExtendedListView.Forms.ToggleColumnHeader[] {
            toggleColumnHeader9,
            toggleColumnHeader10});
            this.treeListView4.ColumnSortColor = System.Drawing.Color.Gainsboro;
            this.treeListView4.ColumnTrackColor = System.Drawing.Color.WhiteSmoke;
            this.treeListView4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeListView4.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.treeListView4.GridLineColor = System.Drawing.Color.WhiteSmoke;
            this.treeListView4.GridLines = true;
            this.treeListView4.HeaderMenu = null;
            this.treeListView4.ItemHeight = 17;
            this.treeListView4.ItemMenu = null;
            this.treeListView4.LabelEdit = false;
            this.treeListView4.Location = new System.Drawing.Point(0, 0);
            this.treeListView4.Name = "treeListView4";
            this.treeListView4.RowSelectColor = System.Drawing.SystemColors.Highlight;
            this.treeListView4.RowTrackColor = System.Drawing.Color.WhiteSmoke;
            this.treeListView4.ShowLines = true;
            this.treeListView4.ShowRootLines = true;
            this.treeListView4.Size = new System.Drawing.Size(486, 403);
            this.treeListView4.SmallImageList = this.lstIcons;
            this.treeListView4.StateImageList = null;
            this.treeListView4.TabIndex = 2;
            this.treeListView4.Text = "treeListView1";
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.listView4);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(496, 444);
            this.tabPage6.TabIndex = 3;
            this.tabPage6.Text = "Softwares";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // listView4
            // 
            this.listView4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView4.FullRowSelect = true;
            this.listView4.GridLines = true;
            this.listView4.Location = new System.Drawing.Point(3, 3);
            this.listView4.Name = "listView4";
            this.listView4.Size = new System.Drawing.Size(490, 438);
            this.listView4.SmallImageList = this.lstIcons;
            this.listView4.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.listView4.TabIndex = 7;
            this.listView4.UseCompatibleStateImageBehavior = false;
            this.listView4.View = System.Windows.Forms.View.Details;
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.listView5);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(496, 444);
            this.tabPage7.TabIndex = 1;
            this.tabPage7.Text = "Processes";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // listView5
            // 
            this.listView5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView5.FullRowSelect = true;
            this.listView5.GridLines = true;
            this.listView5.Location = new System.Drawing.Point(3, 3);
            this.listView5.Name = "listView5";
            this.listView5.Size = new System.Drawing.Size(490, 438);
            this.listView5.SmallImageList = this.lstIcons;
            this.listView5.TabIndex = 5;
            this.listView5.UseCompatibleStateImageBehavior = false;
            this.listView5.View = System.Windows.Forms.View.Details;
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this.listView6);
            this.tabPage8.Location = new System.Drawing.Point(4, 22);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Size = new System.Drawing.Size(496, 444);
            this.tabPage8.TabIndex = 2;
            this.tabPage8.Text = "Services";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // listView6
            // 
            this.listView6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView6.FullRowSelect = true;
            this.listView6.GridLines = true;
            this.listView6.Location = new System.Drawing.Point(0, 0);
            this.listView6.Name = "listView6";
            this.listView6.Size = new System.Drawing.Size(496, 444);
            this.listView6.SmallImageList = this.lstIcons;
            this.listView6.TabIndex = 6;
            this.listView6.UseCompatibleStateImageBehavior = false;
            this.listView6.View = System.Windows.Forms.View.Details;
            // 
            // MainUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(767, 549);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.menuStrip);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip;
            this.Name = "MainUI";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Network Asset Manager";
            this.Load += new System.EventHandler(this.MainUI_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainUI_FormClosing);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            this.splitContainer1.ResumeLayout(false);
            this.contextMenuStrip1.ResumeLayout(false);
            this.tbCtrl.ResumeLayout(false);
            this.tabSystem.ResumeLayout(false);
            this.tabSystem.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tabSoftwares.ResumeLayout(false);
            this.tabProcesses.ResumeLayout(false);
            this.tabServices.ResumeLayout(false);
            this.tabIPRoutes.ResumeLayout(false);
            this.tabEnvVars.ResumeLayout(false);
            this.tabUserGroups.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.tabPage6.ResumeLayout(false);
            this.tabPage7.ResumeLayout(false);
            this.tabPage8.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion


        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolsMenu;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpMenu;
        private System.Windows.Forms.ToolStripMenuItem contentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem indexToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem searchToolStripMenuItem;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem configurationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addMachinesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addComputersByHostnameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dNSLookupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem iPScanToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem addComputersByDomainToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addComputersByIpRangeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addComputersFromFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem deleteComputerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem scanSelectedComputerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem scanAllComputerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteCurScanToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem viewSystemInformationToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem credentialsConfigToolStripMenuItem1;
        private System.Windows.Forms.ListView lstMonitoredMachine;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ToolStripMenuItem debugViewToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem scanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteMachineToolStripMenuItem;
        private System.Windows.Forms.ImageList lstIcons;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label3;
        private ExtendedListView.Forms.TreeListView treeListView1;
        private System.Windows.Forms.Label label4;
        private ExtendedListView.Forms.TreeListView treeListView2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbScans;
        private System.Windows.Forms.ToolStripProgressBar tsProgBar;
        private System.Windows.Forms.ToolStripStatusLabel tsStatus;
        private System.Windows.Forms.ToolStripMenuItem reportsToolStripMenuItem;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label5;
        private ExtendedListView.Forms.TreeListView treeListView3;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.ListView listView2;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.ListView listView3;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label6;
        private ExtendedListView.Forms.TreeListView treeListView4;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.ListView listView4;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.ListView listView5;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.ListView listView6;
        private System.Windows.Forms.TabControl tbCtrl;
        private System.Windows.Forms.TabPage tabSystem;
        private System.Windows.Forms.CheckBox cbListExpand;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblCollectionStatus;
        private ExtendedListView.Forms.TreeListView lstDisplayInfo;
        private System.Windows.Forms.TabPage tabSoftwares;
        private System.Windows.Forms.ListView lstSoftwares;
        private System.Windows.Forms.TabPage tabProcesses;
        private System.Windows.Forms.ListView lstProcesses;
        private System.Windows.Forms.TabPage tabServices;
        private System.Windows.Forms.ListView lstServices;
        private System.Windows.Forms.ToolStripMenuItem systemReportToolStripMenuItem;
        private System.Windows.Forms.TabPage tabIPRoutes;
        private System.Windows.Forms.ListView lstIPRoutes;
        private System.Windows.Forms.TabPage tabEnvVars;
        private System.Windows.Forms.ListView lstEnvVars;
        private System.Windows.Forms.TabPage tabUserGroups;
        private System.Windows.Forms.ListView lstUserGroups;
        private System.Windows.Forms.ToolStripMenuItem credentialsMapToolStripMenuItem;
    }
}



