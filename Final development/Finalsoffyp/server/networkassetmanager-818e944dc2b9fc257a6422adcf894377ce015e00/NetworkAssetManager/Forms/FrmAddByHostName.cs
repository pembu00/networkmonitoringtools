﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NetworkAssetManager.Forms
{
    public partial class FrmAddByHostName : Form
    {
        public FrmAddByHostName()
        {
            InitializeComponent();
        }

        public string HostName 
        {
            get
            {
                return txtHostName.Text; 
            }
        }
    }
}
