﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NetworkAssetManager.Forms
{
    public partial class FrmAddByIPRange : Form
    {
        public FrmAddByIPRange()
        {
            InitializeComponent();
        }
        public bool OnlyActive
        {
            get
            {
                return chkLive.Checked; 
            }
        }
    }
}
