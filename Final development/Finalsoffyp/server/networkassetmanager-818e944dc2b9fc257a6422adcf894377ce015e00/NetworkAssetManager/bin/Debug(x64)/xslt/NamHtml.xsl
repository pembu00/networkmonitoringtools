<?xml version='1.0'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <html>
      <STYLE>
        .TableStyle { border-collapse: collapse }
        .ReportHeader {font-family: arial; text-align:left; background-color:white; color:blue; font-weight:bold; font-size:22pt }
        .SectionHeader {font-family: arial; text-align:left; background-color:white; color:blue; font-weight:bold; font-size:14pt }
        .DetailHeader {font-family: arial; text-align:left; background-color:white; color:black; font-weight:bold; font-size:11pt }
        .ColumnHeaderStyle  {font-family: arial; text-align:left; background-color:grey; font-weight:bold; font-size:9pt; border-style:outset; border-width:1}
        .DetailData  {font-family: arial; text-align:left; vertical-align:top; background-color:white; color:black; font-size:9pt }
      </STYLE>
      <body TOPMARGIN='0' LEFTMARGIN='0' RIGHTMARGIN='0' BOTTOMMARGIN='0'>
        <span class='ReportHeader'>
          <xsl:text>Report</xsl:text>
        </span>
        <hr/>
        <br/>

        <xsl:for-each select="Results/Machines">

          <span class='SectionHeader'>
            <xsl:value-of select="MachineName"/>
          </span>
          <br/>
          <br/>
          <xsl:if test="Bios">
            <span class='DetailHeader'>
              <xsl:text>Bios</xsl:text>
            </span>
            <br/>
            <table class='TableStyle' cellspacing='0' cellpadding='5' border='1'>
              <xsl:if test="Bios/ReleaseDate">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>ReleaseDate</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Bios/Manufacturer">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Manufacturer</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Bios/Version">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Version</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:for-each select="Bios">
                <tbody>
                  <xsl:apply-templates select="."/>
                </tbody>
              </xsl:for-each>
            </table>
            <br/>
            <br/>

          </xsl:if>


          <xsl:if test="CDRom">
            <span class='DetailHeader'>
              <xsl:text>CDRom</xsl:text>
            </span>
            <br/>
            <table class='TableStyle' cellspacing='0' cellpadding='5' border='1'>
              <xsl:if test="CDRom/Name">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Name</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="CDRom/MediaType">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>MediaType</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="CDRom/Manufacturer">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Manufacturer</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="CDRom/Drive">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Drive</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:for-each select="CDRom">
                <tbody>
                  <xsl:apply-templates select="."/>
                </tbody>
              </xsl:for-each>
            </table>
            <br/>
            <br/>

          </xsl:if>


          <xsl:if test="Computer">
            <span class='DetailHeader'>
              <xsl:text>Computer</xsl:text>
            </span>
            <br/>
            <table class='TableStyle' cellspacing='0' cellpadding='5' border='1'>
              <xsl:for-each select="Computer">
                <tbody>
                  <xsl:apply-templates select="."/>
                </tbody>
              </xsl:for-each>
            </table>
            <br/>
            <br/>

          </xsl:if>
          <xsl:if test="Credential">
            <span class='DetailHeader'>
              <xsl:text>Credential</xsl:text>
            </span>
            <br/>
            <table class='TableStyle' cellspacing='0' cellpadding='5' border='1'>
              <xsl:if test="Credential/Username">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Username</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Credential/Password">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Password</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Credential/CredentialID">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>CredentialID</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Credential/CredentialName">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>CredentialName</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:for-each select="Credential">
                <tbody>
                  <xsl:apply-templates select="."/>
                </tbody>
              </xsl:for-each>
            </table>
            <br/>
            <br/>

          </xsl:if>


          <xsl:if test="Discover">
            <span class='DetailHeader'>
              <xsl:text>Discover</xsl:text>
            </span>
            <br/>
            <table class='TableStyle' cellspacing='0' cellpadding='5' border='1'>
              <xsl:if test="Discover/IPAddress">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>IPAddress</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Discover/LastChecked">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>LastChecked</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Discover/Discovered">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Discovered</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Discover/StatusMessage">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>StatusMessage</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Discover/MachineID">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>MachineID</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Discover/DomainName">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>DomainName</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Discover/MachineName">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>MachineName</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Discover/CredentialID">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>CredentialID</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:for-each select="Discover">
                <tbody>
                  <xsl:apply-templates select="."/>
                </tbody>
              </xsl:for-each>
            </table>
            <br/>
            <br/>

          </xsl:if>


          <xsl:if test="Disk">
            <span class='DetailHeader'>
              <xsl:text>Disk</xsl:text>
            </span>
            <br/>
            <table class='TableStyle' cellspacing='0' cellpadding='5' border='1'>
              <xsl:if test="Disk/Caption">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Caption</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Disk/Size">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Size</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Disk/Manufacturer">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Manufacturer</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Disk/Interface">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Interface</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Disk/MediaType">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>MediaType</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Disk/BytesPerSector">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>BytesPerSector</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Disk/Heads">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Heads</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Disk/Cylinders">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Cylinders</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Disk/Sectors">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Sectors</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Disk/Tracks">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Tracks</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Disk/Model">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Model</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:for-each select="Disk">
                <tbody>
                  <xsl:apply-templates select="."/>
                </tbody>
              </xsl:for-each>
            </table>
            <br/>
            <br/>

          </xsl:if>


          <xsl:if test="EnvironmentVars">
            <span class='DetailHeader'>
              <xsl:text>EnvironmentVars</xsl:text>
            </span>
            <br/>
            <table class='TableStyle' cellspacing='0' cellpadding='5' border='1'>
              <xsl:if test="EnvironmentVars/Name">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Name</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="EnvironmentVars/Status">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Status</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="EnvironmentVars/SystemVariable">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>SystemVariable</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="EnvironmentVars/UserName">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>UserName</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="EnvironmentVars/VariableValue">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>VariableValue</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:for-each select="EnvironmentVars">
                <tbody>
                  <xsl:apply-templates select="."/>
                </tbody>
              </xsl:for-each>
            </table>
            <br/>
            <br/>

          </xsl:if>


          <xsl:if test="Hotfixes">
            <span class='DetailHeader'>
              <xsl:text>Hotfixes</xsl:text>
            </span>
            <br/>
            <table class='TableStyle' cellspacing='0' cellpadding='5' border='1'>
              <xsl:if test="Hotfixes/HotfixID">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>HotfixID</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Hotfixes/Description">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Description</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Hotfixes/InstallBy">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>InstallBy</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Hotfixes/FixComments">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>FixComments</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Hotfixes/SPEffect">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>SPEffect</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:for-each select="Hotfixes">
                <tbody>
                  <xsl:apply-templates select="."/>
                </tbody>
              </xsl:for-each>
            </table>
            <br/>
            <br/>

          </xsl:if>


          <xsl:if test="IPRoutes">
            <span class='DetailHeader'>
              <xsl:text>IPRoutes</xsl:text>
            </span>
            <br/>
            <table class='TableStyle' cellspacing='0' cellpadding='5' border='1'>
              <xsl:if test="IPRoutes/Age">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Age</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="IPRoutes/Name">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Name</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="IPRoutes/Description">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Description</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="IPRoutes/Destination">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Destination</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="IPRoutes/Mask">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Mask</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="IPRoutes/Metric1">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Metric1</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="IPRoutes/Metric2">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Metric2</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="IPRoutes/Metric3">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Metric3</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="IPRoutes/Metric4">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Metric4</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="IPRoutes/Metric5">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Metric5</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="IPRoutes/NextHop">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>NextHop</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="IPRoutes/Protocol">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Protocol</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="IPRoutes/Status">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Status</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="IPRoutes/RouteType">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>RouteType</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:for-each select="IPRoutes">
                <tbody>
                  <xsl:apply-templates select="."/>
                </tbody>
              </xsl:for-each>
            </table>
            <br/>
            <br/>

          </xsl:if>


          <xsl:if test="LogicalDrive">
            <span class='DetailHeader'>
              <xsl:text>LogicalDrive</xsl:text>
            </span>
            <br/>
            <table class='TableStyle' cellspacing='0' cellpadding='5' border='1'>
              <xsl:if test="LogicalDrive/Name">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Name</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="LogicalDrive/Description">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Description</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="LogicalDrive/Size">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Size</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="LogicalDrive/FreeSpace">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>FreeSpace</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="LogicalDrive/FileSystem">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>FileSystem</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="LogicalDrive/SerialNumber">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>SerialNumber</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="LogicalDrive/VolumeLabel">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>VolumeLabel</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:for-each select="LogicalDrive">
                <tbody>
                  <xsl:apply-templates select="."/>
                </tbody>
              </xsl:for-each>
            </table>
            <br/>
            <br/>

          </xsl:if>


          <xsl:if test="Memory">
            <span class='DetailHeader'>
              <xsl:text>Memory</xsl:text>
            </span>
            <br/>
            <table class='TableStyle' cellspacing='0' cellpadding='5' border='1'>
              <xsl:if test="Memory/Capacity">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Capacity</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Memory/DeviceLocator">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>DeviceLocator</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Memory/BankLabel">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>BankLabel</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Memory/FormFactor">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>FormFactor</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Memory/MemoryType">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>MemoryType</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Memory/Manufacturer">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Manufacturer</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Memory/Speed">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Speed</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:for-each select="Memory">
                <tbody>
                  <xsl:apply-templates select="."/>
                </tbody>
              </xsl:for-each>
            </table>
            <br/>
            <br/>

          </xsl:if>


          <xsl:if test="Monitor">
            <span class='DetailHeader'>
              <xsl:text>Monitor</xsl:text>
            </span>
            <br/>
            <table class='TableStyle' cellspacing='0' cellpadding='5' border='1'>
              <xsl:if test="Monitor/Name">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Name</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Monitor/ScreenWidth">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>ScreenWidth</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Monitor/ScreenHeight">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>ScreenHeight</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Monitor/Availability">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Availability</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:for-each select="Monitor">
                <tbody>
                  <xsl:apply-templates select="."/>
                </tbody>
              </xsl:for-each>
            </table>
            <br/>
            <br/>

          </xsl:if>


          <xsl:if test="MotherBoard">
            <span class='DetailHeader'>
              <xsl:text>MotherBoard</xsl:text>
            </span>
            <br/>
            <table class='TableStyle' cellspacing='0' cellpadding='5' border='1'>
              <xsl:if test="MotherBoard/Manufacturer">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Manufacturer</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="MotherBoard/Version">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Version</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="MotherBoard/Product">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Product</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="MotherBoard/SerialNo">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>SerialNo</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="MotherBoard/Model">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Model</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:for-each select="MotherBoard">
                <tbody>
                  <xsl:apply-templates select="."/>
                </tbody>
              </xsl:for-each>
            </table>
            <br/>
            <br/>

          </xsl:if>


          <xsl:if test="Multimedia">
            <span class='DetailHeader'>
              <xsl:text>Multimedia</xsl:text>
            </span>
            <br/>
            <table class='TableStyle' cellspacing='0' cellpadding='5' border='1'>
              <xsl:if test="Multimedia/Name">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Name</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Multimedia/Manufacturer">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Manufacturer</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:for-each select="Multimedia">
                <tbody>
                  <xsl:apply-templates select="."/>
                </tbody>
              </xsl:for-each>
            </table>
            <br/>
            <br/>

          </xsl:if>


          <xsl:if test="NetworkAdapter">
            <span class='DetailHeader'>
              <xsl:text>NetworkAdapter</xsl:text>
            </span>
            <br/>
            <table class='TableStyle' cellspacing='0' cellpadding='5' border='1'>
              <xsl:if test="NetworkAdapter/AdapterType">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>AdapterType</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="NetworkAdapter/NetConnectonState">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>NetConnectonState</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="NetworkAdapter/Manufacturer">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Manufacturer</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="NetworkAdapter/Speed">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Speed</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="NetworkAdapter/AdapterIPAddress">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>AdapterIPAddress</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="NetworkAdapter/AdapterMACAddress">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>AdapterMACAddress</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="NetworkAdapter/DHCPEnabled">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>DHCPEnabled</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="NetworkAdapter/DHCPServer">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>DHCPServer</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="NetworkAdapter/DNSDomain">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>DNSDomain</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="NetworkAdapter/WINSPrimaryServer">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>WINSPrimaryServer</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="NetworkAdapter/WINSSecondaryServer">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>WINSSecondaryServer</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:for-each select="NetworkAdapter">
                <tbody>
                  <xsl:apply-templates select="."/>
                </tbody>
              </xsl:for-each>
            </table>
            <br/>
            <br/>

          </xsl:if>


          <xsl:if test="OS">
            <span class='DetailHeader'>
              <xsl:text>OS</xsl:text>
            </span>
            <br/>
            <table class='TableStyle' cellspacing='0' cellpadding='5' border='1'>
              <xsl:for-each select="OS">
                <tbody>
                  <xsl:apply-templates select="."/>
                </tbody>
              </xsl:for-each>
            </table>
            <br/>
            <br/>

          </xsl:if>
          <xsl:if test="Printer">
            <span class='DetailHeader'>
              <xsl:text>Printer</xsl:text>
            </span>
            <br/>
            <table class='TableStyle' cellspacing='0' cellpadding='5' border='1'>
              <xsl:if test="Printer/Attributes">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Attributes</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Printer/Availability">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Availability</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Printer/AveragePagesPerMinute">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>AveragePagesPerMinute</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Printer/Caption">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Caption</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Printer/HorizontalResolution">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>HorizontalResolution</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Printer/VerticalResolution">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>VerticalResolution</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Printer/Local">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Local</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Printer/Network">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Network</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Printer/Shared">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Shared</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Printer/Location">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Location</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Printer/PortName">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>PortName</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Printer/PrinterState">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>PrinterState</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Printer/PrinterStatus">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>PrinterStatus</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Printer/ServerName">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>ServerName</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Printer/ShareName">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>ShareName</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:for-each select="Printer">
                <tbody>
                  <xsl:apply-templates select="."/>
                </tbody>
              </xsl:for-each>
            </table>
            <br/>
            <br/>

          </xsl:if>


          <xsl:if test="Processes">
            <span class='DetailHeader'>
              <xsl:text>Processes</xsl:text>
            </span>
            <br/>
            <table class='TableStyle' cellspacing='0' cellpadding='5' border='1'>
              <xsl:if test="Processes/Name">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Name</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Processes/CommandLine">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>CommandLine</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Processes/ExecutablePath">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>ExecutablePath</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Processes/CreationDate">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>CreationDate</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Processes/ProcessID">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>ProcessID</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Processes/ParentProcessID">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>ParentProcessID</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Processes/ThreadCount">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>ThreadCount</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Processes/VirtualSize">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>VirtualSize</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Processes/PrivateBytes">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>PrivateBytes</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Processes/Priority">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Priority</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:for-each select="Processes">
                <tbody>
                  <xsl:apply-templates select="."/>
                </tbody>
              </xsl:for-each>
            </table>
            <br/>
            <br/>

          </xsl:if>


          <xsl:if test="Processor">
            <span class='DetailHeader'>
              <xsl:text>Processor</xsl:text>
            </span>
            <br/>
            <table class='TableStyle' cellspacing='0' cellpadding='5' border='1'>
              <xsl:if test="Processor/DeviceID">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>DeviceID</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Processor/Name">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Name</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Processor/Manufacturer">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Manufacturer</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Processor/ClockSpeed">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>ClockSpeed</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Processor/Architecture">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Architecture</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Processor/L2CacheSize">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>L2CacheSize</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Processor/L2CacheSpeed">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>L2CacheSpeed</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Processor/SocketType">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>SocketType</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Processor/Version">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Version</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:for-each select="Processor">
                <tbody>
                  <xsl:apply-templates select="."/>
                </tbody>
              </xsl:for-each>
            </table>
            <br/>
            <br/>

          </xsl:if>


          <xsl:if test="Scan">
            <span class='DetailHeader'>
              <xsl:text>Scan</xsl:text>
            </span>
            <br/>
            <table class='TableStyle' cellspacing='0' cellpadding='5' border='1'>
              <xsl:if test="Scan/MachineID">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>MachineID</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Scan/ScanDate">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>ScanDate</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:for-each select="Scan">
                <tbody>
                  <xsl:apply-templates select="."/>
                </tbody>
              </xsl:for-each>
            </table>
            <br/>
            <br/>

          </xsl:if>


          <xsl:if test="Services">
            <span class='DetailHeader'>
              <xsl:text>Services</xsl:text>
            </span>
            <br/>
            <table class='TableStyle' cellspacing='0' cellpadding='5' border='1'>
              <xsl:if test="Services/DisplayName">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>DisplayName</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Services/Description">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Description</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Services/Path">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Path</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Services/Account">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Account</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Services/State">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>State</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Services/StartMode">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>StartMode</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:for-each select="Services">
                <tbody>
                  <xsl:apply-templates select="."/>
                </tbody>
              </xsl:for-each>
            </table>
            <br/>
            <br/>

          </xsl:if>


          <xsl:if test="Share">
            <span class='DetailHeader'>
              <xsl:text>Share</xsl:text>
            </span>
            <br/>
            <table class='TableStyle' cellspacing='0' cellpadding='5' border='1'>
              <xsl:if test="Share/Name">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Name</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Share/Caption">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Caption</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Share/Path">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Path</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Share/Type">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Type</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:for-each select="Share">
                <tbody>
                  <xsl:apply-templates select="."/>
                </tbody>
              </xsl:for-each>
            </table>
            <br/>
            <br/>

          </xsl:if>


          <xsl:if test="Softwares">
            <span class='DetailHeader'>
              <xsl:text>Softwares</xsl:text>
            </span>
            <br/>
            <table class='TableStyle' cellspacing='0' cellpadding='5' border='1'>
              <xsl:if test="Softwares/Name">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Name</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Softwares/Version">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Version</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Softwares/InstallDate">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>InstallDate</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Softwares/Location">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Location</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:for-each select="Softwares">
                <tbody>
                  <xsl:apply-templates select="."/>
                </tbody>
              </xsl:for-each>
            </table>
            <br/>
            <br/>

          </xsl:if>


          <xsl:if test="StartUp">
            <span class='DetailHeader'>
              <xsl:text>StartUp</xsl:text>
            </span>
            <br/>
            <table class='TableStyle' cellspacing='0' cellpadding='5' border='1'>
              <xsl:if test="StartUp/Name">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Name</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="StartUp/Command">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Command</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="StartUp/Location">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Location</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="StartUp/StartUpUser">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>StartUpUser</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:for-each select="StartUp">
                <tbody>
                  <xsl:apply-templates select="."/>
                </tbody>
              </xsl:for-each>
            </table>
            <br/>
            <br/>

          </xsl:if>


          <xsl:if test="UserGroups">
            <span class='DetailHeader'>
              <xsl:text>UserGroups</xsl:text>
            </span>
            <br/>
            <table class='TableStyle' cellspacing='0' cellpadding='5' border='1'>
              <xsl:if test="UserGroups/Users">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Users</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="UserGroups/Caption">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Caption</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="UserGroups/Description">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Description</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="UserGroups/Domain">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Domain</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="UserGroups/InstallDate">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>InstallDate</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="UserGroups/LocalAccount">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>LocalAccount</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="UserGroups/Name">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Name</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="UserGroups/SID">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>SID</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="UserGroups/Status">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Status</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:for-each select="UserGroups">
                <tbody>
                  <xsl:apply-templates select="."/>
                </tbody>
              </xsl:for-each>
            </table>
            <br/>
            <br/>

          </xsl:if>


          <xsl:if test="Video">
            <span class='DetailHeader'>
              <xsl:text>Video</xsl:text>
            </span>
            <br/>
            <table class='TableStyle' cellspacing='0' cellpadding='5' border='1'>
              <xsl:if test="Video/Name">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>Name</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Video/HorizontalResolution">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>HorizontalResolution</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Video/VerticalResolution">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>VerticalResolution</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Video/CurrentBitsperPixel">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>CurrentBitsperPixel</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Video/CurrentRefreshRate">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>CurrentRefreshRate</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:if test="Video/MemorySize">
                <td class='ColumnHeaderStyle' >
                  <span>
                    <xsl:text>MemorySize</xsl:text>
                  </span>
                </td>
              </xsl:if>
              <xsl:for-each select="Video">
                <tbody>
                  <xsl:apply-templates select="."/>
                </tbody>
              </xsl:for-each>
            </table>
            <br/>
            <br/>

          </xsl:if>


        </xsl:for-each>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="Bios">
    <tr>

      <xsl:if test="ReleaseDate">
        <td class='DetailData'>
          <xsl:value-of select="ReleaseDate"/>
        </td>
      </xsl:if>

      <xsl:if test="Manufacturer">
        <td class='DetailData'>
          <xsl:value-of select="Manufacturer"/>
        </td>
      </xsl:if>

      <xsl:if test="Version">
        <td class='DetailData'>
          <xsl:value-of select="Version"/>
        </td>
      </xsl:if>
    </tr>
  </xsl:template>
  <xsl:template match="CDRom">
    <tr>

      <xsl:if test="Name">
        <td class='DetailData'>
          <xsl:value-of select="Name"/>
        </td>
      </xsl:if>

      <xsl:if test="MediaType">
        <td class='DetailData'>
          <xsl:value-of select="MediaType"/>
        </td>
      </xsl:if>

      <xsl:if test="Manufacturer">
        <td class='DetailData'>
          <xsl:value-of select="Manufacturer"/>
        </td>
      </xsl:if>

      <xsl:if test="Drive">
        <td class='DetailData'>
          <xsl:value-of select="Drive"/>
        </td>
      </xsl:if>
    </tr>
  </xsl:template>
  <xsl:template match="Computer">
    <tr>

      <xsl:if test="BootupState">

        <td class='ColumnHeaderStyle'>
          <xsl:text>BootupState</xsl:text>
        </td>

        <td class='DetailData'>
          <xsl:value-of select="BootupState"/>
        </td>
      </xsl:if>
    </tr>

    <tr>

      <xsl:if test="Caption">

        <td class='ColumnHeaderStyle'>
          <xsl:text>Caption</xsl:text>
        </td>

        <td class='DetailData'>
          <xsl:value-of select="Caption"/>
        </td>
      </xsl:if>
    </tr>

    <tr>

      <xsl:if test="CurrentTimeZone">

        <td class='ColumnHeaderStyle'>
          <xsl:text>CurrentTimeZone</xsl:text>
        </td>

        <td class='DetailData'>
          <xsl:value-of select="CurrentTimeZone"/>
        </td>
      </xsl:if>
    </tr>

    <tr>

      <xsl:if test="DaylightInEffect">

        <td class='ColumnHeaderStyle'>
          <xsl:text>DaylightInEffect</xsl:text>
        </td>

        <td class='DetailData'>
          <xsl:value-of select="DaylightInEffect"/>
        </td>
      </xsl:if>
    </tr>

    <tr>

      <xsl:if test="Description">

        <td class='ColumnHeaderStyle'>
          <xsl:text>Description</xsl:text>
        </td>

        <td class='DetailData'>
          <xsl:value-of select="Description"/>
        </td>
      </xsl:if>
    </tr>

    <tr>

      <xsl:if test="DNSHostName">

        <td class='ColumnHeaderStyle'>
          <xsl:text>DNSHostName</xsl:text>
        </td>

        <td class='DetailData'>
          <xsl:value-of select="DNSHostName"/>
        </td>
      </xsl:if>
    </tr>

    <tr>

      <xsl:if test="Domain">

        <td class='ColumnHeaderStyle'>
          <xsl:text>Domain</xsl:text>
        </td>

        <td class='DetailData'>
          <xsl:value-of select="Domain"/>
        </td>
      </xsl:if>
    </tr>

    <tr>

      <xsl:if test="DomainRole">

        <td class='ColumnHeaderStyle'>
          <xsl:text>DomainRole</xsl:text>
        </td>

        <td class='DetailData'>
          <xsl:value-of select="DomainRole"/>
        </td>
      </xsl:if>
    </tr>

    <tr>

      <xsl:if test="EnableDaylightSavingsTime">

        <td class='ColumnHeaderStyle'>
          <xsl:text>EnableDaylightSavingsTime</xsl:text>
        </td>

        <td class='DetailData'>
          <xsl:value-of select="EnableDaylightSavingsTime"/>
        </td>
      </xsl:if>
    </tr>

    <tr>

      <xsl:if test="Manufacturer">

        <td class='ColumnHeaderStyle'>
          <xsl:text>Manufacturer</xsl:text>
        </td>

        <td class='DetailData'>
          <xsl:value-of select="Manufacturer"/>
        </td>
      </xsl:if>
    </tr>

    <tr>

      <xsl:if test="Model">

        <td class='ColumnHeaderStyle'>
          <xsl:text>Model</xsl:text>
        </td>

        <td class='DetailData'>
          <xsl:value-of select="Model"/>
        </td>
      </xsl:if>
    </tr>

    <tr>

      <xsl:if test="Name">

        <td class='ColumnHeaderStyle'>
          <xsl:text>Name</xsl:text>
        </td>

        <td class='DetailData'>
          <xsl:value-of select="Name"/>
        </td>
      </xsl:if>
    </tr>

    <tr>

      <xsl:if test="NetworkServerModeEnabled">

        <td class='ColumnHeaderStyle'>
          <xsl:text>NetworkServerModeEnabled</xsl:text>
        </td>

        <td class='DetailData'>
          <xsl:value-of select="NetworkServerModeEnabled"/>
        </td>
      </xsl:if>
    </tr>

    <tr>

      <xsl:if test="PartOfDomain">

        <td class='ColumnHeaderStyle'>
          <xsl:text>PartOfDomain</xsl:text>
        </td>

        <td class='DetailData'>
          <xsl:value-of select="PartOfDomain"/>
        </td>
      </xsl:if>
    </tr>

    <tr>

      <xsl:if test="PCSystemType">

        <td class='ColumnHeaderStyle'>
          <xsl:text>PCSystemType</xsl:text>
        </td>

        <td class='DetailData'>
          <xsl:value-of select="PCSystemType"/>
        </td>
      </xsl:if>
    </tr>

    <tr>

      <xsl:if test="PowerState">

        <td class='ColumnHeaderStyle'>
          <xsl:text>PowerState</xsl:text>
        </td>

        <td class='DetailData'>
          <xsl:value-of select="PowerState"/>
        </td>
      </xsl:if>
    </tr>

    <tr>

      <xsl:if test="PowerSupplyState">

        <td class='ColumnHeaderStyle'>
          <xsl:text>PowerSupplyState</xsl:text>
        </td>

        <td class='DetailData'>
          <xsl:value-of select="PowerSupplyState"/>
        </td>
      </xsl:if>
    </tr>

    <tr>

      <xsl:if test="PrimaryOwnerContact">

        <td class='ColumnHeaderStyle'>
          <xsl:text>PrimaryOwnerContact</xsl:text>
        </td>

        <td class='DetailData'>
          <xsl:value-of select="PrimaryOwnerContact"/>
        </td>
      </xsl:if>
    </tr>

    <tr>

      <xsl:if test="PrimaryOwnerName">

        <td class='ColumnHeaderStyle'>
          <xsl:text>PrimaryOwnerName</xsl:text>
        </td>

        <td class='DetailData'>
          <xsl:value-of select="PrimaryOwnerName"/>
        </td>
      </xsl:if>
    </tr>

    <tr>

      <xsl:if test="Roles">

        <td class='ColumnHeaderStyle'>
          <xsl:text>Roles</xsl:text>
        </td>

        <td class='DetailData'>
          <xsl:value-of select="Roles"/>
        </td>
      </xsl:if>
    </tr>

    <tr>

      <xsl:if test="Status">

        <td class='ColumnHeaderStyle'>
          <xsl:text>Status</xsl:text>
        </td>

        <td class='DetailData'>
          <xsl:value-of select="Status"/>
        </td>
      </xsl:if>
    </tr>

    <tr>

      <xsl:if test="SupportContactDescription">

        <td class='ColumnHeaderStyle'>
          <xsl:text>SupportContactDescription</xsl:text>
        </td>

        <td class='DetailData'>
          <xsl:value-of select="SupportContactDescription"/>
        </td>
      </xsl:if>
    </tr>

    <tr>

      <xsl:if test="SystemStartupDelay">

        <td class='ColumnHeaderStyle'>
          <xsl:text>SystemStartupDelay</xsl:text>
        </td>

        <td class='DetailData'>
          <xsl:value-of select="SystemStartupDelay"/>
        </td>
      </xsl:if>
    </tr>

    <tr>

      <xsl:if test="SystemStartupOptions">

        <td class='ColumnHeaderStyle'>
          <xsl:text>SystemStartupOptions</xsl:text>
        </td>

        <td class='DetailData'>
          <xsl:value-of select="SystemStartupOptions"/>
        </td>
      </xsl:if>
    </tr>

    <tr>

      <xsl:if test="SystemType">

        <td class='ColumnHeaderStyle'>
          <xsl:text>SystemType</xsl:text>
        </td>

        <td class='DetailData'>
          <xsl:value-of select="SystemType"/>
        </td>
      </xsl:if>
    </tr>

    <tr>

      <xsl:if test="ThermalState">

        <td class='ColumnHeaderStyle'>
          <xsl:text>ThermalState</xsl:text>
        </td>

        <td class='DetailData'>
          <xsl:value-of select="ThermalState"/>
        </td>
      </xsl:if>
    </tr>

    <tr>

      <xsl:if test="UserName">

        <td class='ColumnHeaderStyle'>
          <xsl:text>UserName</xsl:text>
        </td>

        <td class='DetailData'>
          <xsl:value-of select="UserName"/>
        </td>
      </xsl:if>
    </tr>

    <tr>

      <xsl:if test="WakeUpType">

        <td class='ColumnHeaderStyle'>
          <xsl:text>WakeUpType</xsl:text>
        </td>

        <td class='DetailData'>
          <xsl:value-of select="WakeUpType"/>
        </td>
      </xsl:if>
    </tr>

    <tr>

      <xsl:if test="Workgroup">

        <td class='ColumnHeaderStyle'>
          <xsl:text>Workgroup</xsl:text>
        </td>

        <td class='DetailData'>
          <xsl:value-of select="Workgroup"/>
        </td>
      </xsl:if>
    </tr>

  </xsl:template>
  <xsl:template match="Credential">
    <tr>

      <xsl:if test="Username">
        <td class='DetailData'>
          <xsl:value-of select="Username"/>
        </td>
      </xsl:if>

      <xsl:if test="Password">
        <td class='DetailData'>
          <xsl:value-of select="Password"/>
        </td>
      </xsl:if>

      <xsl:if test="CredentialID">
        <td class='DetailData'>
          <xsl:value-of select="CredentialID"/>
        </td>
      </xsl:if>

      <xsl:if test="CredentialName">
        <td class='DetailData'>
          <xsl:value-of select="CredentialName"/>
        </td>
      </xsl:if>
    </tr>
  </xsl:template>
  <xsl:template match="Discover">
    <tr>

      <xsl:if test="IPAddress">
        <td class='DetailData'>
          <xsl:value-of select="IPAddress"/>
        </td>
      </xsl:if>

      <xsl:if test="LastChecked">
        <td class='DetailData'>
          <xsl:value-of select="LastChecked"/>
        </td>
      </xsl:if>

      <xsl:if test="Discovered">
        <td class='DetailData'>
          <xsl:value-of select="Discovered"/>
        </td>
      </xsl:if>

      <xsl:if test="StatusMessage">
        <td class='DetailData'>
          <xsl:value-of select="StatusMessage"/>
        </td>
      </xsl:if>

      <xsl:if test="MachineID">
        <td class='DetailData'>
          <xsl:value-of select="MachineID"/>
        </td>
      </xsl:if>

      <xsl:if test="DomainName">
        <td class='DetailData'>
          <xsl:value-of select="DomainName"/>
        </td>
      </xsl:if>

      <xsl:if test="MachineName">
        <td class='DetailData'>
          <xsl:value-of select="MachineName"/>
        </td>
      </xsl:if>

      <xsl:if test="CredentialID">
        <td class='DetailData'>
          <xsl:value-of select="CredentialID"/>
        </td>
      </xsl:if>
    </tr>
  </xsl:template>
  <xsl:template match="Disk">
    <tr>

      <xsl:if test="Caption">
        <td class='DetailData'>
          <xsl:value-of select="Caption"/>
        </td>
      </xsl:if>

      <xsl:if test="Size">
        <td class='DetailData'>
          <xsl:value-of select="Size"/>
        </td>
      </xsl:if>

      <xsl:if test="Manufacturer">
        <td class='DetailData'>
          <xsl:value-of select="Manufacturer"/>
        </td>
      </xsl:if>

      <xsl:if test="Interface">
        <td class='DetailData'>
          <xsl:value-of select="Interface"/>
        </td>
      </xsl:if>

      <xsl:if test="MediaType">
        <td class='DetailData'>
          <xsl:value-of select="MediaType"/>
        </td>
      </xsl:if>

      <xsl:if test="BytesPerSector">
        <td class='DetailData'>
          <xsl:value-of select="BytesPerSector"/>
        </td>
      </xsl:if>

      <xsl:if test="Heads">
        <td class='DetailData'>
          <xsl:value-of select="Heads"/>
        </td>
      </xsl:if>

      <xsl:if test="Cylinders">
        <td class='DetailData'>
          <xsl:value-of select="Cylinders"/>
        </td>
      </xsl:if>

      <xsl:if test="Sectors">
        <td class='DetailData'>
          <xsl:value-of select="Sectors"/>
        </td>
      </xsl:if>

      <xsl:if test="Tracks">
        <td class='DetailData'>
          <xsl:value-of select="Tracks"/>
        </td>
      </xsl:if>

      <xsl:if test="Model">
        <td class='DetailData'>
          <xsl:value-of select="Model"/>
        </td>
      </xsl:if>
    </tr>
  </xsl:template>
  <xsl:template match="EnvironmentVars">
    <tr>

      <xsl:if test="Name">
        <td class='DetailData'>
          <xsl:value-of select="Name"/>
        </td>
      </xsl:if>

      <xsl:if test="Status">
        <td class='DetailData'>
          <xsl:value-of select="Status"/>
        </td>
      </xsl:if>

      <xsl:if test="SystemVariable">
        <td class='DetailData'>
          <xsl:value-of select="SystemVariable"/>
        </td>
      </xsl:if>

      <xsl:if test="UserName">
        <td class='DetailData'>
          <xsl:value-of select="UserName"/>
        </td>
      </xsl:if>

      <xsl:if test="VariableValue">
        <td class='DetailData'>
          <xsl:value-of select="VariableValue"/>
        </td>
      </xsl:if>
    </tr>
  </xsl:template>
  <xsl:template match="Hotfixes">
    <tr>

      <xsl:if test="HotfixID">
        <td class='DetailData'>
          <xsl:value-of select="HotfixID"/>
        </td>
      </xsl:if>

      <xsl:if test="Description">
        <td class='DetailData'>
          <xsl:value-of select="Description"/>
        </td>
      </xsl:if>

      <xsl:if test="InstallBy">
        <td class='DetailData'>
          <xsl:value-of select="InstallBy"/>
        </td>
      </xsl:if>

      <xsl:if test="FixComments">
        <td class='DetailData'>
          <xsl:value-of select="FixComments"/>
        </td>
      </xsl:if>

      <xsl:if test="SPEffect">
        <td class='DetailData'>
          <xsl:value-of select="SPEffect"/>
        </td>
      </xsl:if>
    </tr>
  </xsl:template>
  <xsl:template match="IPRoutes">
    <tr>

      <xsl:if test="Age">
        <td class='DetailData'>
          <xsl:value-of select="Age"/>
        </td>
      </xsl:if>

      <xsl:if test="Name">
        <td class='DetailData'>
          <xsl:value-of select="Name"/>
        </td>
      </xsl:if>

      <xsl:if test="Description">
        <td class='DetailData'>
          <xsl:value-of select="Description"/>
        </td>
      </xsl:if>

      <xsl:if test="Destination">
        <td class='DetailData'>
          <xsl:value-of select="Destination"/>
        </td>
      </xsl:if>

      <xsl:if test="Mask">
        <td class='DetailData'>
          <xsl:value-of select="Mask"/>
        </td>
      </xsl:if>

      <xsl:if test="Metric1">
        <td class='DetailData'>
          <xsl:value-of select="Metric1"/>
        </td>
      </xsl:if>

      <xsl:if test="Metric2">
        <td class='DetailData'>
          <xsl:value-of select="Metric2"/>
        </td>
      </xsl:if>

      <xsl:if test="Metric3">
        <td class='DetailData'>
          <xsl:value-of select="Metric3"/>
        </td>
      </xsl:if>

      <xsl:if test="Metric4">
        <td class='DetailData'>
          <xsl:value-of select="Metric4"/>
        </td>
      </xsl:if>

      <xsl:if test="Metric5">
        <td class='DetailData'>
          <xsl:value-of select="Metric5"/>
        </td>
      </xsl:if>

      <xsl:if test="NextHop">
        <td class='DetailData'>
          <xsl:value-of select="NextHop"/>
        </td>
      </xsl:if>

      <xsl:if test="Protocol">
        <td class='DetailData'>
          <xsl:value-of select="Protocol"/>
        </td>
      </xsl:if>

      <xsl:if test="Status">
        <td class='DetailData'>
          <xsl:value-of select="Status"/>
        </td>
      </xsl:if>

      <xsl:if test="RouteType">
        <td class='DetailData'>
          <xsl:value-of select="RouteType"/>
        </td>
      </xsl:if>
    </tr>
  </xsl:template>
  <xsl:template match="LogicalDrive">
    <tr>

      <xsl:if test="Name">
        <td class='DetailData'>
          <xsl:value-of select="Name"/>
        </td>
      </xsl:if>

      <xsl:if test="Description">
        <td class='DetailData'>
          <xsl:value-of select="Description"/>
        </td>
      </xsl:if>

      <xsl:if test="Size">
        <td class='DetailData'>
          <xsl:value-of select="Size"/>
        </td>
      </xsl:if>

      <xsl:if test="FreeSpace">
        <td class='DetailData'>
          <xsl:value-of select="FreeSpace"/>
        </td>
      </xsl:if>

      <xsl:if test="FileSystem">
        <td class='DetailData'>
          <xsl:value-of select="FileSystem"/>
        </td>
      </xsl:if>

      <xsl:if test="SerialNumber">
        <td class='DetailData'>
          <xsl:value-of select="SerialNumber"/>
        </td>
      </xsl:if>

      <xsl:if test="VolumeLabel">
        <td class='DetailData'>
          <xsl:value-of select="VolumeLabel"/>
        </td>
      </xsl:if>
    </tr>
  </xsl:template>
  <xsl:template match="Memory">
    <tr>

      <xsl:if test="Capacity">
        <td class='DetailData'>
          <xsl:value-of select="Capacity"/>
        </td>
      </xsl:if>

      <xsl:if test="DeviceLocator">
        <td class='DetailData'>
          <xsl:value-of select="DeviceLocator"/>
        </td>
      </xsl:if>

      <xsl:if test="BankLabel">
        <td class='DetailData'>
          <xsl:value-of select="BankLabel"/>
        </td>
      </xsl:if>

      <xsl:if test="FormFactor">
        <td class='DetailData'>
          <xsl:value-of select="FormFactor"/>
        </td>
      </xsl:if>

      <xsl:if test="MemoryType">
        <td class='DetailData'>
          <xsl:value-of select="MemoryType"/>
        </td>
      </xsl:if>

      <xsl:if test="Manufacturer">
        <td class='DetailData'>
          <xsl:value-of select="Manufacturer"/>
        </td>
      </xsl:if>

      <xsl:if test="Speed">
        <td class='DetailData'>
          <xsl:value-of select="Speed"/>
        </td>
      </xsl:if>
    </tr>
  </xsl:template>
  <xsl:template match="Monitor">
    <tr>

      <xsl:if test="Name">
        <td class='DetailData'>
          <xsl:value-of select="Name"/>
        </td>
      </xsl:if>

      <xsl:if test="ScreenWidth">
        <td class='DetailData'>
          <xsl:value-of select="ScreenWidth"/>
        </td>
      </xsl:if>

      <xsl:if test="ScreenHeight">
        <td class='DetailData'>
          <xsl:value-of select="ScreenHeight"/>
        </td>
      </xsl:if>

      <xsl:if test="Availability">
        <td class='DetailData'>
          <xsl:value-of select="Availability"/>
        </td>
      </xsl:if>
    </tr>
  </xsl:template>
  <xsl:template match="MotherBoard">
    <tr>

      <xsl:if test="Manufacturer">
        <td class='DetailData'>
          <xsl:value-of select="Manufacturer"/>
        </td>
      </xsl:if>

      <xsl:if test="Version">
        <td class='DetailData'>
          <xsl:value-of select="Version"/>
        </td>
      </xsl:if>

      <xsl:if test="Product">
        <td class='DetailData'>
          <xsl:value-of select="Product"/>
        </td>
      </xsl:if>

      <xsl:if test="SerialNo">
        <td class='DetailData'>
          <xsl:value-of select="SerialNo"/>
        </td>
      </xsl:if>

      <xsl:if test="Model">
        <td class='DetailData'>
          <xsl:value-of select="Model"/>
        </td>
      </xsl:if>
    </tr>
  </xsl:template>
  <xsl:template match="Multimedia">
    <tr>

      <xsl:if test="Name">
        <td class='DetailData'>
          <xsl:value-of select="Name"/>
        </td>
      </xsl:if>

      <xsl:if test="Manufacturer">
        <td class='DetailData'>
          <xsl:value-of select="Manufacturer"/>
        </td>
      </xsl:if>
    </tr>
  </xsl:template>
  <xsl:template match="NetworkAdapter">
    <tr>

      <xsl:if test="AdapterType">
        <td class='DetailData'>
          <xsl:value-of select="AdapterType"/>
        </td>
      </xsl:if>

      <xsl:if test="NetConnectonState">
        <td class='DetailData'>
          <xsl:value-of select="NetConnectonState"/>
        </td>
      </xsl:if>

      <xsl:if test="Manufacturer">
        <td class='DetailData'>
          <xsl:value-of select="Manufacturer"/>
        </td>
      </xsl:if>

      <xsl:if test="Speed">
        <td class='DetailData'>
          <xsl:value-of select="Speed"/>
        </td>
      </xsl:if>

      <xsl:if test="AdapterIPAddress">
        <td class='DetailData'>
          <xsl:value-of select="AdapterIPAddress"/>
        </td>
      </xsl:if>

      <xsl:if test="AdapterMACAddress">
        <td class='DetailData'>
          <xsl:value-of select="AdapterMACAddress"/>
        </td>
      </xsl:if>

      <xsl:if test="DHCPEnabled">
        <td class='DetailData'>
          <xsl:value-of select="DHCPEnabled"/>
        </td>
      </xsl:if>

      <xsl:if test="DHCPServer">
        <td class='DetailData'>
          <xsl:value-of select="DHCPServer"/>
        </td>
      </xsl:if>

      <xsl:if test="DNSDomain">
        <td class='DetailData'>
          <xsl:value-of select="DNSDomain"/>
        </td>
      </xsl:if>

      <xsl:if test="WINSPrimaryServer">
        <td class='DetailData'>
          <xsl:value-of select="WINSPrimaryServer"/>
        </td>
      </xsl:if>

      <xsl:if test="WINSSecondaryServer">
        <td class='DetailData'>
          <xsl:value-of select="WINSSecondaryServer"/>
        </td>
      </xsl:if>
    </tr>
  </xsl:template>
  <xsl:template match="OS">
    <tr>

      <xsl:if test="Name">

        <td class='ColumnHeaderStyle'>
          <xsl:text>Name</xsl:text>
        </td>

        <td class='DetailData'>
          <xsl:value-of select="Name"/>
        </td>
      </xsl:if>
    </tr>

    <tr>

      <xsl:if test="Version">

        <td class='ColumnHeaderStyle'>
          <xsl:text>Version</xsl:text>
        </td>

        <td class='DetailData'>
          <xsl:value-of select="Version"/>
        </td>
      </xsl:if>
    </tr>

    <tr>

      <xsl:if test="BuildNo">

        <td class='ColumnHeaderStyle'>
          <xsl:text>BuildNo</xsl:text>
        </td>

        <td class='DetailData'>
          <xsl:value-of select="BuildNo"/>
        </td>
      </xsl:if>
    </tr>

    <tr>

      <xsl:if test="InstallDate">

        <td class='ColumnHeaderStyle'>
          <xsl:text>InstallDate</xsl:text>
        </td>

        <td class='DetailData'>
          <xsl:value-of select="InstallDate"/>
        </td>
      </xsl:if>
    </tr>

    <tr>

      <xsl:if test="ServicePack">

        <td class='ColumnHeaderStyle'>
          <xsl:text>ServicePack</xsl:text>
        </td>

        <td class='DetailData'>
          <xsl:value-of select="ServicePack"/>
        </td>
      </xsl:if>
    </tr>

    <tr>

      <xsl:if test="WindowsID">

        <td class='ColumnHeaderStyle'>
          <xsl:text>WindowsID</xsl:text>
        </td>

        <td class='DetailData'>
          <xsl:value-of select="WindowsID"/>
        </td>
      </xsl:if>
    </tr>

    <tr>

      <xsl:if test="LastBootupTime">

        <td class='ColumnHeaderStyle'>
          <xsl:text>LastBootupTime</xsl:text>
        </td>

        <td class='DetailData'>
          <xsl:value-of select="LastBootupTime"/>
        </td>
      </xsl:if>
    </tr>

    <tr>

      <xsl:if test="SerialKey">

        <td class='ColumnHeaderStyle'>
          <xsl:text>SerialKey</xsl:text>
        </td>

        <td class='DetailData'>
          <xsl:value-of select="SerialKey"/>
        </td>
      </xsl:if>
    </tr>

  </xsl:template>
  <xsl:template match="Printer">
    <tr>

      <xsl:if test="Attributes">
        <td class='DetailData'>
          <xsl:value-of select="Attributes"/>
        </td>
      </xsl:if>

      <xsl:if test="Availability">
        <td class='DetailData'>
          <xsl:value-of select="Availability"/>
        </td>
      </xsl:if>

      <xsl:if test="AveragePagesPerMinute">
        <td class='DetailData'>
          <xsl:value-of select="AveragePagesPerMinute"/>
        </td>
      </xsl:if>

      <xsl:if test="Caption">
        <td class='DetailData'>
          <xsl:value-of select="Caption"/>
        </td>
      </xsl:if>

      <xsl:if test="HorizontalResolution">
        <td class='DetailData'>
          <xsl:value-of select="HorizontalResolution"/>
        </td>
      </xsl:if>

      <xsl:if test="VerticalResolution">
        <td class='DetailData'>
          <xsl:value-of select="VerticalResolution"/>
        </td>
      </xsl:if>

      <xsl:if test="Local">
        <td class='DetailData'>
          <xsl:value-of select="Local"/>
        </td>
      </xsl:if>

      <xsl:if test="Network">
        <td class='DetailData'>
          <xsl:value-of select="Network"/>
        </td>
      </xsl:if>

      <xsl:if test="Shared">
        <td class='DetailData'>
          <xsl:value-of select="Shared"/>
        </td>
      </xsl:if>

      <xsl:if test="Location">
        <td class='DetailData'>
          <xsl:value-of select="Location"/>
        </td>
      </xsl:if>

      <xsl:if test="PortName">
        <td class='DetailData'>
          <xsl:value-of select="PortName"/>
        </td>
      </xsl:if>

      <xsl:if test="PrinterState">
        <td class='DetailData'>
          <xsl:value-of select="PrinterState"/>
        </td>
      </xsl:if>

      <xsl:if test="PrinterStatus">
        <td class='DetailData'>
          <xsl:value-of select="PrinterStatus"/>
        </td>
      </xsl:if>

      <xsl:if test="ServerName">
        <td class='DetailData'>
          <xsl:value-of select="ServerName"/>
        </td>
      </xsl:if>

      <xsl:if test="ShareName">
        <td class='DetailData'>
          <xsl:value-of select="ShareName"/>
        </td>
      </xsl:if>
    </tr>
  </xsl:template>
  <xsl:template match="Processes">
    <tr>

      <xsl:if test="Name">
        <td class='DetailData'>
          <xsl:value-of select="Name"/>
        </td>
      </xsl:if>

      <xsl:if test="CommandLine">
        <td class='DetailData'>
          <xsl:value-of select="CommandLine"/>
        </td>
      </xsl:if>

      <xsl:if test="ExecutablePath">
        <td class='DetailData'>
          <xsl:value-of select="ExecutablePath"/>
        </td>
      </xsl:if>

      <xsl:if test="CreationDate">
        <td class='DetailData'>
          <xsl:value-of select="CreationDate"/>
        </td>
      </xsl:if>

      <xsl:if test="ProcessID">
        <td class='DetailData'>
          <xsl:value-of select="ProcessID"/>
        </td>
      </xsl:if>

      <xsl:if test="ParentProcessID">
        <td class='DetailData'>
          <xsl:value-of select="ParentProcessID"/>
        </td>
      </xsl:if>

      <xsl:if test="ThreadCount">
        <td class='DetailData'>
          <xsl:value-of select="ThreadCount"/>
        </td>
      </xsl:if>

      <xsl:if test="VirtualSize">
        <td class='DetailData'>
          <xsl:value-of select="VirtualSize"/>
        </td>
      </xsl:if>

      <xsl:if test="PrivateBytes">
        <td class='DetailData'>
          <xsl:value-of select="PrivateBytes"/>
        </td>
      </xsl:if>

      <xsl:if test="Priority">
        <td class='DetailData'>
          <xsl:value-of select="Priority"/>
        </td>
      </xsl:if>
    </tr>
  </xsl:template>
  <xsl:template match="Processor">
    <tr>

      <xsl:if test="DeviceID">
        <td class='DetailData'>
          <xsl:value-of select="DeviceID"/>
        </td>
      </xsl:if>

      <xsl:if test="Name">
        <td class='DetailData'>
          <xsl:value-of select="Name"/>
        </td>
      </xsl:if>

      <xsl:if test="Manufacturer">
        <td class='DetailData'>
          <xsl:value-of select="Manufacturer"/>
        </td>
      </xsl:if>

      <xsl:if test="ClockSpeed">
        <td class='DetailData'>
          <xsl:value-of select="ClockSpeed"/>
        </td>
      </xsl:if>

      <xsl:if test="Architecture">
        <td class='DetailData'>
          <xsl:value-of select="Architecture"/>
        </td>
      </xsl:if>

      <xsl:if test="L2CacheSize">
        <td class='DetailData'>
          <xsl:value-of select="L2CacheSize"/>
        </td>
      </xsl:if>

      <xsl:if test="L2CacheSpeed">
        <td class='DetailData'>
          <xsl:value-of select="L2CacheSpeed"/>
        </td>
      </xsl:if>

      <xsl:if test="SocketType">
        <td class='DetailData'>
          <xsl:value-of select="SocketType"/>
        </td>
      </xsl:if>

      <xsl:if test="Version">
        <td class='DetailData'>
          <xsl:value-of select="Version"/>
        </td>
      </xsl:if>
    </tr>
  </xsl:template>
  <xsl:template match="Scan">
    <tr>

      <xsl:if test="MachineID">
        <td class='DetailData'>
          <xsl:value-of select="MachineID"/>
        </td>
      </xsl:if>

      <xsl:if test="ScanDate">
        <td class='DetailData'>
          <xsl:value-of select="ScanDate"/>
        </td>
      </xsl:if>
    </tr>
  </xsl:template>
  <xsl:template match="Services">
    <tr>

      <xsl:if test="DisplayName">
        <td class='DetailData'>
          <xsl:value-of select="DisplayName"/>
        </td>
      </xsl:if>

      <xsl:if test="Description">
        <td class='DetailData'>
          <xsl:value-of select="Description"/>
        </td>
      </xsl:if>

      <xsl:if test="Path">
        <td class='DetailData'>
          <xsl:value-of select="Path"/>
        </td>
      </xsl:if>

      <xsl:if test="Account">
        <td class='DetailData'>
          <xsl:value-of select="Account"/>
        </td>
      </xsl:if>

      <xsl:if test="State">
        <td class='DetailData'>
          <xsl:value-of select="State"/>
        </td>
      </xsl:if>

      <xsl:if test="StartMode">
        <td class='DetailData'>
          <xsl:value-of select="StartMode"/>
        </td>
      </xsl:if>
    </tr>
  </xsl:template>
  <xsl:template match="Share">
    <tr>

      <xsl:if test="Name">
        <td class='DetailData'>
          <xsl:value-of select="Name"/>
        </td>
      </xsl:if>

      <xsl:if test="Caption">
        <td class='DetailData'>
          <xsl:value-of select="Caption"/>
        </td>
      </xsl:if>

      <xsl:if test="Path">
        <td class='DetailData'>
          <xsl:value-of select="Path"/>
        </td>
      </xsl:if>

      <xsl:if test="Type">
        <td class='DetailData'>
          <xsl:value-of select="Type"/>
        </td>
      </xsl:if>
    </tr>
  </xsl:template>
  <xsl:template match="Softwares">
    <tr>

      <xsl:if test="Name">
        <td class='DetailData'>
          <xsl:value-of select="Name"/>
        </td>
      </xsl:if>

      <xsl:if test="Version">
        <td class='DetailData'>
          <xsl:value-of select="Version"/>
        </td>
      </xsl:if>

      <xsl:if test="InstallDate">
        <td class='DetailData'>
          <xsl:value-of select="InstallDate"/>
        </td>
      </xsl:if>

      <xsl:if test="Location">
        <td class='DetailData'>
          <xsl:value-of select="Location"/>
        </td>
      </xsl:if>
    </tr>
  </xsl:template>
  <xsl:template match="StartUp">
    <tr>

      <xsl:if test="Name">
        <td class='DetailData'>
          <xsl:value-of select="Name"/>
        </td>
      </xsl:if>

      <xsl:if test="Command">
        <td class='DetailData'>
          <xsl:value-of select="Command"/>
        </td>
      </xsl:if>

      <xsl:if test="Location">
        <td class='DetailData'>
          <xsl:value-of select="Location"/>
        </td>
      </xsl:if>

      <xsl:if test="StartUpUser">
        <td class='DetailData'>
          <xsl:value-of select="StartUpUser"/>
        </td>
      </xsl:if>
    </tr>
  </xsl:template>
  <xsl:template match="UserGroups">
    <tr>

      <xsl:if test="Users">
        <td class='DetailData'>
          <xsl:value-of select="Users"/>
        </td>
      </xsl:if>

      <xsl:if test="Caption">
        <td class='DetailData'>
          <xsl:value-of select="Caption"/>
        </td>
      </xsl:if>

      <xsl:if test="Description">
        <td class='DetailData'>
          <xsl:value-of select="Description"/>
        </td>
      </xsl:if>

      <xsl:if test="Domain">
        <td class='DetailData'>
          <xsl:value-of select="Domain"/>
        </td>
      </xsl:if>

      <xsl:if test="InstallDate">
        <td class='DetailData'>
          <xsl:value-of select="InstallDate"/>
        </td>
      </xsl:if>

      <xsl:if test="LocalAccount">
        <td class='DetailData'>
          <xsl:value-of select="LocalAccount"/>
        </td>
      </xsl:if>

      <xsl:if test="Name">
        <td class='DetailData'>
          <xsl:value-of select="Name"/>
        </td>
      </xsl:if>

      <xsl:if test="SID">
        <td class='DetailData'>
          <xsl:value-of select="SID"/>
        </td>
      </xsl:if>

      <xsl:if test="Status">
        <td class='DetailData'>
          <xsl:value-of select="Status"/>
        </td>
      </xsl:if>
    </tr>
  </xsl:template>
  <xsl:template match="Video">
    <tr>

      <xsl:if test="Name">
        <td class='DetailData'>
          <xsl:value-of select="Name"/>
        </td>
      </xsl:if>

      <xsl:if test="HorizontalResolution">
        <td class='DetailData'>
          <xsl:value-of select="HorizontalResolution"/>
        </td>
      </xsl:if>

      <xsl:if test="VerticalResolution">
        <td class='DetailData'>
          <xsl:value-of select="VerticalResolution"/>
        </td>
      </xsl:if>

      <xsl:if test="CurrentBitsperPixel">
        <td class='DetailData'>
          <xsl:value-of select="CurrentBitsperPixel"/>
        </td>
      </xsl:if>

      <xsl:if test="CurrentRefreshRate">
        <td class='DetailData'>
          <xsl:value-of select="CurrentRefreshRate"/>
        </td>
      </xsl:if>

      <xsl:if test="MemorySize">
        <td class='DetailData'>
          <xsl:value-of select="MemorySize"/>
        </td>
      </xsl:if>
    </tr>
  </xsl:template>
</xsl:stylesheet>
