-- Remove the existing constraints
GO
ALTER TABLE [Bios] DROP CONSTRAINT  [FK_SCAN_ID];
GO
ALTER TABLE [CDRom] DROP CONSTRAINT [FK_SCAN_ID];
GO
ALTER TABLE [Disk] DROP CONSTRAINT  [FK_SCAN_ID];
GO
ALTER TABLE [Hotfixes] DROP CONSTRAINT  [FK_SCAN_ID];
GO
ALTER TABLE [LogicalDrive] DROP CONSTRAINT  [FK_SCAN_ID];
GO
ALTER TABLE [Memory] DROP CONSTRAINT  [FK_SCAN_ID];
GO
ALTER TABLE [Monitor] DROP CONSTRAINT  [FK_SCAN_ID];
GO
ALTER TABLE [Motherboard] DROP CONSTRAINT  [FK_SCAN_ID];
GO
ALTER TABLE [Multimedia] DROP CONSTRAINT  [FK_SCAN_ID];
GO
ALTER TABLE [NetworkAdapter] DROP CONSTRAINT  [FK_SCAN_ID];
GO
ALTER TABLE [OS] DROP CONSTRAINT  [FK_SCAN_ID];
GO
ALTER TABLE [Processes] DROP CONSTRAINT  [FK_SCAN_ID];
GO
ALTER TABLE [Processor] DROP CONSTRAINT  [FK_SCAN_ID];
GO
ALTER TABLE [Scan] DROP CONSTRAINT  [FK_MACHINE_ID];
GO
ALTER TABLE [Services] DROP CONSTRAINT  [FK_SCAN_ID];
GO
ALTER TABLE [Share] DROP CONSTRAINT  [FK_SCAN_ID];
GO
ALTER TABLE [Softwares] DROP CONSTRAINT  [FK_SCAN_ID];
GO
ALTER TABLE [StartUp] DROP CONSTRAINT  [FK_SCAN_ID];
GO
ALTER TABLE [Video] DROP CONSTRAINT  [FK_SCAN_ID];
GO
CREATE TABLE [Bios] (
  [ReleaseDate] datetime NULL
, [Manufacturer] nvarchar(100) NULL
, [Version] nvarchar(100) NULL
, [EntryID] int NOT NULL  IDENTITY (1,1)
, [ScanID] int NULL
);
GO
CREATE TABLE [CDRom] (
  [Name] nvarchar(255) NULL
, [MediaType] nvarchar(50) NULL
, [Manufacturer] nvarchar(255) NULL
, [Drive] nvarchar(5) NULL
, [ScanID] int NULL
, [EntryID] int NOT NULL  IDENTITY (1,1)
);
GO
CREATE TABLE [Credential] (
  [Username] nvarchar(255) NULL
, [Password] nvarchar(255) NULL
, [CredentialID] int NOT NULL  IDENTITY (1,1)
, [CredentialName] nvarchar(255) NULL
);
GO
CREATE TABLE [Discover] (
  [IPAddress] nvarchar(20) NULL
, [LastChecked] datetime NULL
, [Discovered] bit NULL
, [StatusMessage] nchar(512) NULL
, [MachineID] int NOT NULL  IDENTITY (1,1)
, [DomainName] nvarchar(200) NULL
, [MachineName] nvarchar(200) NULL
, [CredentialID] int NULL DEFAULT 0
);
GO
CREATE TABLE [Disk] (
  [ScanID] int NULL
, [EntryID] int NOT NULL  IDENTITY (1,1)
, [Caption] nvarchar(100) NULL
, [Size] nvarchar(20) NULL
, [Manufacturer] nvarchar(100) NULL
, [Interface] nvarchar(20) NULL
, [MediaType] nvarchar(100) NULL
, [BytesPerSector] int NULL
, [Heads] int NULL
, [Cylinders] nvarchar(12) NULL
, [Sectors] nvarchar(20) NULL
, [Tracks] nvarchar(20) NULL
, [Model] nvarchar(100) NULL
);
GO
CREATE TABLE [Hotfixes] (
  [ScanID] int NULL
, [EntryID] int NOT NULL  IDENTITY (1,1)
, [HotfixID] nvarchar(512) NULL
, [Description] nvarchar(2056) NULL
, [InstallBy] nvarchar(300) NULL
, [FixComments] nvarchar(512) NULL
, [SPEffect] nvarchar(300) NULL
);
GO
CREATE TABLE [LogicalDrive] (
  [Name] nvarchar(5) NULL
, [Description] nvarchar(300) NULL
, [Size] nvarchar(20) NULL
, [FreeSpace] nvarchar(20) NULL
, [FileSystem] nvarchar(20) NULL
, [SerialNumber] nvarchar(50) NULL
, [ScanID] int NULL
, [EntryID] int NOT NULL  IDENTITY (1,1)
, [VolumeLabel] nvarchar(255) NULL
);
GO
CREATE TABLE [Memory] (
  [Capicity] nvarchar(20) NULL
, [DeviceLocator] nvarchar(100) NULL
, [BankLabel] nvarchar(50) NULL
, [FormFactor] nvarchar(30) NULL
, [MemoryType] nvarchar(50) NULL
, [Manufacturer] nvarchar(50) NULL
, [Speed] nvarchar(20) NULL
, [ScanID] int NULL
, [EntryID] int NOT NULL  IDENTITY (1,1)
);
GO
CREATE TABLE [Monitor] (
  [ScanID] int NULL
, [EntryID] int NOT NULL  IDENTITY (1,1)
, [Name] nvarchar(255) NULL
, [ScreenWidth] int NULL
, [ScreenHeight] int NULL
, [Availability] nvarchar(100) NULL
);
GO
CREATE TABLE [MotherBoard] (
  [Manufacturer] nvarchar(100) NULL
, [Version] nvarchar(100) NULL
, [Product] nvarchar(100) NULL
, [SerialNo] nvarchar(100) NULL
, [Model] nvarchar(100) NULL
, [ScanID] int NULL
, [EntryID] int NOT NULL  IDENTITY (1,1)
);
GO
CREATE TABLE [Multimedia] (
  [ScanID] int NULL
, [EntryID] int NOT NULL  IDENTITY (1,1)
, [Name] nvarchar(255) NULL
, [Manufacturer] nvarchar(255) NULL
);
GO
CREATE TABLE [NetworkAdapter] (
  [AdapterType] nvarchar(100) NULL
, [NetConnectonState] nvarchar(20) NULL
, [Manufacturer] nvarchar(255) NULL
, [Speed] nvarchar(10) NULL
, [AdapterIPAddress] nvarchar(255) NULL
, [AdapterMACAddress] nvarchar(255) NULL
, [DHCPEnabled] nvarchar(10) NULL
, [DHCPServer] nvarchar(255) NULL
, [DNSDomain] nvarchar(255) NULL
, [WINSPrimaryServer] nvarchar(255) NULL
, [WINSSecondaryServer] nvarchar(255) NULL
, [ScanID] int NULL
, [EntryID] int NOT NULL  IDENTITY (1,1)
);
GO
CREATE TABLE [OS] (
  [Name] nvarchar(512) NULL
, [Version] nvarchar(100) NULL
, [BuildNo] nvarchar(10) NULL
, [InstallDate] datetime NULL
, [ServicePack] nvarchar(100) NULL
, [WindowsID] nvarchar(30) NULL
, [ScanID] int NULL
, [EntryID] int NOT NULL  IDENTITY (1,1)
, [SerialKey] nvarchar(30) NULL
);
GO
CREATE TABLE [Processes] (
  [Name] nvarchar(512) NULL
, [CommandLine] nvarchar(2048) NULL
, [ExecutablePath] nvarchar(2048) NULL
, [CreationDate] datetime NULL
, [ProcessID] int NULL
, [ParentProcessID] int NULL
, [ThreadCount] int NULL
, [VirtualSize] nchar(30) NULL
, [PrivateBytes] nvarchar(30) NULL
, [Priority] int NULL
, [ScanID] int NULL
, [EntryID] int NOT NULL  IDENTITY (1,1)
);
GO
CREATE TABLE [Processor] (
  [DeviceID] nvarchar(10) NULL
, [Name] nvarchar(255) NULL
, [Manufacturer] nvarchar(100) NULL
, [ClockSpeed] nvarchar(10) NULL
, [Architecture] nvarchar(20) NULL
, [L2CacheSize] int NULL
, [L2CacheSpeed] int NULL
, [SocketType] nvarchar(20) NULL
, [Version] nvarchar(100) NULL
, [ScanID] int NULL
, [EntryID] int NOT NULL  IDENTITY (1,1)
);
GO
CREATE TABLE [Scan] (
  [ScanID] int NOT NULL  IDENTITY (1,1)
, [MachineID] int NOT NULL
, [ScanDate] datetime NOT NULL
);
GO
CREATE TABLE [Services] (
  [ScanID] int NULL
, [EntryID] int NOT NULL  IDENTITY (1,1)
, [DisplayName] nvarchar(512) NULL
, [Description] nvarchar(1024) NULL
, [Path] nvarchar(1024) NULL
, [Account] nvarchar(512) NULL
, [State] nvarchar(100) NULL
, [StartMode] nvarchar(100) NULL
);
GO
CREATE TABLE [Share] (
  [Name] nvarchar(255) NULL
, [Caption] nvarchar(255) NULL
, [Path] nvarchar(1024) NULL
, [Type] nvarchar(255) NULL
, [ScanID] int NULL
, [EntryID] int NOT NULL  IDENTITY (1,1)
);
GO
CREATE TABLE [Softwares] (
  [Name] nvarchar(512) NULL
, [Version] nvarchar(20) NULL
, [InstallDate] datetime NULL
, [Location] nvarchar(1024) NULL
, [EntryID] int NOT NULL  IDENTITY (1,1)
, [ScanID] int NULL
);
GO
CREATE TABLE [StartUp] (
  [ScanID] int NULL
, [EntryID] int NOT NULL  IDENTITY (1,1)
, [Name] nvarchar(1024) NULL
, [Command] nvarchar(1024) NULL
, [Location] nvarchar(1024) NULL
, [StartUpUser] nvarchar(512) NULL
);
GO
CREATE TABLE [Video] (
  [ScanID] int NULL
, [EntryID] int NOT NULL  IDENTITY (1,1)
, [Name] nvarchar(255) NULL
, [HorizontalResolution] int NULL
, [VerticalResolution] int NULL
, [CurrentBitsperPixel] int NULL
, [CurrentRefreshRate] nvarchar(10) NULL
, [MemorySize] nvarchar(20) NULL
);
GO
SET IDENTITY_INSERT [Bios] ON
GO
SET IDENTITY_INSERT [Bios] OFF
GO
SET IDENTITY_INSERT [CDRom] ON
GO
SET IDENTITY_INSERT [CDRom] OFF
GO
SET IDENTITY_INSERT [Credential] ON
GO
SET IDENTITY_INSERT [Credential] OFF
GO
SET IDENTITY_INSERT [Discover] ON
GO
SET IDENTITY_INSERT [Discover] OFF
GO
SET IDENTITY_INSERT [Disk] ON
GO
SET IDENTITY_INSERT [Disk] OFF
GO
SET IDENTITY_INSERT [Hotfixes] ON
GO
SET IDENTITY_INSERT [Hotfixes] OFF
GO
SET IDENTITY_INSERT [LogicalDrive] ON
GO
SET IDENTITY_INSERT [LogicalDrive] OFF
GO
SET IDENTITY_INSERT [Memory] ON
GO
SET IDENTITY_INSERT [Memory] OFF
GO
SET IDENTITY_INSERT [Monitor] ON
GO
SET IDENTITY_INSERT [Monitor] OFF
GO
SET IDENTITY_INSERT [MotherBoard] ON
GO
SET IDENTITY_INSERT [MotherBoard] OFF
GO
SET IDENTITY_INSERT [Multimedia] ON
GO
SET IDENTITY_INSERT [Multimedia] OFF
GO
SET IDENTITY_INSERT [NetworkAdapter] ON
GO
SET IDENTITY_INSERT [NetworkAdapter] OFF
GO
SET IDENTITY_INSERT [OS] ON
GO
SET IDENTITY_INSERT [OS] OFF
GO
SET IDENTITY_INSERT [Processes] ON
GO
SET IDENTITY_INSERT [Processes] OFF
GO
SET IDENTITY_INSERT [Processor] ON
GO
SET IDENTITY_INSERT [Processor] OFF
GO
SET IDENTITY_INSERT [Scan] ON
GO
SET IDENTITY_INSERT [Scan] OFF
GO
SET IDENTITY_INSERT [Services] ON
GO
SET IDENTITY_INSERT [Services] OFF
GO
SET IDENTITY_INSERT [Share] ON
GO
SET IDENTITY_INSERT [Share] OFF
GO
SET IDENTITY_INSERT [Softwares] ON
GO
SET IDENTITY_INSERT [Softwares] OFF
GO
SET IDENTITY_INSERT [StartUp] ON
GO
SET IDENTITY_INSERT [StartUp] OFF
GO
SET IDENTITY_INSERT [Video] ON
GO
SET IDENTITY_INSERT [Video] OFF
GO
ALTER TABLE [Bios] ADD PRIMARY KEY ([EntryID]);
GO
ALTER TABLE [CDRom] ADD PRIMARY KEY ([EntryID]);
GO
ALTER TABLE [Credential] ADD PRIMARY KEY ([CredentialID]);
GO
ALTER TABLE [Discover] ADD PRIMARY KEY ([MachineID]);
GO
ALTER TABLE [Disk] ADD PRIMARY KEY ([EntryID]);
GO
ALTER TABLE [Hotfixes] ADD PRIMARY KEY ([EntryID]);
GO
ALTER TABLE [LogicalDrive] ADD PRIMARY KEY ([EntryID]);
GO
ALTER TABLE [Memory] ADD PRIMARY KEY ([EntryID]);
GO
ALTER TABLE [Monitor] ADD PRIMARY KEY ([EntryID]);
GO
ALTER TABLE [MotherBoard] ADD PRIMARY KEY ([EntryID]);
GO
ALTER TABLE [Multimedia] ADD PRIMARY KEY ([EntryID]);
GO
ALTER TABLE [NetworkAdapter] ADD PRIMARY KEY ([EntryID]);
GO
ALTER TABLE [OS] ADD PRIMARY KEY ([EntryID]);
GO
ALTER TABLE [Processes] ADD PRIMARY KEY ([EntryID]);
GO
ALTER TABLE [Processor] ADD PRIMARY KEY ([EntryID]);
GO
ALTER TABLE [Scan] ADD PRIMARY KEY ([ScanID]);
GO
ALTER TABLE [Services] ADD PRIMARY KEY ([EntryID]);
GO
ALTER TABLE [Share] ADD PRIMARY KEY ([EntryID]);
GO
ALTER TABLE [Softwares] ADD PRIMARY KEY ([EntryID]);
GO
ALTER TABLE [StartUp] ADD PRIMARY KEY ([EntryID]);
GO
ALTER TABLE [Video] ADD PRIMARY KEY ([EntryID]);
GO
CREATE UNIQUE INDEX [UQ__Bios__0000000000000185] ON [Bios] ([EntryID] ASC);
GO
CREATE UNIQUE INDEX [UQ__CDRom__000000000000021D] ON [CDRom] ([EntryID] ASC);
GO
CREATE UNIQUE INDEX [UQ__Credential__000000000000009F] ON [Credential] ([CredentialID] ASC);
GO
CREATE UNIQUE INDEX [UQ__Discover__000000000000004E] ON [Discover] ([MachineID] ASC);
GO
CREATE UNIQUE INDEX [UQ__Discover__0000000000000053] ON [Discover] ([MachineName] ASC);
GO
CREATE UNIQUE INDEX [UQ__Disk__00000000000001B1] ON [Disk] ([EntryID] ASC);
GO
CREATE UNIQUE INDEX [UQ__Hotfixes__000000000000020B] ON [Hotfixes] ([EntryID] ASC);
GO
CREATE UNIQUE INDEX [UQ__LogicalDrive__0000000000000230] ON [LogicalDrive] ([EntryID] ASC);
GO
CREATE UNIQUE INDEX [UQ__Memory__00000000000001ED] ON [Memory] ([EntryID] ASC);
GO
CREATE UNIQUE INDEX [UQ__Monitor__0000000000000418] ON [Monitor] ([EntryID] ASC);
GO
CREATE UNIQUE INDEX [UQ__MotherBoard__0000000000000176] ON [MotherBoard] ([EntryID] ASC);
GO
CREATE UNIQUE INDEX [UQ__Multimedia__000000000000042D] ON [Multimedia] ([EntryID] ASC);
GO
CREATE UNIQUE INDEX [UQ__NetwworkAdapter__0000000000000259] ON [NetworkAdapter] ([EntryID] ASC);
GO
CREATE UNIQUE INDEX [UQ__OS__000000000000012C] ON [OS] ([EntryID] ASC);
GO
CREATE UNIQUE INDEX [UQ__Processes__0000000000000236] ON [Processes] ([EntryID] ASC);
GO
CREATE UNIQUE INDEX [UQ__Processor__000000000000010F] ON [Processor] ([EntryID] ASC);
GO
CREATE UNIQUE INDEX [UQ__Scan__00000000000000B3] ON [Scan] ([ScanID] ASC);
GO
CREATE UNIQUE INDEX [UQ__Services__000000000000026F] ON [Services] ([EntryID] ASC);
GO
CREATE UNIQUE INDEX [UQ__Share__0000000000000448] ON [Share] ([EntryID] ASC);
GO
CREATE UNIQUE INDEX [UQ__Softwares__000000000000024B] ON [Softwares] ([EntryID] ASC);
GO
CREATE UNIQUE INDEX [UQ__StartUp__0000000000000461] ON [StartUp] ([EntryID] ASC);
GO
CREATE UNIQUE INDEX [UQ__Video__0000000000000401] ON [Video] ([EntryID] ASC);
GO
-- Upgrade to 1.0.6
GO
ALTER TABLE [Memory] ADD [Capacity] nvarchar(20) NULL
GO
UPDATE [Memory] SET [Capacity] = [Capicity] 
GO
ALTER TABLE [Memory] DROP COLUMN [Capicity]
GO
ALTER TABLE [OS] ADD [LastBootupTime] datetime NULL;
GO
CREATE TABLE [Computer] (
  [ScanID] int NULL
, [EntryID] int NOT NULL  IDENTITY (1,1)
, [BootupState] nvarchar(35) NULL
, [Caption] nvarchar(512) NULL
, [CurrentTimeZone] nvarchar(20) NULL
, [DaylightInEffect] bit NULL
, [Description] nvarchar(512) NULL
, [DNSHostName] nvarchar(512) NULL
, [Domain] nvarchar(512) NULL
, [DomainRole] nvarchar(35) NULL
, [EnableDaylightSavingsTime] bit NULL
, [Manufacturer] nvarchar(512) NULL
, [Model] nvarchar(512) NULL
, [Name] nvarchar(512) NULL
, [NetworkServerModeEnabled] bit NULL
, [PartOfDomain] bit NULL
, [PCSystemType] nvarchar(50) NULL
, [PowerState] nvarchar(35) NULL
, [PowerSupplyState] nvarchar(20) NULL
, [PrimaryOwnerContact] nvarchar(512) NULL
, [PrimaryOwnerName] nvarchar(512) NULL
, [Roles] nvarchar(1024) NULL
, [Status] nvarchar(15) NULL
, [SupportContactDescription] nvarchar(1024) NULL
, [SystemStartupDelay] int NULL
, [SystemStartupOptions] nvarchar(1024) NULL
, [SystemType] nvarchar(30) NULL
, [ThermalState] nvarchar(30) NULL
, [UserName] nvarchar(512) NULL
, [WakeUpType] nvarchar(25) NULL
, [Workgroup] nvarchar(512) NULL
);
GO
CREATE TABLE [EnvironmentVars] (
  [ScanID] int NULL
, [EntryID] int NOT NULL  IDENTITY (1,1)
, [Name] nvarchar(512) NULL
, [Status] nvarchar(25) NULL
, [SystemVariable] bit NULL
, [UserName] nvarchar(512) NULL
, [VariableValue] nvarchar(2048) NULL
);
GO
CREATE TABLE [IPRoutes] (
  [EntryID] int NOT NULL  IDENTITY (1,1)
, [ScanID] int NULL
, [Age] int NULL
, [Name] nvarchar(255) NULL
, [Description] nvarchar(512) NULL
, [Destination] nvarchar(25) NULL
, [Mask] nvarchar(25) NULL
, [Metric1] int NULL
, [Metric2] int NULL
, [Metric3] int NULL
, [Metric4] int NULL
, [Metric5] int NULL
, [NextHop] nvarchar(25) NULL
, [Protocol] nvarchar(30) NULL
, [Status] nvarchar(15) NULL
, [RouteType] nvarchar(15) NULL
);
GO
CREATE TABLE [Printer] (
  [Attributes] nvarchar(1024) NULL
, [Availability] nvarchar(50) NULL
, [AveragePagesPerMinute] nvarchar(50) NULL
, [Caption] nvarchar(65) NULL
, [HorizontalResolution] int NULL
, [VerticalResolution] int NULL
, [Local] bit NULL
, [Network] bit NULL
, [Shared] bit NULL
, [Location] nvarchar(1024) NULL
, [PortName] nvarchar(40) NULL
, [PrinterState] nvarchar(40) NULL
, [PrinterStatus] nvarchar(40) NULL
, [ServerName] nvarchar(255) NULL
, [ShareName] nvarchar(1024) NULL
, [ScanID] int NULL
, [EntryID] int NOT NULL  IDENTITY (1,1)
);
GO
CREATE TABLE [UserGroups] (
  [ScanID] int NULL
, [EntryID] int NOT NULL  IDENTITY (1,1)
, [Users] nvarchar(2056) NULL
, [Caption] nvarchar(512) NULL
, [Description] nvarchar(2056) NULL
, [Domain] nvarchar(512) NULL
, [InstallDate] datetime NULL
, [LocalAccount] bit NULL
, [Name] nvarchar(512) NULL
, [SID] nvarchar(512) NULL
, [Status] nvarchar(20) NULL
);
GO
SET IDENTITY_INSERT [Computer] ON
GO
SET IDENTITY_INSERT [Computer] OFF
GO
SET IDENTITY_INSERT [EnvironmentVars] ON
GO
SET IDENTITY_INSERT [EnvironmentVars] OFF
GO
SET IDENTITY_INSERT [IPRoutes] OFF
GO
SET IDENTITY_INSERT [IPRoutes] ON
GO
SET IDENTITY_INSERT [UserGroups] ON
GO
SET IDENTITY_INSERT [UserGroups] OFF
GO
SET IDENTITY_INSERT [Printer] ON
GO
SET IDENTITY_INSERT [Printer] OFF
GO
ALTER TABLE [Computer] ADD PRIMARY KEY ([EntryID]);
GO
ALTER TABLE [EnvironmentVars] ADD PRIMARY KEY ([EntryID]);
GO
ALTER TABLE [IPRoutes] ADD PRIMARY KEY ([EntryID]);
GO
ALTER TABLE [Printer] ADD PRIMARY KEY ([EntryID]);
GO
ALTER TABLE [UserGroups] ADD PRIMARY KEY ([EntryID]);
GO
ALTER TABLE [Bios] ADD CONSTRAINT [FK_BIOS_SCAN] FOREIGN KEY ([ScanID]) REFERENCES [Scan]([ScanID]) ON DELETE CASCADE ON UPDATE CASCADE;
GO
ALTER TABLE [CDRom] ADD CONSTRAINT [FK_CDROM_SCAN] FOREIGN KEY ([ScanID]) REFERENCES [Scan]([ScanID]) ON DELETE CASCADE ON UPDATE CASCADE;
GO
ALTER TABLE [Computer] ADD CONSTRAINT [FK_COMPUTER_SCAN] FOREIGN KEY ([ScanID]) REFERENCES [Scan]([ScanID]) ON DELETE CASCADE ON UPDATE CASCADE;
GO
ALTER TABLE [Disk] ADD CONSTRAINT [FK_DISK_SCAN] FOREIGN KEY ([ScanID]) REFERENCES [Scan]([ScanID]) ON DELETE CASCADE ON UPDATE CASCADE;
GO
ALTER TABLE [EnvironmentVars] ADD CONSTRAINT [FK_ENVIRONMENTVARS_SCAN] FOREIGN KEY ([ScanID]) REFERENCES [Scan]([ScanID]) ON DELETE CASCADE ON UPDATE CASCADE;
GO
ALTER TABLE [Hotfixes] ADD CONSTRAINT [FK_HOTFIXES_SCAN] FOREIGN KEY ([ScanID]) REFERENCES [Scan]([ScanID]) ON DELETE CASCADE ON UPDATE CASCADE;
GO
ALTER TABLE [IPRoutes] ADD CONSTRAINT [FK_IPROUTES_SCAN] FOREIGN KEY ([ScanID]) REFERENCES [Scan]([ScanID]) ON DELETE CASCADE ON UPDATE CASCADE;
GO
ALTER TABLE [LogicalDrive] ADD CONSTRAINT [FK_LOGICALDRIVE_SCAN] FOREIGN KEY ([ScanID]) REFERENCES [Scan]([ScanID]) ON DELETE CASCADE ON UPDATE CASCADE;
GO
ALTER TABLE [Memory] ADD CONSTRAINT [FK_MEMORY_SCAN] FOREIGN KEY ([ScanID]) REFERENCES [Scan]([ScanID]) ON DELETE CASCADE ON UPDATE CASCADE;
GO
ALTER TABLE [Monitor] ADD CONSTRAINT [FK_MONITOR_SCAN] FOREIGN KEY ([ScanID]) REFERENCES [Scan]([ScanID]) ON DELETE CASCADE ON UPDATE CASCADE;
GO
ALTER TABLE [MotherBoard] ADD CONSTRAINT [FK_MOTHERBOARD_SCAN] FOREIGN KEY ([ScanID]) REFERENCES [Scan]([ScanID]) ON DELETE CASCADE ON UPDATE CASCADE;
GO
ALTER TABLE [Multimedia] ADD CONSTRAINT [FK_MULTIMEDIA_SCAN] FOREIGN KEY ([ScanID]) REFERENCES [Scan]([ScanID]) ON DELETE CASCADE ON UPDATE CASCADE;
GO
ALTER TABLE [NetworkAdapter] ADD CONSTRAINT [FK_NETWORKADAPTER_SCAN] FOREIGN KEY ([ScanID]) REFERENCES [Scan]([ScanID]) ON DELETE CASCADE ON UPDATE CASCADE;
GO
ALTER TABLE [OS] ADD CONSTRAINT [FK_OS_SCAN] FOREIGN KEY ([ScanID]) REFERENCES [Scan]([ScanID]) ON DELETE CASCADE ON UPDATE CASCADE;
GO
ALTER TABLE [Printer] ADD CONSTRAINT [FK_PRINTER_SCAN] FOREIGN KEY ([ScanID]) REFERENCES [Scan]([ScanID]) ON DELETE CASCADE ON UPDATE CASCADE;
GO
ALTER TABLE [Processes] ADD CONSTRAINT [FK_PROCESSES_SCAN] FOREIGN KEY ([ScanID]) REFERENCES [Scan]([ScanID]) ON DELETE CASCADE ON UPDATE CASCADE;
GO
ALTER TABLE [Processor] ADD CONSTRAINT [FK_PROCESSOR_SCAN] FOREIGN KEY ([ScanID]) REFERENCES [Scan]([ScanID]) ON DELETE CASCADE ON UPDATE CASCADE;
GO
ALTER TABLE [Scan] ADD CONSTRAINT [FK_SCAN_MACHINE] FOREIGN KEY ([MachineID]) REFERENCES [Discover]([MachineID]) ON DELETE CASCADE ON UPDATE CASCADE;
GO
ALTER TABLE [Services] ADD CONSTRAINT [FK_SERVICES_SCAN] FOREIGN KEY ([ScanID]) REFERENCES [Scan]([ScanID]) ON DELETE CASCADE ON UPDATE CASCADE;
GO
ALTER TABLE [Share] ADD CONSTRAINT [FK_SHARE_SCAN] FOREIGN KEY ([ScanID]) REFERENCES [Scan]([ScanID]) ON DELETE CASCADE ON UPDATE CASCADE;
GO
ALTER TABLE [Softwares] ADD CONSTRAINT [FK_SOFTWARE_SCAN] FOREIGN KEY ([ScanID]) REFERENCES [Scan]([ScanID]) ON DELETE CASCADE ON UPDATE CASCADE;
GO
ALTER TABLE [StartUp] ADD CONSTRAINT [FK_STARTUP_SCAN] FOREIGN KEY ([ScanID]) REFERENCES [Scan]([ScanID]) ON DELETE CASCADE ON UPDATE CASCADE;
GO
ALTER TABLE [UserGroups] ADD CONSTRAINT [FK_USERGROUP_SCAN] FOREIGN KEY ([ScanID]) REFERENCES [Scan]([ScanID]) ON DELETE CASCADE ON UPDATE CASCADE;
GO
ALTER TABLE [Video] ADD CONSTRAINT [FK_VIDEO_SCAN] FOREIGN KEY ([ScanID]) REFERENCES [Scan]([ScanID]) ON DELETE CASCADE ON UPDATE CASCADE;
GO
CREATE UNIQUE INDEX [UQ__Computer__00000000000003A0] ON [Computer] ([EntryID] ASC);
GO
CREATE UNIQUE INDEX [UQ__EnvironmentVars__0000000000000377] ON [EnvironmentVars] ([EntryID] ASC);
GO
CREATE UNIQUE INDEX [UQ__IPRoutes__0000000000000263] ON [IPRoutes] ([EntryID] ASC);
GO
CREATE UNIQUE INDEX [UQ__Printer__000000000000023A] ON [Printer] ([EntryID] ASC);
GO
CREATE UNIQUE INDEX [UQ__UserGroups__0000000000000421] ON [UserGroups] ([EntryID] ASC);
GO