﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetworkAssetManager.General;

namespace NetworkAssetManager.Entity
{
    public class EntShare : IEntity
	{
		private string _Name = string.Empty;
        private string _Caption = string.Empty;
        private string _Path = string.Empty;
        private string _Type = string.Empty; 
		private int _ScanID = 0; 
		private string _ClassName = "Share";

        public string NodeName
        {
            get
            {
                return _Caption+ " " + _Path;
            }
        }

		
		public string Name
		{
			get
			{
				return _Name; 
			}
			
			set
			{ 
				_Name = value; 
			}
		}
		public string Caption
		{
			get
			{
				return _Caption; 
			}
			
			set
			{ 
				_Caption = value; 
			}
		}
		public string Path
		{
			get
			{
				return _Path; 
			}
			
			set
			{ 
				_Path = value; 
			}
		}
		public string Type
		{
			get
			{
				return _Type; 
			}
			
			set
			{ 
				_Type = value; 
			}
		}
		public int ScanID
		{
			get
			{
				return _ScanID; 
			}
			
			set
			{ 
				_ScanID = value; 
			}
		}

        public string ClassName
        {
            get
            {
                return _ClassName;
            }
        }

        public ImageCode Icon
        {
            get
            {
                return ImageCode.Share;
            }
        }

	}
}