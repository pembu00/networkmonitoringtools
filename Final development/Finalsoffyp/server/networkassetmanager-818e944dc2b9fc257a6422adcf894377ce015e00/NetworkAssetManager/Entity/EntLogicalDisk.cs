﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetworkAssetManager.General;

namespace NetworkAssetManager.Entity
{
    public class EntLogicalDrive : IEntity
    {
        private string _Name = string.Empty;
        private string _Description = string.Empty;
        private string _Size = string.Empty;
        private string _FreeSpace = string.Empty;
        private string _FileSystem = string.Empty;
        private string _SerialNumber = string.Empty;
        private string _VolumeLabel = string.Empty; 
        private int _ScanID = 0;
        private string _ClassName = "LogicalDrive";

        public string NodeName
        {
            get
            {
                return _Name+ " " + _Size;
            }
        }

        public string Name
        {
            get
            {
                return _Name;
            }

            set
            {
                _Name = value;
            }
        }
        public string Description
        {
            get
            {
                return _Description;
            }

            set
            {
                _Description = value;
            }
        }
        public string Size
        {
            get
            {
                return _Size;
            }

            set
            {
                _Size = value;
            }
        }
        public string FreeSpace
        {
            get
            {
                return _FreeSpace;
            }

            set
            {
                _FreeSpace = value;
            }
        }
        public string FileSystem
        {
            get
            {
                return _FileSystem;
            }

            set
            {
                _FileSystem = value;
            }
        }
        public string SerialNumber
        {
            get
            {
                return _SerialNumber;
            }

            set
            {
                _SerialNumber = value;
            }
        }
        public int ScanID
        {
            get
            {
                return _ScanID;
            }

            set
            {
                _ScanID = value;
            }
        }

        public string VolumeLabel
        {
            get
            {
                return _VolumeLabel;
            }

            set
            {
                _VolumeLabel = value;
            }
        }

        public string ClassName
        {
            get
            {
                return _ClassName;
            }
        }

        public ImageCode Icon
        {
            get
            {
                return ImageCode.LogicalDrive;
            }
        }
    }
}


