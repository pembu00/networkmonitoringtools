﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetworkAssetManager.General;

namespace NetworkAssetManager.Entity
{

    public class EntUserGroups : IEntity
    {
        private int _ScanID;
        private string _Users;
        private string _Caption;
        private string _Description;
        private string _Domain;
        private DateTime _InstallDate;
        private bool _LocalAccount;
        private string _Name;
        private string _SID;
        private string _Status;
        private string _ClassName = "UserGroups";

        public string NodeName
        {
            get
            {
                return _Name ;
            }
        }

        public int ScanID
        {
            get
            {
                return _ScanID;
            }
            set
            {
                _ScanID = value;
            }
        }

        public string Users
        {
            get
            {
                return _Users;
            }
            set
            {
                _Users = value;
            }
        }


        public string Caption
        {
            get
            {
                return _Caption;
            }
            set
            {
                _Caption = value;
            }
        }


        public string Description
        {
            get
            {
                return _Description;
            }
            set
            {
                _Description = value;
            }
        }


        public string Domain
        {
            get
            {
                return _Domain;
            }
            set
            {
                _Domain = value;
            }
        }


        public System.DateTime InstallDate
        {
            get
            {
                return _InstallDate;
            }
            set
            {
                _InstallDate = value;
            }
        }


        public bool LocalAccount
        {
            get
            {
                return _LocalAccount;
            }
            set
            {
                _LocalAccount = value;
            }
        }


        public string Name
        {
            get
            {
                return _Name;
            }
            set
            {
                _Name = value;
            }
        }


        public string SID
        {
            get
            {
                return _SID;
            }
            set
            {
                _SID = value;
            }
        }


        public string Status
        {
            get
            {
                return _Status;
            }
            set
            {
                _Status = value;
            }
        }

        public string ClassName
        {
            get
            {
                return _ClassName;
            }
        }

        public ImageCode Icon
        {
            get
            {
                return ImageCode.UserGroup;
            }
        }


    }
}