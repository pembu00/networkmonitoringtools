﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetworkAssetManager.General;

namespace NetworkAssetManager.Entity
{
    public class EntCDRom : IEntity
    {
        private string _Name = string.Empty;
        private string _MediaType = string.Empty;
        private string _Manufacturer = string.Empty;
        private string _Drive = string.Empty;
        private int _ScanID = 0;
        private string _ClassName = "CDRom";

        public string NodeName
        {
            get
            {
                return _Name + " " + MediaType;
            }
        }

        public string Name
        {
            get
            {
                return _Name;
            }

            set
            {
                _Name = value;
            }
        }
        public string MediaType
        {
            get
            {
                return _MediaType;
            }

            set
            {
                _MediaType = value;
            }
        }
        public string Manufacturer
        {
            get
            {
                return _Manufacturer;
            }

            set
            {
                _Manufacturer = value;
            }
        }
        public string Drive
        {
            get
            {
                return _Drive;
            }

            set
            {
                _Drive = value;
            }
        }
        public int ScanID
        {
            get
            {
                return _ScanID;
            }

            set
            {
                _ScanID = value;
            }
        }

        public string ClassName
        {
            get
            {
                return _ClassName;
            }
        }

        public ImageCode Icon
        {
            get
            {
                return ImageCode.CDRom;
            }
        }
    }
}


