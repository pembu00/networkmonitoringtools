﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetworkAssetManager.General;

namespace NetworkAssetManager.Entity
{
    public class EntHotfixes : IEntity
	{
		private int _ScanID = 0;
        private string _HotfixID = string.Empty;
        private string _Description = string.Empty;
        private string _ServicePackInEffect = string.Empty;
        private string _InstallBy = string.Empty;
        private string _FixComments = string.Empty; 
		private string _ClassName = "Hotfixes";

        public string NodeName
        {
            get
            {
                return _HotfixID + " " + _ServicePackInEffect;
            }
        }

		public int ScanID
		{
            get
            {
                return _ScanID;
            }
			set
			{ 
				_ScanID = value; 
			}
		}
		public string HotfixID
		{
			get
			{
				return _HotfixID; 
			}
			
			set
			{ 
				_HotfixID = value; 
			}
		}
		public string Description
		{
			get
			{
				return _Description; 
			}
			
			set
			{ 
				_Description = value; 
			}
		}
		public string SPEffect 
		{
			get
			{
				return _ServicePackInEffect ; 
			}
			
			set
			{ 
				_ServicePackInEffect  = value; 
			}
		}
		public string InstallBy
		{
			get
			{
				return _InstallBy; 
			}
			
			set
			{ 
				_InstallBy = value; 
			}
		}
		public string FixComments
		{
			get
			{
				return _FixComments; 
			}
			
			set
			{ 
				_FixComments = value; 
			}
		}
		
		public string ClassName
        {
            get
            {
                return _ClassName;
            }
        }

        public ImageCode Icon
        {
            get
            {
                return ImageCode.Hotfix;
            }
        }

	}
}