﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetworkAssetManager.General;

namespace NetworkAssetManager.Entity
{
    public class EntProcesses : IEntity
    {
        private string _Name = string.Empty;
        private string _CommandLine = string.Empty;
        private string _ExecutablePath = string.Empty;
        private DateTime _CreationDate;
        private int _ProcessID = 0;
        private int _ParentProcessID = 0;
        private int _ThreadCount = 0;
        private string _VirtualSize = string.Empty;
        private string _PrivateBytes = string.Empty;
        private int _Priority = 0;
        private int _ScanID = 0;
        private string _ClassName = "Processes";

        public string NodeName
        {
            get
            {
                return _Name; 
            }
        }

        public string Name
        {
            get
            {
                return _Name;
            }
            set
            {
                _Name = value;
            }
        }

        public int ProcessID
        {
            get
            {
                return _ProcessID;
            }
            set
            {
                _ProcessID = value;
            }
        }

        public int ParentProcessID
        {
            get
            {
                return _ParentProcessID;
            }
            set
            {
                _ParentProcessID = value;
            }
        }

        public int ThreadCount
        {
            get
            {
                return _ThreadCount;
            }
            set
            {
                _ThreadCount = value;
            }
        }

        public string VirtualSize
        {
            get
            {
                return _VirtualSize;
            }
            set
            {
                _VirtualSize = value;
            }
        }

        public string PrivateBytes
        {
            get
            {
                return _PrivateBytes;
            }
            set
            {
                _PrivateBytes = value;
            }
        }

        public int Priority
        {
            get
            {
                return _Priority;
            }
            set
            {
                _Priority = value;
            }
        }

        public string CommandLine
        {
            get
            {
                return _CommandLine;
            }
            set
            {
                _CommandLine = value;
            }
        }

        public string ExecutablePath
        {
            get
            {
                return _ExecutablePath;
            }
            set
            {
                _ExecutablePath = value;
            }
        }

        public System.DateTime CreationDate
        {
            get
            {
                return _CreationDate;
            }
            set
            {
                _CreationDate = value;
            }
        }


        public int ScanID
        {
            get
            {
                return _ScanID;
            }
            set
            {
                _ScanID = value;
            }
        }

        public string ClassName
        {
            get
            {
                return _ClassName;
            }
        }

        public ImageCode Icon
        {
            get
            {
                return ImageCode.Hotfix;
            }
        }
    }
}