﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetworkAssetManager.General;

namespace NetworkAssetManager.Entity
{
    public class EntStartUp : IEntity
	{
		private int _ScanID = 0;
        private string _Name = string.Empty;
        private string _Command = string.Empty;
        private string _Location = string.Empty;
        private string _User = string.Empty; 
		private string _ClassName = "StartUp";

        public string NodeName
        {
            get
            {
                return _Name+ " " + _Location;
            }
        }

		
		public int ScanID
		{
			get
			{
				return _ScanID; 
			}
			
			set
			{ 
				_ScanID = value; 
			}
		}
		public string Name
		{
			get
			{
				return _Name; 
			}
			
			set
			{ 
				_Name = value; 
			}
		}
		public string Command
		{
			get
			{
				return _Command; 
			}
			
			set
			{ 
				_Command = value; 
			}
		}
		public string Location
		{
			get
			{
				return _Location; 
			}
			
			set
			{ 
				_Location = value; 
			}
		}
		public string StartUpUser
		{
			get
			{
				return _User; 
			}
			
			set
			{ 
				_User = value; 
			}
		}
		
		public string ClassName
        {
            get
            {
                return _ClassName;
            }
        }

        public ImageCode Icon
        {
            get
            {
                return ImageCode.StartUp;
            }
        }

	}
}