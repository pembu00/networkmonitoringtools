﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetworkAssetManager.General;

namespace NetworkAssetManager.Entity
{
    public class EntDisk : IEntity
    {
        private int _ScanID = 0;
        private string _Caption = string.Empty;
        private string _Size = string.Empty;
        private string _Manufacturer = string.Empty;
        private string _Interface = string.Empty;
        private string _MediaType = string.Empty;
        private int _BytesPerSector = 0;
        private int _Heads = 0;
        private string  _Cylinders = string.Empty;
        private string _Sectors = string.Empty;
        private string _Tracks = string.Empty;
        private string _Model = string.Empty;
        private string _ClassName = "Disk";

        public string NodeName
        {
            get
            {
                return _Manufacturer+ " " + _Interface+ " "+_Size;
            }
        }

        public int ScanID
        {
            get
            {
                return _ScanID;
            }

            set
            {
                _ScanID = value;
            }
        }
        public string Caption
        {
            get
            {
                return _Caption;
            }

            set
            {
                _Caption = value;
            }
        }
        public string Size
        {
            get
            {
                return _Size;
            }

            set
            {
                _Size = value;
            }
        }
        public string Manufacturer
        {
            get
            {
                return _Manufacturer;
            }

            set
            {
                _Manufacturer = value;
            }
        }
        public string Interface
        {
            get
            {
                return _Interface;
            }

            set
            {
                _Interface = value;
            }
        }
        public string MediaType
        {
            get
            {
                return _MediaType;
            }

            set
            {
                _MediaType = value;
            }
        }
        public int BytesPerSector
        {
            get
            {
                return _BytesPerSector;
            }

            set
            {
                _BytesPerSector = value;
            }
        }
        public int Heads
        {
            get
            {
                return _Heads;
            }

            set
            {
                _Heads = value;
            }
        }
        public string Cylinders
        {
            get
            {
                return _Cylinders;
            }

            set
            {
                _Cylinders = value;
            }
        }
        public string Sectors
        {
            get
            {
                return _Sectors;
            }

            set
            {
                _Sectors = value;
            }
        }
        public string Tracks
        {
            get
            {
                return _Tracks;
            }

            set
            {
                _Tracks = value;
            }
        }
        public string Model
        {
            get
            {
                return _Model;
            }

            set
            {
                _Model = value;
            }
        }

        public string ClassName
        {
            get
            {
                return _ClassName;
            }
        }

        public ImageCode Icon
        {
            get
            {
                return ImageCode.Disk;
            }
        }
    }
}

