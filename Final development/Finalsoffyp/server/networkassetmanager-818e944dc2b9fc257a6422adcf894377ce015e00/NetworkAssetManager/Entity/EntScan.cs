﻿
using System;

namespace NetworkAssetManager.Entity
{
    public class EntScan
    {
        #region Internal private variables
        private int _ScanID = 0;
        private int _MachineID = 0;
        private DateTime _Date = DateTime.MinValue;

        #endregion

        #region	Properties for the Scan object
        public int ScanID
        {
            get
            {
                return _ScanID;
            }

            set
            {
                _ScanID = value;
            }
        }
        public int MachineID
        {
            get
            {
                return _MachineID;
            }

            set
            {
                _MachineID = value;
            }
        }
        public DateTime Date
        {
            get
            {
                return _Date;
            }

            set
            {
                _Date = value;
            }
        }

        #endregion
    }
}

