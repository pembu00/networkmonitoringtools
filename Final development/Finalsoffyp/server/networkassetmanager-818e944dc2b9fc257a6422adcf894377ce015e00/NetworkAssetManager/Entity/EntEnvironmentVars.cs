﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetworkAssetManager.General;

namespace NetworkAssetManager.Entity
{
    public class EntEnvironmentVars : IEntity
    {
        private int _ScanID = 0;
        private string _Name = string.Empty;
        private string _Status = string.Empty;
        private bool _SystemVariable = false;
        private string _UserName = string.Empty;
        private string _VariableValue = string.Empty;
        private string _ClassName = "EnvironmentVars";


        public string NodeName
        {
            get
            {
                return _Name;
            }
        }

        public int ScanID
        {
            get
            {
                return _ScanID;
            }
            set
            {
                _ScanID = value;
            }
        }

        public string Name
        {
            get
            {
                return _Name;
            }
            set
            {
                _Name = value;
            }
        }


        public string Status
        {
            get
            {
                return _Status;
            }
            set
            {
                _Status = value;
            }
        }


        public bool SystemVariable
        {
            get
            {
                return _SystemVariable;
            }
            set
            {
                _SystemVariable = value;
            }
        }


        public string UserName
        {
            get
            {
                return _UserName;
            }
            set
            {
                _UserName = value;
            }
        }


        public string VariableValue
        {
            get
            {
                return _VariableValue;
            }
            set
            {
                _VariableValue = value;
            }
        }

        public string ClassName
        {
            get
            {
                return _ClassName;
            }
        }

        public ImageCode Icon
        {
            get
            {
                return ImageCode.EnvironmentVar;
            }
        }


    }
}