﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetworkAssetManager.General;

namespace NetworkAssetManager.Entity
{
    public class EntCredential
    {
        #region Internal private variables
        private string _CredentialName = string.Empty;
        private string _Username = string.Empty;
        private string _Password = string.Empty;
        private int _CredentialID = 0;
        private DBOperations _Operation = DBOperations.Insert; 
        #endregion

        #region Properties for the Discover object
        public DBOperations Operation
        {
            get
            {
                return _Operation;
            }
            set
            {
                _Operation = value;

            }
        }

        public string CredentialName
        {
            get
            {
                return _CredentialName;
            }
            set
            {
                _CredentialName = value;

            }
        }

        public string Username
        {
            get
            {
                return _Username;
            }
            set
            {
                _Username = value;

            }
        }
        public string Password
        {
            get
            {
                return _Password;
            }
            set
            {
                _Password = value;

            }
        }
        public int CredentialID
        {
            get
            {
                return _CredentialID;
            }
            set
            {
                _CredentialID = value;
            }

        }
        #endregion 

    }
}
