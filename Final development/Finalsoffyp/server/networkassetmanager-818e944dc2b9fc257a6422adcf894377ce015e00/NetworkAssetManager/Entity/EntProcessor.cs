﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetworkAssetManager.General;

namespace NetworkAssetManager.Entity
{
    public class EntProcessor : IEntity
    {
        private int _ScanID = 0; 
        private string _DeviceID = string.Empty;
        private string _Name = string.Empty;
        private string _Manufacturer = string.Empty;
        private string _ClockSpeed = string.Empty;
        private string _Architecture= string.Empty;
        private int    _L2CacheSize = 0;
        private int    _L2CacheSpeed = 0;
        private string _SocketType= string.Empty;
        private string _Version = string.Empty;
        private string _ClassName = "Processor";

        public string NodeName
        {
            get
            {
                return _DeviceID + " " + _ClockSpeed;
            }
        }


        public int ScanID
        {
            set
            {
                _ScanID = value;
            }
            get
            {
                return _ScanID;
            }
        }


        public string DeviceID
        {
            set
            {
                _DeviceID = value;
            }
            get
            {
                return _DeviceID;
            }
        }

        public string Name
        {
            set
            {
                _Name = value;
            }
            get
            {
                return _Name;
            }
        }

        public string Manufacturer
        {
            set
            {
                _Manufacturer = value;
            }
            get
            {
                return _Manufacturer;
            }
        }

        public string ClockSpeed
        {
            set
            {
                _ClockSpeed = value;
            }
            get
            {
                return _ClockSpeed;
            }
        }

        public string Architecture
        {
            set
            {
                _Architecture = value;
            }
            get
            {
                return _Architecture;
            }
        }

        public int L2CacheSize
        {
            set
            {
                _L2CacheSize = value;  
            }
            get
            {
                return _L2CacheSize; 
            }
        }

        public int L2CacheSpeed
        {
            set
            {
                _L2CacheSpeed = value;
            }
            get
            {
                return _L2CacheSpeed;
            }
        }

/*
        public int L3CacheSize
        {
            set
            {
                _L3CacheSize = value;
            }
            get
            {
                return _L3CacheSize;
            }
        }

        public int L3CacheSpeed
        {
            set
            {
                _L3CacheSpeed = value;
            }
            get
            {
                return _L3CacheSpeed;
            }
        }
*/

        public string SocketType
        {
            set
            {
                _SocketType = value;
            }
            get
            {
                return _SocketType;
            }
        }

        public string Version
        {
            set
            {
                _Version = value;
            }
            get
            {
                return _Version;
            }
        }

        public string ClassName
        {
            get
            {
                return _ClassName; 
            }
        }

        public ImageCode Icon
        {
            get
            {
                return ImageCode.Processor;
            }
        }




    }
}
