﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetworkAssetManager.General;

namespace NetworkAssetManager.Entity
{
    public class EntMonitor : IEntity
    {
        private int _ScanID = 0;
        private string _Name = string.Empty;
        private int _ScreenWidth = 0;
        private int _ScreenHeight = 0;
        private string _ClassName = "Monitor";
        private string _Availability = string.Empty;

        public string NodeName
        {
            get
            {
                return _Name + " " + _ScreenHeight+"X"+_ScreenWidth;
            }
        }

        public string Availability
        {
            get
            {
                return _Availability;
            }

            set
            {
                _Availability = value;
            }
        }

        public int ScanID
        {
            get
            {
                return _ScanID;
            }

            set
            {
                _ScanID = value;
            }
        }
        public string Name
        {
            get
            {
                return _Name;
            }

            set
            {
                _Name = value;
            }
        }
        public int ScreenWidth
        {
            get
            {
                return _ScreenWidth;
            }

            set
            {
                _ScreenWidth = value;
            }
        }
        public int ScreenHeight
        {
            get
            {
                return _ScreenHeight;
            }

            set
            {
                _ScreenHeight = value;
            }
        }

        public string ClassName
        {
            get
            {
                return _ClassName;
            }
        }

        public ImageCode Icon
        {
            get
            {
                return ImageCode.Monitor;
            }
        }
    }
}

