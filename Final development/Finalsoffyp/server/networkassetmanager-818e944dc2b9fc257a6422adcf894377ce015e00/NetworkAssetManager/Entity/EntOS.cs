﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetworkAssetManager.General; 

namespace NetworkAssetManager.Entity
{
    public class EntOS : IEntity
    {
        private int _ScanID = 0; 
        private string _Name = string.Empty;
        private string _Version = string.Empty;
        private string _BuildNo = string.Empty;
        DateTime _InstallDate = DateTime.MinValue;
        DateTime _LastBootupTime = DateTime.MinValue;
        private string _ServicePack = string.Empty;
        private string _WindowsID = string.Empty; 
        private string _SerialKey = string.Empty;
        private string _ClassName = "OS";

        public string NodeName
        {
            get
            {
                return _Name + " " + _ServicePack;
            }
        }


        public int ScanID
        {
            set
            {
                _ScanID = value;
            }
            get
            {
                return _ScanID;
            }
        }

        public string Name
        {
            set
            {
                _Name = value; 
            }
            get
            {
                return _Name; 
            }
        }

        public string Version 
        {
            set
            {
                _Version = value; 
            }
            get
            {
                return _Version; 
            }
        }

        public string BuildNo
        {
            set
            {
                _BuildNo = value; 
            }
            get
            {
                return _BuildNo; 
            }
        }

        public DateTime InstallDate
        {
            set
            {
                _InstallDate = value; 
            }
            get
            {
                return _InstallDate; 
            }
        }

        public DateTime LastBootupTime
        {
            set
            {
                _LastBootupTime = value;
            }
            get
            {
                return _LastBootupTime;
            }
        }

        public string ServicePack
        {
            set
            {
                _ServicePack= value; 
            }
            get
            {
                return _ServicePack; 
            }
        }

        public string WindowsID
        {
            set
            {
                _WindowsID= value; 
            }
            get
            {
                return _WindowsID; 
            }
        }

        public string SerialKey
        {
            set
            {
                _SerialKey = value; 
            }
            get
            {
                return _SerialKey; 
            }
        }
        public string ClassName
        {
            get
            {
                return _ClassName;
            }
        }

        public ImageCode Icon
        {
            get
            {
                return ImageCode.OS;
            }
        }


    }
}
