﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetworkAssetManager.General;

namespace NetworkAssetManager.Entity
{
    public interface IEntity
    {
        string NodeName
        {
            get;
        }
        int ScanID
        {
            set;
            get;
        }
        string ClassName
        {
            get;
        }
        ImageCode Icon
        {
            get;
        }
    }
}
